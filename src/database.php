<?php

return [
    'database_type' => 'pgsql',
    'database_name' => 'pa',
    'server' => '127.0.0.1',
    'port' => 15432,
    'username' => 'postgres',
    'password' => 'postgres',
];