<?php

use Medoo\Medoo;
use QL\QueryList;

/**
 * http://docs.guzzlephp.org/en/stable/request-options.html#query
 * https://medoo.in/api/new
 * http://www.querylist.cc/docs/guide/v4/http-client
 */
chdir(__DIR__);
require_once realpath('..') . '/vendor/autoload.php';
$config = include 'database.php';

const BRAND = '宝马';
echo '爬虫开始' . PHP_EOL;
$db = new Medoo($config);
$db->delete('tDealer', ['sBrand' => BRAND]);
$client = new GuzzleHttp\Client();

$res = $client->get(
    'https://cn-digital2-app.bmw.com.cn/dlo/init', [
        'headers' => [
            'Referer' => 'https://www.minichina.com.cn/zh_CN/home/dlo.html',
        ]
    ]
);
$html = (string)$res->getBody();
preg_match('/{"provinces":\[(?<provinces>.*?)\],"cities":\[(?<cities>.*?)\]/', $html, $m);

$provinces = json_decode('[' . $m['provinces'] . ']', true);
$cities = json_decode('[' . $m['cities'] . ']', true);

$dict = [];
array_map(static function ($item) use (&$dict) {
    $dict[$item['id']] = $item['nz'];
}, array_merge($provinces, $cities));


/**
 * @return mixed
 */
function get_dealers()
{
    global $client;
    $res = $client->get('https://cn-digital2-app.bmw.com.cn/dlo/outlet',
        [
            'headers' => [
                'Referer' => 'https://www.minichina.com.cn/zh_CN/home/dlo.html',
            ]
        ]);
    $html = (string)$res->getBody();

    preg_match('/{"outlets":\[(?<dealers>.*?)\],"totalNum"/', $html, $m);

    return json_decode('[' . $m['dealers'] . ']', true);
}

$dealers = get_dealers();

foreach ($dealers as $item) {
    $remarks = '';
    if (in_array('20', $item['serviceCodes'], true)) {
        $remarks .= '二手车认证/';
    }
    if (in_array('03', $item['serviceCodes'], true)) {
        $remarks .= '大客户服务/';
    }
    if (in_array('05', $item['serviceCodes'], true)) {
        $remarks .= 'BMWi 标示/';
    }
    if (in_array('06', $item['serviceCodes'], true)) {
        $remarks .= 'BMWm 标示/';
    }
    $db->insert('tDealer', [
        'sDealerName' => $item['nz'],
        'nBrandID' => $item['dealerId'],
        'sBrand' => BRAND,
        'sProvince' => $dict[$item['pv']],
        'sCity' => $dict[$item['ct']],
        'sAddress' => $item['az'],
        'sSaleCall' => $item['tel'],
        'sLatitude' => $item['lnb'],
        'sLongitude' => $item['ltb'],
        'dUpdateTime' => Medoo::raw('now()'),
        'sManufacturer' => BRAND,
        'sRemarks' => $remarks,
    ]);
    echo "{$item['nz']}完成" . PHP_EOL;
}


echo '爬虫结束' . PHP_EOL;