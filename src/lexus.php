<?php

use Medoo\Medoo;
use QL\QueryList;

/**
 * http://docs.guzzlephp.org/en/stable/request-options.html#query
 * https://medoo.in/api/new
 * http://www.querylist.cc/docs/guide/v4/http-client
 */
chdir(__DIR__);
require_once realpath('..') . '/vendor/autoload.php';
$config = include 'database.php';
const BRAND = '雷克萨斯';

echo '爬虫开始' . PHP_EOL;
$db = new Medoo($config);
$db->delete('tDealer', ['sBrand' => BRAND]);
$client = new GuzzleHttp\Client();

$res = $client->get('http://www.lexus.com.cn/json/dealerdata');
$html = (string)$res->getBody();
$data = json_decode($html, true);

foreach ($data as $item) {
    list($lnb, $ltb) = explode(',', $item['MapCode']);
    $db->insert('tDealer', [
        'sDealerName' => $item['Name'],
        'nBrandID' => $item['dealerCode'],
        'sBrand' => BRAND,
        'sProvince' => $item['Province'],
        'sCity' => $item['City'],
        'sAddress' => $item['Address'],
        'sSaleCall' => $item['Tel'],
        'sLatitude' => $ltb,
        'sLongitude' => $lnb,
        'dUpdateTime' => Medoo::raw('now()'),
        'sManufacturer' => BRAND,
        'sRemarks' => '',
    ]);
    echo "{$item['Name']}完成" . PHP_EOL;
}
echo '爬虫结束' . PHP_EOL;