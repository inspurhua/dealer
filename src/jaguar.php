<?php

use Medoo\Medoo;
use QL\QueryList;

/**
 * http://docs.guzzlephp.org/en/stable/request-options.html#query
 * https://medoo.in/api/new
 * http://www.querylist.cc/docs/guide/v4/http-client
 */
chdir(__DIR__);
require_once realpath('..') . '/vendor/autoload.php';
$config = include 'database.php';
const BRAND = '捷豹';

echo '爬虫开始' . PHP_EOL;
$db = new Medoo($config);
$db->delete('tDealer', ['sBrand' => BRAND]);
$client = new GuzzleHttp\Client();
$res = $client->get('https://dealer.jaguar.com.cn/index.php?s=/JDealer/api/getDealerList&is_extend=11',
    [
        'headers' => [
            'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36'

        ],
        'verify' => false,
    ]);
$html = (string)$res->getBody();
$data = json_decode($html, true);

foreach ($data['data'] as $item) {

    $db->insert('tDealer', [
        'sDealerName' => trim($item['dealer_name']),
        'nBrandID' => $item['dms_code'],
        'sBrand' => BRAND,
        'sProvince' => $item['province_name'],
        'sCity' => $item['city_name'],
        'sAddress' => $item['addr'],
        'sSaleCall' => $item['sales_phone_jaguar'],
        'sLatitude' => $item['latitude'],
        'sLongitude' => $item['longitude'],
        'dUpdateTime' => Medoo::raw('now()'),
        'sManufacturer' => BRAND,
        'sRemarks' => '',
    ]);
    echo "{$item['dealer_name']}完成" . PHP_EOL;
}
echo '爬虫结束' . PHP_EOL;