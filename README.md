# dealer

CREATE TABLE "public"."tDealer" (
  "sDealerName" text,
  "nBrandID" text,
  "sBrand" text,
  "sProvince" text,
  "sCity" text,
  "sAddress" text,
  "sSaleCall" text,
  "sCustomerServiceCall" text,
  "sDealerType" text,
  "nManufacturerID" text,
  "sManufacturer" text,
  "nState" text,
  "dOpeningDate" text,
  "dCloseDate" text,
  "dUpdateTime" text,
  "nDealerIDWeb" text,
  "sLongitude" text,
  "sLatitude" text,
  "sControllingShareholder" text,
  "sOtherShareholders" text,
  "sStatus" text,
  "sRemarks" text
)
;

create table "charger"(
    "id" bigserial primary key,
    "brand" text,
    "province" text,
    "city" text,
    "title" text,
    "address" text,
    "tel" text,
    "lng" text,
    "lat" text,
    "note" text
);