package com.zql.Main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年8月17日 上午10:56:20 
* 类说明 
*/
public class Rizhi {
	public PrintStream old = System.out;
	public static void main(String[] args) {
		System.out.println("开启日志功能！");
		try {
			Date d = new Date();
			SimpleDateFormat sim = new SimpleDateFormat("yyyyMM");
			String lj = "conn//rizhi"+sim.format(d)+".txt";
			File f=new File(lj);
			f.setWritable(true, false);    //设置写权限，windows下不用此语句`
			f.createNewFile();
			FileOutputStream fileOutputStream = new FileOutputStream(f);
			PrintStream printStream = new PrintStream(fileOutputStream);
			System.setOut(printStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
 