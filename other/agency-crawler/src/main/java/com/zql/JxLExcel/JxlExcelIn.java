package com.zql.JxLExcel; 
import java.io.File;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月6日 下午2:33:32 
* 类说明 
*/
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
 
/**
 * Created by huangteng on 2016/8/4.
 */
public class JxlExcelIn {
    /**
     *  JXL解析Excel文件
     * @author huangteng
     */
    public static void main(String args[]){
        Workbook wb = null;
        try {
            //1.创建工作簿
            wb = Workbook.getWorkbook(new File("f:/tPremiumRatemodel.xls"));
            //2.获取sheet
            Sheet sheet = wb.getSheet(0);
            //3.简单的获取并打印Excel数据
            //sheet.getRows() 获取总的行数  sheet.getColumns() 获取一行的列数
            for(int i = 0;i<sheet.getRows();i++){
                for(int j=0;j<sheet.getColumns();j++){
                    Cell cell = sheet.getCell(j,i);//获取每一个单元格，参数是行和列坐标	
                    System.out.print(cell.getContents()+"  ");
                }
                System.out.println();//隔行
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(wb!=null){
                wb.close();
            }
        }
    }
}
 