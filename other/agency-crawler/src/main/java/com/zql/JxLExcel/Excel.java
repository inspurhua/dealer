package com.zql.JxLExcel;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月6日 上午11:26:16 
* 类说明 
*/
public class Excel {
	private List<Integer> nDealerID;
	private List<String> sDealerName;
	private List<Integer> nBrandID;//品牌
	private List<String> sBrand;
	private List<String> sProvince;
	private List<String> sCity;
	private List<String> sAddress;
	private List<String> sSaleCall;
	private List<String> sCustomerServiceCall;
	private List<String> sDealerType;
	private List<Integer> nManufacturerID;//厂商
	private List<String> sManufacturer;
	private List<Integer> nState;
	private List<String> dOpeningDate;
	private List<String> dCloseDate;
	private List<String> dUpdateTime;
	private List<Integer> nDealerIDWeb;
	private List<String> sLongitude; // 经度
	private List<String> sLatitude; // 纬度
	private List<String> sControllingShareholder; // 控股方
	private List<String> sOtherShareholders; //其他股东
	private List<String> sStatus;
	private List<String> sRemarks;
	public List<Integer> getnDealerID() {
		return nDealerID;
	}
	public void setnDealerID(List<Integer> nDealerID) {
		this.nDealerID = nDealerID;
	}
	public List<String> getsDealerName() {
		return sDealerName;
	}
	public void setsDealerName(List<String> sDealerName) {
		this.sDealerName = sDealerName;
	}
	public List<Integer> getnBrandID() {
		return nBrandID;
	}
	public void setnBrandID(List<Integer> nBrandID) {
		this.nBrandID = nBrandID;
	}
	public List<String> getsBrand() {
		return sBrand;
	}
	public void setsBrand(List<String> sBrand) {
		this.sBrand = sBrand;
	}
	public List<String> getsProvince() {
		return sProvince;
	}
	public void setsProvince(List<String> sProvince) {
		this.sProvince = sProvince;
	}
	public List<String> getsCity() {
		return sCity;
	}
	public void setsCity(List<String> sCity) {
		this.sCity = sCity;
	}
	public List<String> getsAddress() {
		return sAddress;
	}
	public void setsAddress(List<String> sAddress) {
		this.sAddress = sAddress;
	}
	public List<String> getsSaleCall() {
		return sSaleCall;
	}
	public void setsSaleCall(List<String> sSaleCall) {
		this.sSaleCall = sSaleCall;
	}
	public List<String> getsCustomerServiceCall() {
		return sCustomerServiceCall;
	}
	public void setsCustomerServiceCall(List<String> sCustomerServiceCall) {
		this.sCustomerServiceCall = sCustomerServiceCall;
	}
	public List<String> getsDealerType() {
		return sDealerType;
	}
	public void setsDealerType(List<String> sDealerType) {
		this.sDealerType = sDealerType;
	}
	public List<Integer> getnManufacturerID() {
		return nManufacturerID;
	}
	public void setnManufacturerID(List<Integer> nManufacturerID) {
		this.nManufacturerID = nManufacturerID;
	}
	public List<String> getsManufacturer() {
		return sManufacturer;
	}
	public void setsManufacturer(List<String> sManufacturer) {
		this.sManufacturer = sManufacturer;
	}
	public List<Integer> getnState() {
		return nState;
	}
	public void setnState(List<Integer> nState) {
		this.nState = nState;
	}
	public List<String> getdOpeningDate() {
		return dOpeningDate;
	}
	public void setdOpeningDate(List<String> dOpeningDate) {
		this.dOpeningDate = dOpeningDate;
	}
	public List<String> getdCloseDate() {
		return dCloseDate;
	}
	public void setdCloseDate(List<String> dCloseDate) {
		this.dCloseDate = dCloseDate;
	}
	public List<String> getdUpdateTime() {
		return dUpdateTime;
	}
	public void setdUpdateTime(List<String> dUpdateTime) {
		this.dUpdateTime = dUpdateTime;
	}
	public List<Integer> getnDealerIDWeb() {
		return nDealerIDWeb;
	}
	public void setnDealerIDWeb(List<Integer> nDealerIDWeb) {
		this.nDealerIDWeb = nDealerIDWeb;
	}
	public List<String> getsLongitude() {
		return sLongitude;
	}
	public void setsLongitude(List<String> sLongitude) {
		this.sLongitude = sLongitude;
	}
	public List<String> getsLatitude() {
		return sLatitude;
	}
	public void setsLatitude(List<String> sLatitude) {
		this.sLatitude = sLatitude;
	}
	public List<String> getsControllingShareholder() {
		return sControllingShareholder;
	}
	public void setsControllingShareholder(List<String> sControllingShareholder) {
		this.sControllingShareholder = sControllingShareholder;
	}
	public List<String> getsOtherShareholders() {
		return sOtherShareholders;
	}
	public void setsOtherShareholders(List<String> sOtherShareholders) {
		this.sOtherShareholders = sOtherShareholders;
	}
	public List<String> getsStatus() {
		return sStatus;
	}
	public void setsStatus(List<String> sStatus) {
		this.sStatus = sStatus;
	}
	public List<String> getsRemarks() {
		return sRemarks;
	}
	public void setsRemarks(List<String> sRemarks) {
		this.sRemarks = sRemarks;
	}
	@Override
	public String toString() {
		return "Excel [nDealerID=" + nDealerID + ", sDealerName=" + sDealerName + ", nBrandID=" + nBrandID + ", sBrand="
				+ sBrand + ", sProvince=" + sProvince + ", sCity=" + sCity + ", sAddress=" + sAddress + ", sSaleCall="
				+ sSaleCall + ", sCustomerServiceCall=" + sCustomerServiceCall + ", sDealerType=" + sDealerType
				+ ", nManufacturerID=" + nManufacturerID + ", sManufacturer=" + sManufacturer + ", nState=" + nState
				+ ", dOpeningDate=" + dOpeningDate + ", dCloseDate=" + dCloseDate + ", dUpdateTime=" + dUpdateTime
				+ ", nDealerIDWeb=" + nDealerIDWeb + ", sLongitude=" + sLongitude + ", sLatitude=" + sLatitude
				+ ", sControllingShareholder=" + sControllingShareholder + ", sOtherShareholders=" + sOtherShareholders
				+ ", sStatus=" + sStatus + ", sRemarks=" + sRemarks + "]";
	}
	
}
 