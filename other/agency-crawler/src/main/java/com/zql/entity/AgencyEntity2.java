package com.zql.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class AgencyEntity2 {
	private List<Integer> nDealerID;
	private List<String> sDealerName;
	private Integer nBrandID;//品牌
	private String sBrand;
	private List<String> sProvince;
	private List<String> sCity;
	private List<String> sAddress;
	private List<String> sSaleCall;
	private List<String> sCustomerServiceCall;
	private List<String> sDealerType;
	private Integer nManufacturerID;//厂商
	private String sManufacturer;
	private List<Integer> nState;
	private Date dOpeningDate;
	private Date dCloseDate;
	private Timestamp dUpdateTime;
	private List<Integer> nDealerIDWeb;
	private List<String> sLongitude; // 经度
	private List<String> sLatitude; // 纬度
	private String sControllingShareholder; // 控股方
	private String sOtherShareholders; //其他股东
	private String sStatus;
	private String sRemarks;
	
	
	public List<Integer> getnDealerID() {
		return nDealerID;
	}
	public void setnDealerID(List<Integer> nDealerID) {
		this.nDealerID = nDealerID;
	}
	public  List<String> getsDealerName() {
		return sDealerName;
	}
	public void setsDealerName(List<String> sDealerName) {
		this.sDealerName = sDealerName;
	}
	public Integer getnBrandID() {
		return nBrandID;
	}
	public void setnBrandID(Integer nBrandID) {
		this.nBrandID = nBrandID;
	}
	public String getsBrand() {
		return sBrand;
	}
	public void setsBrand(String sBrand) {
		this.sBrand = sBrand;
	}
	public List<String> getsProvince() {
		return sProvince;
	}
	public void setsProvince(List<String> sProvince) {
		this.sProvince = sProvince;
	}
	public List<String> getsCity() {
		return sCity;
	}
	public void setsCity(List<String> sCity) {
		this.sCity = sCity;
	}
	public List<String> getsAddress() {
		return sAddress;
	}
	public void setsAddress(List<String> sAddress) {
		this.sAddress = sAddress;
	}
	public List<String> getsSaleCall() {
		return sSaleCall;
	}
	public void setsSaleCall(List<String> sSaleCall) {
		this.sSaleCall = sSaleCall;
	}
	public List<String> getsCustomerServiceCall() {
		return sCustomerServiceCall;
	}
	public void setsCustomerServiceCall(List<String> sCustomerServiceCall) {
		this.sCustomerServiceCall = sCustomerServiceCall;
	}
	public List<String> getsDealerType() {
		return sDealerType;
	}
	public void setsDealerType(List<String> sDealerType) {
		this.sDealerType = sDealerType;
	}
	public Integer getnManufacturerID() {
		return nManufacturerID;
	}
	public void setnManufacturerID(Integer nManufacturerID) {
		this.nManufacturerID = nManufacturerID;
	}
	public String getsManufacturer() {
		return sManufacturer;
	}
	public void setsManufacturer(String sManufacturer) {
		this.sManufacturer = sManufacturer;
	}
	public List<Integer> getnState() {
		return nState;
	}
	public void setnState(List<Integer> nState) {
		this.nState = nState;
	}
	public Date getdOpeningDate() {
		return dOpeningDate;
	}
	public void setdOpeningDate(Date dOpeningDate) {
		this.dOpeningDate = dOpeningDate;
	}
	public Date getdCloseDate() {
		return dCloseDate;
	}
	public void setdCloseDate(Date dCloseDate) {
		this.dCloseDate = dCloseDate;
	}
	public Timestamp getdUpdateTime() {
		return dUpdateTime;
	}
	public void setdUpdateTime(Timestamp dUpdateTime) {
		this.dUpdateTime = dUpdateTime;
	}
	public List<Integer> getnDealerIDWeb() {
		return nDealerIDWeb;
	}
	public void setnDealerIDWeb(List<Integer> nDealerIDWeb) {
		this.nDealerIDWeb = nDealerIDWeb;
	}
	public List<String> getsLongitude() {
		return sLongitude;
	}
	public void setsLongitude(List<String> sLongitude) {
		this.sLongitude = sLongitude;
	}
	public List<String> getsLatitude() {
		return sLatitude;
	}
	public void setsLatitude(List<String> sLatitude) {
		this.sLatitude = sLatitude;
	}
	public String getsControllingShareholder() {
		return sControllingShareholder;
	}
	public void setsControllingShareholder(String sControllingShareholder) {
		this.sControllingShareholder = sControllingShareholder;
	}
	public String getsOtherShareholders() {
		return sOtherShareholders;
	}
	public void setsOtherShareholders(String sOtherShareholders) {
		this.sOtherShareholders = sOtherShareholders;
	}
	public String getsStatus() {
		return sStatus;
	}
	public void setsStatus(String sStatus) {
		this.sStatus = sStatus;
	}
	public String getsRemarks() {
		return sRemarks;
	}
	public void setsRemarks(String sRemarks) {
		this.sRemarks = sRemarks;
	}
	
	
	@Override
	public String toString() {
		return "AgencyEntity2 [nDealerID=" + nDealerID + ", sDealerName=" + sDealerName + ", nBrandID=" + nBrandID
				+ ", sBrand=" + sBrand + ", sProvince=" + sProvince + ", sCity=" + sCity + ", sAddress=" + sAddress
				+ ", sSaleCall=" + sSaleCall + ", sCustomerServiceCall=" + sCustomerServiceCall + ", sDealerType="
				+ sDealerType + ", nManufacturerID=" + nManufacturerID + ", sManufacturer=" + sManufacturer
				+ ", nState=" + nState + ", dOpeningDate=" + dOpeningDate + ", dCloseDate=" + dCloseDate
				+ ", dUpdateTime=" + dUpdateTime + ", nDealerIDWeb=" + nDealerIDWeb + ", sLongitude=" + sLongitude
				+ ", sLatitude=" + sLatitude + ", sControllingShareholder=" + sControllingShareholder
				+ ", sOtherShareholders=" + sOtherShareholders + ", sStatus=" + sStatus + ", sRemarks=" + sRemarks
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sDealerName == null) ? 0 : sDealerName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyEntity2 other = (AgencyEntity2) obj;
		if (sDealerName == null) {
			if (other.sDealerName != null)
				return false;
		} else if (!sDealerName.equals(other.sDealerName))
			return false;
		return true;
	}
	
	
	
}