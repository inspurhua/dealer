package com.zql.entity;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.zql.Main.GongJu;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年10月10日 下午4:45:40 
* 类说明 
*/
public class AgencyDao {
	private static PreparedStatement ps;
	public void add(AgencyEntity2 agencyEntity){
		try {
			GongJu.getDao();
			String sql = "INSERT INTO \"201906.tDealer\"(\"sDealerName\","
					+ "\"sBrand\",\"sProvince\",\"sCity\",\"sAddress\",\"sSaleCall\","
					+ "\"sCustomerServiceCall\",\"sDealerType\",\"sManufacturer\",\"dUpdateTime\","
					+ "\"sLongitude\",\"sLatitude\")"
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = GongJu.conn.prepareStatement(sql);
			for(int i=0;i<agencyEntity.getsDealerName().size();i++) {
			ps.setString(1, agencyEntity.getsDealerName().get(i));
			ps.setString(2, agencyEntity.getsBrand());
			ps.setString(3, agencyEntity.getsProvince().get(i));
			ps.setString(4, agencyEntity.getsCity().get(i));
			ps.setString(5, agencyEntity.getsAddress().get(i));
			ps.setString(6, agencyEntity.getsSaleCall().get(i));
			ps.setString(7, agencyEntity.getsCustomerServiceCall().get(i));
			ps.setString(8, agencyEntity.getsDealerType().get(i));
			ps.setString(9, agencyEntity.getsManufacturer());
			ps.setTimestamp(10, agencyEntity.getdUpdateTime());
			ps.setString(11, agencyEntity.getsLongitude().get(i));
			ps.setString(12, agencyEntity.getsLatitude().get(i));
			ps.addBatch();
			//ps.executeUpdate();
			
			}
			ps.executeBatch();
			System.out.println("存储完成！");
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(ps !=null){
					ps.close();
				}
				if(GongJu.stmt != null){
					GongJu.stmt.close();
				}
				if(GongJu.conn != null){
					GongJu.conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
 