package com.zql.entity;

import java.sql.Date;
import java.sql.Timestamp;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年12月3日 上午10:14:35 
* 类说明 
*/
public class AgencyEntity {
	private Integer nDealerID;
	private String sDealerName;
	private Integer nBrandID;//品牌
	private String sBrand;
	private String sProvince;
	private String sCity;
	private String sAddress;
	private String sSaleCall;
	private String sCustomerServiceCall;
	private String sDealerType;
	private Integer nManufacturerID;//厂商
	private String sManufacturer;
	private Integer nState;
	private Date dOpeningDate;
	private Date dCloseDate;
	private Timestamp dUpdateTime;
	private Integer nDealerIDWeb;
	private String sLongitude; // 经度
	private String sLatitude; // 纬度
	private String sControllingShareholder; // 控股方
	private String sOtherShareholders; //其他股东
	private String sStatus;
	private String sRemarks;
	
	
	
	public String getsRemarks() {
		return sRemarks;
	}
	public void setsRemarks(String sRemarks) {
		this.sRemarks = sRemarks;
	}
	public String getsLongitude() {
		return sLongitude;
	}
	public void setsLongitude(String sLongitude) {
		this.sLongitude = sLongitude;
	}
	public String getsLatitude() {
		return sLatitude;
	}
	public void setsLatitude(String sLatitude) {
		this.sLatitude = sLatitude;
	}
	public String getsControllingShareholder() {
		return sControllingShareholder;
	}
	public void setsControllingShareholder(String sControllingShareholder) {
		this.sControllingShareholder = sControllingShareholder;
	}
	public String getsOtherShareholders() {
		return sOtherShareholders;
	}
	public void setsOtherShareholders(String sOtherShareholders) {
		this.sOtherShareholders = sOtherShareholders;
	}
	public String getsStatus() {
		return sStatus;
	}
	public void setsStatus(String sStatus) {
		this.sStatus = sStatus;
	}
	public Integer getnDealerID() {
		return nDealerID;
	}
	public void setnDealerID(Integer nDealerID) {
		this.nDealerID = nDealerID;
	}
	public String getsDealerName() {
		return sDealerName;
	}
	public void setsDealerName(String sDealerName) {
		this.sDealerName = sDealerName;
	}
	public Integer getnBrandID() {
		return nBrandID;
	}
	public void setnBrandID(Integer nBrandID) {
		this.nBrandID = nBrandID;
	}
	public String getsBrand() {
		return sBrand;
	}
	public void setsBrand(String sBrand) {
		this.sBrand = sBrand;
	}
	public String getsProvince() {
		return sProvince;
	}
	public void setsProvince(String sProvince) {
		this.sProvince = sProvince;
	}
	public String getsCity() {
		return sCity;
	}
	public void setsCity(String sCity) {
		this.sCity = sCity;
	}
	public String getsAddress() {
		return sAddress;
	}
	public void setsAddress(String sAddress) {
		this.sAddress = sAddress;
	}
	public String getsSaleCall() {
		return sSaleCall;
	}
	public void setsSaleCall(String sSaleCall) {
		this.sSaleCall = sSaleCall;
	}
	public String getsCustomerServiceCall() {
		return sCustomerServiceCall;
	}
	public void setsCustomerServiceCall(String sCustomerServiceCall) {
		this.sCustomerServiceCall = sCustomerServiceCall;
	}
	public String getsDealerType() {
		return sDealerType;
	}
	public void setsDealerType(String sDealerType) {
		this.sDealerType = sDealerType;
	}
	public Integer getnManufacturerID() {
		return nManufacturerID;
	}
	public void setnManufacturerID(Integer nManufacturerID) {
		this.nManufacturerID = nManufacturerID;
	}
	public String getsManufacturer() {
		return sManufacturer;
	}
	public void setsManufacturer(String sManufacturer) {
		this.sManufacturer = sManufacturer;
	}
	public Integer getnState() {
		return nState;
	}
	public void setnState(Integer nState) {
		this.nState = nState;
	}
	public Date getdOpeningDate() {
		return dOpeningDate;
	}
	public void setdOpeningDate(Date dOpeningDate) {
		this.dOpeningDate = dOpeningDate;
	}
	public Date getdCloseDate() {
		return dCloseDate;
	}
	public void setdCloseDate(Date dCloseDate) {
		this.dCloseDate = dCloseDate;
	}
	public Timestamp getdUpdateTime() {
		return dUpdateTime;
	}
	public void setdUpdateTime(Timestamp dUpdateTime) {
		this.dUpdateTime = dUpdateTime;
	}
	public Integer getnDealerIDWeb() {
		return nDealerIDWeb;
	}
	public void setnDealerIDWeb(Integer nDealerIDWeb) {
		this.nDealerIDWeb = nDealerIDWeb;
	}
	
	
	@Override
	public String toString() {
		return "AgencyEntity [nDealerID=" + nDealerID + ", sDealerName=" + sDealerName + ", nBrandID=" + nBrandID
				+ ", sBrand=" + sBrand + ", sProvince=" + sProvince + ", sCity=" + sCity + ", sAddress=" + sAddress
				+ ", sSaleCall=" + sSaleCall + ", sCustomerServiceCall=" + sCustomerServiceCall + ", sDealerType="
				+ sDealerType + ", nManufacturerID=" + nManufacturerID + ", sManufacturer=" + sManufacturer
				+ ", nState=" + nState + ", dOpeningDate=" + dOpeningDate + ", dCloseDate=" + dCloseDate
				+ ", dUpdateTime=" + dUpdateTime + ", nDealerIDWeb=" + nDealerIDWeb + ", sLongitude=" + sLongitude
				+ ", sLatitude=" + sLatitude + ", sControllingShareholder=" + sControllingShareholder
				+ ", sOtherShareholders=" + sOtherShareholders + ", sStatus=" + sStatus + ", sRemarks=" + sRemarks
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sDealerName == null) ? 0 : sDealerName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyEntity other = (AgencyEntity) obj;
		if (sDealerName == null) {
			if (other.sDealerName != null)
				return false;
		} else if (!sDealerName.equals(other.sDealerName))
			return false;
		return true;
	}
	
	
	
}