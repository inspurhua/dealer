package dao;

import com.zql.entity.AgencyEntity;

/**
 * Mapper映射器
 * @author bowan
 *
 */
public interface AgencyDaoInterface {
	public void save(AgencyEntity agency);
}
