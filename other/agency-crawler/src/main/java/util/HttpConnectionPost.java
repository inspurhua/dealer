package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpConnectionPost {
	public static void main(String[] args) {
		String path = "http://www.faw-hongqi.com.cn/Common/Data/mapData";
		String param = "ajax=1&class_id=1&city_name=济南市&pcount=1&kw=";
		String json = getJson(path, param);
		System.out.println(json);
	}
	/**
	 * 
	 * @param path
	 * @param param 示例:"province=1810&city=1896"
	 * @return
	 */
	public static String getJson(String path, String param) {
		
		PrintWriter pw = null;
		BufferedReader br = null;
		try {
			//String param = "province=1810&city=1896";
			//String path = "http://www.zznissan.com.cn/index.php/support/ajax_jxs_point";
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			//设置参数
			conn.setConnectTimeout(600000000);
			conn.setReadTimeout(600000000);
			conn.setDoOutput(true);//需要输出
			conn.setDoInput(true);//需要输入
			conn.setUseCaches(false);//不允许缓存
			conn.setRequestMethod("POST");//设置请求方式,默认为get
			//设置通用属性
			conn.setRequestProperty("Connection", "Keep-Alive");// 维持长连接
			conn.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
			conn.connect();//可以省了,使用下面的urlConn.getOutputStream()会自动connect
			//建立输出流，向指向的URL传入参数
			pw = new PrintWriter(conn.getOutputStream());
			pw.print(param);
			pw.flush();
			//建立输入流,读取返回的信息
			br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null; // 每行内容
			StringBuffer content = new StringBuffer();
			while ((line = br.readLine()) != null) {
				content.append(line);
			}
			//System.out.println(content);
			return content.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(pw != null) {
				pw.close();
			}
		}
		
		return null;
		
	}
}
