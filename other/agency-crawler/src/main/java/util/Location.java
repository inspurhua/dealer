package util;

import java.util.Arrays;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
/**
 * 根据地址,获取经纬度
 * @author bowan
 *
 */
public class Location {
	/**
	 * 
	 * @param str地址
	 * @return 数组第一个数是经度(lng),第二个是纬度(lat).如果查询失败,数组里是/斜杠.
	 */
	public static String[] getLocation(String address) {
		address = address.replaceAll("\\s*", "");
		String url = "http://api.map.baidu.com/geocoder/v2/?address="+address+"&output=json&ak=7X8cnmoaY5F8wAOMiSzYG7fSG4aFoGaF";
		String json = HttpConnectionGet.getJson(url);
		//System.out.println(json);
		JSONObject data = JSON.parseObject(json);
		int status = 0;
		try {
			status = Integer.parseInt(data.getString("status"));

		} catch(Exception e) {
			System.out.println(address);
		}
		String[] lngAndLat = new String[]{"/", "/"};
		if (status == 0) {
			JSONObject lacation = data.getJSONObject("result").getJSONObject("location");
			lngAndLat[0] = lacation.getString("lng");
			lngAndLat[1] = lacation.getString("lat");
		}
		
		return lngAndLat;
	}
	public static void main(String[] args) {
		System.out.println(Arrays.toString(getLocation("吉首市峒河办事处大田社区武陵东路93号（大田湾加油站对面）")));
	}
}
