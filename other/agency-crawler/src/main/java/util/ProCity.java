package util;

import java.util.Arrays;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class ProCity {
	/**
	 * 
	 * @param location "36.2332,117.32424"的格式
	 * @return 返回String数组,第一个值为省,第二值为市
	 */
	public static String[] getData(String location) {
		String path = "http://api.map.baidu.com/geocoder/v2/?location="+location+"&output=json&pois=1&ak=Ex2IEtIUpuek8ATzZ4FG2MPGxE7uyUvx";
		String json = HttpConnectionGet.getJson(path);
		return getProCity(json);
	}
	/**
	 * 
	 * @param lngAndLat 数组里第一参数为经度(117.2424),第二个参数为纬度(36.23423)
	 * @return 返回String数组,第一个值为省,第二值为市
	 */
	public static String[] getData(String[] lngAndLat) {
		String location = String.format("%s,%s", lngAndLat[1],lngAndLat[0]);
		return getData(location);
	}
	/**
	 * 
	 * @param lat 纬度
	 * @param lng 经度
	 * @return 返回String数组,第一个值为省,第二值为市
	 */
	public static String[] getData(String lat, String lng) {
		lat = lat.replaceAll("\\s*", "");
		lng = lng.replaceAll("\\s*", "");
		String location = String.format("%s,%s", lat,lng);
		return getData(location);
	}
	
	static String[] getProCity(String json) {
		String[] proCity = new String[2];
		JSONObject jsonObject = JSON.parseObject(json);
		if ("0".equals(jsonObject.getString("status"))) {
			JSONObject addressComponent = jsonObject.getJSONObject("result").getJSONObject("addressComponent");
			proCity[0] = addressComponent.getString("province");
			proCity[1] = addressComponent.getString("city");
		}
		return proCity;
	}
	public static void main(String[] args) {
		String str = "{\"status\":0,\"result\":{\"location\":{\"lng\":121.48041699999992,\"lat\":31.349300884114397},\"formatted_address\":\"上海市宝山区长江西路600号\",\"business\":\"淞南,长江西路,泗塘新村\",\"addressComponent\":{\"country\":\"中国\",\"country_code\":0,\"country_code_iso\":\"CHN\",\"country_code_iso2\":\"CN\",\"province\":\"上海市\",\"city\":\"上海市\",\"city_level\":2,\"district\":\"宝山区\",\"town\":\"\",\"adcode\":\"310113\",\"street\":\"长江西路\",\"street_number\":\"600号\",\"direction\":\"附近\",\"distance\":\"32\"},\"pois\":[{\"addr\":\"宝山区长江西路600号(近逸仙路)\",\"cp\":\" \",\"direction\":\"附近\",\"distance\":\"30\",\"name\":\"上海东昌英菲尼迪汽车销售服务有限公司\",\"poiType\":\"汽车服务\",\"point\":{\"x\":121.48057596019408,\"y\":31.3491079970234},\"tag\":\"汽车服务;汽车销售\",\"tel\":\"\",\"uid\":\"6fd7744b84aa6059ad080ffe\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"长江西路581弄10临\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"88\",\"name\":\"上海沪一管业有限公司仓储\",\"poiType\":\"公司企业\",\"point\":{\"x\":121.48008189216358,\"y\":31.349917520240955},\"tag\":\"公司企业\",\"tel\":\"\",\"uid\":\"c077b32ac73837d29122e07f\",\"zip\":\"\",\"parent_poi\":{\"name\":\"沪一管业\",\"tag\":\"公司企业;公司\",\"addr\":\"淞南镇长江西路581弄58号\",\"point\":{\"x\":121.4795878241331,\"y\":31.351012292827915},\"direction\":\"东南\",\"distance\":\"240\",\"uid\":\"6b134dc971d8b6d616c90595\"}},{\"addr\":\"长江西路585号上海新狮商务楼3层317室\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"92\",\"name\":\"上海同鑫汽车销售服务有限公司\",\"poiType\":\"汽车服务\",\"point\":{\"x\":121.48006392605339,\"y\":31.349948359081528},\"tag\":\"汽车服务;汽车销售\",\"tel\":\"\",\"uid\":\"05cc627b0d212808b21e3619\",\"zip\":\"\",\"parent_poi\":{\"name\":\"上海新狮商务楼\",\"tag\":\"房地产;写字楼\",\"addr\":\"长江西路585号\",\"point\":{\"x\":121.4796057902433,\"y\":31.34986355224536},\"direction\":\"东南\",\"distance\":\"116\",\"uid\":\"e96b4420ed6a592783288ab3\"}},{\"addr\":\"上海市宝山区长江西路583号\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"102\",\"name\":\"上海新锦储运有限公司\",\"poiType\":\"公司企业\",\"point\":{\"x\":121.4797674852351,\"y\":31.34985584252916},\"tag\":\"公司企业;公司\",\"tel\":\"\",\"uid\":\"69fcd6dfb9e51451cb2915bb\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"上海市宝山区长江西路\",\"cp\":\" \",\"direction\":\"西\",\"distance\":\"111\",\"name\":\"保利悦庭\",\"poiType\":\"房地产\",\"point\":{\"x\":121.48135748598776,\"y\":31.349593711798325},\"tag\":\"房地产;住宅区\",\"tel\":\"\",\"uid\":\"efd1f58760be54d3441a32a9\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"长江西路585号\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"116\",\"name\":\"上海新狮商务楼\",\"poiType\":\"房地产\",\"point\":{\"x\":121.4796057902433,\"y\":31.34986355224536},\"tag\":\"房地产;写字楼\",\"tel\":\"\",\"uid\":\"e96b4420ed6a592783288ab3\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"上海市宝山区长江西路611号\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"126\",\"name\":\"新锦通杂货店\",\"poiType\":\"购物\",\"point\":{\"x\":121.4794171460862,\"y\":31.34976332588487},\"tag\":\"购物;商铺\",\"tel\":\"\",\"uid\":\"236c553c1461bfce0b69d9f9\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"上海市宝山区淞南镇上海玻璃博物馆上海新狮商务楼\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"126\",\"name\":\"上海吉吉毅食品贸易有限公司\",\"poiType\":\"公司企业\",\"point\":{\"x\":121.4794171460862,\"y\":31.34976332588487},\"tag\":\"公司企业;公司\",\"tel\":\"\",\"uid\":\"3debb6c42046651a213bf237\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"上海市宝山区长江西路585号\",\"cp\":\" \",\"direction\":\"东南\",\"distance\":\"135\",\"name\":\"柚紫养车（宝山旗舰店）\",\"poiType\":\"汽车服务\",\"point\":{\"x\":121.4797854513453,\"y\":31.35019506943853},\"tag\":\"汽车服务\",\"tel\":\"\",\"uid\":\"96cbdff471a5cdf548cc4ace\",\"zip\":\"\",\"parent_poi\":{\"name\":\"\",\"tag\":\"\",\"addr\":\"\",\"point\":{\"x\":0.0,\"y\":0.0},\"direction\":\"\",\"distance\":\"\",\"uid\":\"\"}},{\"addr\":\"长江西路685号玻璃博物馆2楼（近钢四路）\",\"cp\":\" \",\"direction\":\"东\",\"distance\":\"144\",\"name\":\"玻璃博物馆-咖啡厅\",\"poiType\":\"美食\",\"point\":{\"x\":121.4791835866536,\"y\":31.349639970216207},\"tag\":\"美食;咖啡厅\",\"tel\":\"\",\"uid\":\"925c550fec0373ab5a3ac8c2\",\"zip\":\"\",\"parent_poi\":{\"name\":\"上海玻璃博物馆\",\"tag\":\"旅游景点;博物馆\",\"addr\":\"宝山区长江西路685号(近钢四路)\",\"point\":{\"x\":121.47830324725384,\"y\":31.349917520240955},\"direction\":\"东\",\"distance\":\"248\",\"uid\":\"417e3d204bbd2380ee74f8d2\"}}],\"roads\":[],\"poiRegions\":[],\"sematic_description\":\"上海东昌英菲尼迪汽车销售服务有限公司附近30米\",\"cityCode\":289}}\r\n" + 
				"";
		String[] s = getProCity(str);
		System.out.println(Arrays.toString(s));
	}
}
