package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBTool {
	private static String driver;
	private static String url;
	private static String user;
	private static String pwd;

	static {

		try {
			driver = "org.postgresql.Driver";
			// url="jdbc:postgresql://120.27.7.184:5432/changshuai";
			url = "jdbc:postgresql://127.0.0.1:5432/postgres";
			user = "postgres";
			// pwd = "postgres";
			pwd = "123456";
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("加载驱动类失败", e);

		}
	}

	public static Connection getConnection() throws SQLException {

		return DriverManager.getConnection(url, user, pwd);
	}

	public static void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("关闭连接失败", e);
			}
		}

	}

}
