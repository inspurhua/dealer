package util;
/**
 * 根据经纬度,获取此处的详细信息
 * @author bowan
 *
 */
public class AddressComponent {
	/**
	 * 
	 * @param location "36.2332,117.32424"的格式
	 * @return 返回json格式字符串
	 */
	public static String getData(String location) {
		String path = "http://api.map.baidu.com/geocoder/v2/?location="+location+"&output=json&pois=1&ak=Ex2IEtIUpuek8ATzZ4FG2MPGxE7uyUvx";
		String json = HttpConnectionGet.getJson(path);
		
		return json;
	}
	/**
	 * 
	 * @param lngAndLat 数组里第一参数为纬度(36.23423),第二个参数为经度(117.2424)
	 * @return json格式字符串
	 */
	public static String getData(String[] lngAndLat) {
		String location = String.format("%s,%s", lngAndLat[1],lngAndLat[0]);
		return getData(location);
	}
	/**
	 * 
	 * @param lat 纬度
	 * @param lng 经度
	 * @return json格式字符串
	 */
	public static String getData(String lat, String lng) {
		lat = lat.replaceAll("\\s*", "");
		lng = lng.replaceAll("\\s*", "");
		String location = String.format("%s,%s", lat,lng);
		return getData(location);
	}
	
	public static void main(String[] args) {
		System.out.println(getData(new String[]{"117.343","36.4354"}));
	}
}
