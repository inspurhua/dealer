package crawler.maikailun;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.zql.entity.AgencyEntity;

import util.Location;
import util.MybatisTool;
import util.ProCity;

public class MaiKaiLun {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String home = "http://www.mclarencars.cn/retailers";
		try {
			Document homeDoc = Jsoup.connect(home).get();
			Element tbody = homeDoc.getElementsByTag("tbody").get(0);
			Elements links = tbody.getElementsByTag("a");
			//System.out.println(links);
			for (Element link : links) {
				String dealerUrl = link.attr("href");
				Document dealerDoc = Jsoup.connect(dealerUrl).followRedirects(true).get();
				//System.out.println(dealerDoc);
				String pd = dealerDoc.getElementsByClass("centered").toString();
				if(!pd.equals("")) {
				String name = dealerDoc.getElementsByClass("centered").get(0).text();
				Element dealerInfo = dealerDoc.getElementsByClass("col-sm-5 col-md-5").get(0);
				Elements ps = dealerInfo.getElementsByTag("p");
				boolean addressFlag = true;
				boolean sellTellFlag = true;
				String address = null;
				String sellTell = null;
				for (Element ele : ps) {
					String str = ele.text();
					if (str.contains("地址") && addressFlag) {
						address = str.split("：")[1];
						addressFlag = false;
					}
					if (str.contains("电话") && sellTellFlag) {
						sellTell = str.split("：")[1];
						sellTellFlag = false;
					}

				}
				String[] loca = Location.getLocation(address);
				String lng = loca[0];
				String lat = loca[1];
				String[] proCity = ProCity.getData(loca);
				String proName = proCity[0];
				String cityName = proCity[1];

				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("迈凯伦");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("迈凯伦");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				//System.out.println(agency);
				list.add(agency);
			}else {}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void main(String[] args) {
		MaiKaiLun crawler = new MaiKaiLun();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
