package crawler.lufeng;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;
//异常
public class JiangLingQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://log.land-wind.dma.cig.com.cn/lms_api/gather/get_dealer_list";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		JSONArray dealers = JSON.parseObject(dealerText).getJSONArray("data");
		for (int k = 0; k < dealers.size(); k++) {
			JSONObject dealer = dealers.getJSONObject(k);
			String name = dealer.getString("dea_name");
			String address = dealer.getString("dea_address");
			String sellTell = dealer.getString("dea_sale_phone");
			String proName = dealer.getString("p_name");
			String cityName = dealer.getString("c_name");
			String lng = "";
			String lat = "";
			if (dealer.getString("dea_place")!=null) {
				String[] lngAndLat = dealer.getString("dea_place").split(",");
				if (lngAndLat.length==2) {
					lng = lngAndLat[0];
					lat = lngAndLat[1];
					
				} else {
					lngAndLat = Location.getLocation(address);
					lng = lngAndLat[0];
					lat = lngAndLat[1];
				}
			}
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);

			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("陆风");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("江铃汽车");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);

		}

		return list;
	}

	public static void main(String[] args) {
		JiangLingQiCheCrawler crawler = new JiangLingQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
