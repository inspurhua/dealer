package crawler.lufeng;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年10月12日 上午11:10:28 
* 类说明 
*/
public class JiangLingQiChePageProcess implements PageProcessor {
private static final String purl = "http://dma.landwind.com/lms/gather/get_province_list\\?jsoncallback=jQuery1510870283079558898_1539313957688&_=1539313958625";
private static final String curl = "http://dma.landwind.com/lms/gather/get_city_list\\?jsoncallback=jQuery15106873755054333807_1539315615662&p_id=[0-9]{1,10}&_=1539315679391";
	@Override
	public void process(Page page) {
		if(page.getUrl().regex(purl).match()) {
		String nn =page.getJson().regex("\\(([\\s\\S]*)\\)").get();
		List<String> pid = JsonPath.read(nn, "$.data[*].id");
		System.out.println(pid);
		List<String> url = new ArrayList<>();
		for(int i=0;i<pid.size();i++) {
			url.add("http://dma.landwind.com/lms/gather/get_city_list?jsoncallback=jQuery15106873755054333807_1539315615662&p_id="+pid.get(i)+"&_=1539315679391");
		}
		page.addTargetRequests(url);
		}else if(page.getUrl().regex(curl).match()) {
			String nn =page.getJson().regex("\\(([\\s\\S]*)\\)").get();
			List<String> cid = JsonPath.read(nn, "$.data[*].id");
			System.out.println(cid);
			List<String> url = new ArrayList<>();
			for(int i=0;i<cid.size();i++) {
				url.add("http://dma.landwind.com/lms/gather/get_dealer_list?jsoncallback=jQuery15106873755054333807_1539315615669&cityid="+cid.get(i)+"&_=1539315841340");
			}
			page.addTargetRequests(url);
		}
		else {
		String nn =page.getJson().regex("\\(([\\s\\S]*)\\)").get();
		List<String> name = JsonPath.read(nn, "$.data[*].dea_name");
		List<String> address = JsonPath.read(nn, "$.data[*].dea_address");
		List<String> sellTell = JsonPath.read(nn, "$.data[*].dea_sale_phone");
		List<String> proName = JsonPath.read(nn, "$.data[*].p_name");
		List<String> cityName = JsonPath.read(nn, "$.data[*].c_name");
		List<String> coordinates = JsonPath.read(nn, "$.data[*].dea_place");
		List<String> lng = new ArrayList<>();
		List<String> lat = new ArrayList<>();
		for(int i=0;i<coordinates.size();i++) {
			if(coordinates.get(i)!=null&&coordinates.get(i).contains(",")) {
				String[] jwd = coordinates.get(i).split(",");
					lng.add(jwd[0]);
					lat.add(jwd[1]);
			}else if(coordinates.get(i)!=null&&coordinates.get(i).contains("，")) {
				String[] jwd = coordinates.get(i).split("，");
					lng.add(jwd[0]);
					lat.add(jwd[1]);
			}else{
				lng.add("");
				lat.add("");
			}
			
		}
		//System.out.println(lng.size()+"=="+lng);
		//System.out.println(lat.size()+"=="+lat);
		List<String> dnull = new ArrayList<>();
		for(int i=0;i<name.size();i++) {
			dnull.add("");
		}
			AgencyEntity2 agency = new AgencyEntity2();
			//agency.setdCloseDate(null);
			//agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			//agency.setnBrandID(-1);
			agency.setsBrand("陆风");
			//agency.setnDealerIDWeb(-1);
			//agency.setnManufacturerID(-1);
			agency.setsManufacturer("江铃汽车");
			//agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(dnull);
			agency.setsDealerName(name);
			agency.setsDealerType(dnull);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			//agency.setsControllingShareholder(null);
			//agency.setsOtherShareholders(null);
			new AgencyDao().add(agency);
		//}
	}
}
	private static Site site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(3)
			;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new JiangLingQiChePageProcess())
	.addUrl("http://dma.landwind.com/lms/gather/get_province_list?jsoncallback=jQuery1510870283079558898_1539313957688&_=1539313958625")
	//.addUrl("http://dma.landwind.com/lms/gather/get_city_list?jsoncallback=jQuery15106873755054333807_1539315615662&p_id=210000&_=1539315679391")
	//.addUrl("http://dma.landwind.com/lms/gather/get_dealer_list?jsoncallback=jQuery15106873755054333807_1539315615669&cityid=130100&_=1539315841340")
	.run();
}
}
 