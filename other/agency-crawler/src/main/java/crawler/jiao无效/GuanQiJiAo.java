package crawler.jiao无效;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;
//无结果
public class GuanQiJiAo {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getCity = "http://www.gacgonow.com.cn/map/dealermap.jsp";
		String jsContent = HttpConnectionGet.getJson(getCity);
		
		int start = jsContent.indexOf("var markersByArea");
		int end = jsContent.indexOf("];", start)+2;
		jsContent = jsContent.substring(start, end).replaceAll("\\s*", "");
		int start2 = jsContent.indexOf("[{");
		int end2 = jsContent.lastIndexOf("},]")+3;
		jsContent = jsContent.substring(start2, end2);
		JSONArray dealers = JSON.parseArray(jsContent);
		for (int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String name = dealer.getString("name");
			String address = dealer.getString("address");
			String sellTell = dealer.getString("phone");
			String proName = dealer.getString("province");
			String cityName = dealer.getString("city");
			String lng = dealer.getString("lng");
			String lat = dealer.getString("lat");
			

			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("吉奥");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("广汽吉奥");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			System.out.println(agency);
			list.add(agency);

		}
		
		return list;
	}
	public static void main(String[] args) {
		GuanQiJiAo crawler = new GuanQiJiAo();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}
}
