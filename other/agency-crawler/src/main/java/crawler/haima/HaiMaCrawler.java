package crawler.haima;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.Location;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class HaiMaCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String home = "http://www.ihaima.com/#;pop=sales_search";
		try {
			Document doc = Jsoup.connect(home).get();
			Elements divs = doc.getElementsByClass("sales_search_addr_detail_list");
			for(int i=0;i<divs.size();i++) {
				String proName = divs.get(i).text();
				
				int page = 1;
				while(true) {
					String getDealer = "http://www.ihaima.com/ReadHandler.ashx?ModelName=VNJni2&t=1&pn="+proName+"&PageIndex="+page;
					Document dealerDoc = Jsoup.connect(getDealer).get();
					Elements dealers = dealerDoc.getElementsByClass("sales_search_result_detail");
					//System.out.println(dealers);
					if(dealers.size()==0) {
						break;
					}
					for(int j=0;j<dealers.size();j++) {
						String name = dealers.get(j).getElementsByClass("txt1").text();
						String[] add = dealers.get(j).getElementsByClass("txt2").text().split("：");
						String address = "";
						if(add.length>1) {
							address = add[1];
						}
						String[] lngAndLat = Location.getLocation(address);
						String cityName = ProCity.getData(lngAndLat)[1];
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						
						
						String[] tell = dealers.get(j).getElementsByClass("txt3").text().split("：");
						String sellTell = null;
						if(tell.length>1) {
							sellTell = tell[1];
						}
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);

							
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("海马");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("海马汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					}
					page++;
					
					
				}
			
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	
	
	
	public static void main(String[] args) {
		HaiMaCrawler crawler = new HaiMaCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
