package crawler.dazhong;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class YiQiDaZhongCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://contact.faw-vw.com/uploadfiles/js/dealer.js";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		dealerText = dealerText.substring(dealerText.indexOf("["));
		//System.out.println(dealerText.indexOf("["));
		JSONArray dealerArray = JSON.parseArray(dealerText);
		for(int i=0;i<dealerArray.size();i++) {
			JSONObject dealer = dealerArray.getJSONObject(i);
			String name = dealer.getString("vd_dealerName");
			String address = dealer.getString("vd_address");
			String cityName = dealer.getString("vc_name");
			String proName = dealer.getString("vp_name").split(" ")[1];
			String sellTell = dealer.getString("vd_salePhone");
			String typeCode = dealer.getString("vd_dealerType");
			String lng = dealer.getString("vd_longitude");
			String lat = dealer.getString("vd_latitude");
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			
			String type = null;
			if("1".equals(typeCode)) {
				type = "Mini 4S店";
			} else if("2".equals(typeCode)) {
				type = "城市展厅";
			} else if ("3".equals(typeCode)) {
				type = "特许服务店";
			}
			
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("大众");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("一汽大众");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(type);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			//System.out.println(agency);
			list.add(agency);
			
		}
		return list;
	}
	public static void main(String[] args) {
		YiQiDaZhongCrawler crawler = new YiQiDaZhongCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
	}

}
