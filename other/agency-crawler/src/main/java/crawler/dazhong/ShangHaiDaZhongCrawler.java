package crawler.dazhong;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.MybatisTool;
import util.TianYanCha;
//官网失效
public class ShangHaiDaZhongCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			String getDealer = "http://club.svw-volkswagen.com/map/latlng.xml";
			//1.创建SAXReaer对象
			SAXReader reader = new SAXReader();
			//2.获取Document对象
			Document doc = reader.read(getDealer);
			//3.获取根元素
			Element root = doc.getRootElement();
			//4.获取需求元素
			List<Element> areas = root.elements("area");
			for(Element area : areas) {
				List<Element> provinces = area.elements("province");
				for(Element province : provinces) {
					String proName = province.attributeValue("name");
					List<Element> cities = province.elements("city");
					for(Element city : cities) {
						String cityName = city.attributeValue("name");
						List<Element> districts = city.elements("district");

						for(Element district : districts) {
							List<Element> companies = district.elements("company");
							for(Element company : companies) {
								String name = company.attributeValue("name");
								String type = company.attributeValue("type");
								String address = company.attributeValue("address");
								String sellTell = company.attributeValue("telservice");
								String serviceTell = company.attributeValue("tel24");
								String lng = company.attributeValue("lng");
								String lat = company.attributeValue("lat");
								List<String> shareholder = TianYanCha.getShareholder(name);
								String sControllingShareholder = shareholder.get(0);
								String sOtherShareholders = shareholder.get(1);
								
								
								AgencyEntity agency = new AgencyEntity();
								agency.setdCloseDate(null);
								agency.setdOpeningDate(null);
								agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
								agency.setnBrandID(-1);
								agency.setsBrand("大众");

								agency.setnDealerIDWeb(-1);
								agency.setnManufacturerID(-1);
								agency.setsManufacturer("上海大众");

								agency.setnState(1);
								agency.setsAddress(address);
								agency.setsCity(cityName);
								agency.setsCustomerServiceCall(serviceTell);
								agency.setsDealerName(name);
								agency.setsDealerType(type);
								agency.setsProvince(proName);
								agency.setsSaleCall(sellTell);
								agency.setsLongitude(lng);
								agency.setsLatitude(lat);
								agency.setsControllingShareholder(sControllingShareholder);
								agency.setsOtherShareholders(sOtherShareholders);
								list.add(agency);
								
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		return list;
	}
	public static void main(String[] args) {
		ShangHaiDaZhongCrawler crawler = new ShangHaiDaZhongCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
	}

}
