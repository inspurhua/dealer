package crawler.dazhong;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyEntity;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import util.MybatisTool;

public class ShangHaiDaZhongPageProcessor implements PageProcessor {
	private static List<AgencyEntity> list = new ArrayList<AgencyEntity>();
	@Override
	public void process(Page page) {
		//System.out.println(page.getHtml().get());
/*		List<String> qu = page.getHtml().xpath("html/body/data/area/@name").all();
		System.out.println(qu.size()+"=="+qu);*/
		List<String> proName = page.getHtml().xpath("html/body/data/province/@name").all();
		//System.out.println(sheng.size()+"=sheng="+sheng);
		for(int i=0;i<proName.size();i++) {
			List<String> cityName = page.getHtml().xpath("data/province["+(i+1)+"]/city/@name").all();
			//System.out.println(shi.size()+"=shi="+shi);
			for(int j=0;j<cityName.size();j++) {
				//System.out.println(proName.get(i));
				//System.out.println(cityName.get(j));
				List<String> name = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@name").all();
				//System.out.println(name.size()+"=name="+name);
				List<String> type = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@type").all();
				//System.out.println(type.size()+"=type="+type);
				List<String> address = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@address").all();
				//System.out.println(address.size()+"=address="+address);
				List<String> sellTell = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@telservice").all();
				//System.out.println(sellTell.size()+"=telservice="+sellTell);
				List<String> serviceTell = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@tel24").all();
				//System.out.println(serviceTell.size()+"=serviceTell="+serviceTell);
				List<String> lng = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@lng").all();
				//System.out.println(lng.size()+"=lng="+lng);
				List<String> lat = page.getHtml().xpath("data/province["+(i+1)+"]/city["+(j+1)+"]/district/company/@lat").all();
				//System.out.println(lat.size()+"=lat="+lat);
				for(int p=0;p<name.size();p++) {
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("大众");

				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("上海大众");

				agency.setnState(1);
				agency.setsAddress(address.get(p));
				agency.setsCity(cityName.get(j));
				agency.setsCustomerServiceCall(serviceTell.get(p));
				agency.setsDealerName(name.get(p));
				agency.setsDealerType(type.get(p));
				agency.setsProvince(proName.get(i));
				agency.setsSaleCall(sellTell.get(p));
				agency.setsLongitude(lng.get(p));
				agency.setsLatitude(lat.get(p));
				agency.setsControllingShareholder(null);
				agency.setsOtherShareholders(null);
				list.add(agency);
				}
			}
		}
	}
	private Site site = Site.me()
						.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0")
						;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ShangHaiDaZhongPageProcessor())
	.addUrl("http://club.svw-volkswagen.com/map/latlng.xml")
	.run();
	for(AgencyEntity agency : list) {
		MybatisTool.save(agency);
	}
	MybatisTool.close();
	System.out.println("请查看数据库");
}
}
