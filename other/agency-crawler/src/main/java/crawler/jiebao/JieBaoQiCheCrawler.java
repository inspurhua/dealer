package crawler.jiebao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class JieBaoQiCheCrawler {
	
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://dealer.jaguar.com.cn/index.php?s=/JDealer/api/getDealerList&is_extend=11";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		//System.out.println(dealerText);
		JSONArray dealers = JSON.parseObject(dealerText).getJSONArray("data");
		for(int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String name = dealer.getString("dealer_name");
			String address = dealer.getString("addr");
			String sellTell = dealer.getString("sales_phone_jaguar");
			String serviceTell = dealer.getString("service_phone_jaguar");
			String proName = dealer.getString("province_name");
			String cityName = dealer.getString("city_name");
			String lng = dealer.getString("longitude");
			String lat = dealer.getString("latitude");
			
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("捷豹");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("捷豹汽车");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			//agency.setsDealerType();
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsCustomerServiceCall(serviceTell);
			
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);
			
		}
		
		
		return list;
		
	}
/*	旧版弃用
	public List<AgencyEntity> getAgency1() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			//获取网页源代码
			//String getPage = "http://dealer.jaguar.seeinzen.com/";
			String getPage = "http://dealer.jaguar.com.cn/";
			Document doc = Jsoup.connect(getPage).get();
			//从省份的select中获取省份id和name并存入map
			Element proSelect = doc.getElementById("edit-province");
			Elements proOptions = proSelect.getElementsByTag("option");
			Map<String, String> proMap = new HashMap<String, String>();
			for(int i=0;i<proOptions.size();i++) {
				String proName = proOptions.get(i).text();
				String proNum = proOptions.get(i).attr("value");
				proMap.put(proNum, proName);
			}
			//从城市select中获取城市的id和name,并存入一个map中
			Element citySelect = doc.getElementById("edit-city");
			Elements cityOptions = citySelect.getElementsByTag("option");
			Map<String, String> cityMap = new HashMap<String, String>();
			for(int j=0;j<cityOptions.size();j++) {
				String cityName = cityOptions.get(j).text();
				String cityNum = cityOptions.get(j).attr("value");
				cityMap.put(cityNum, cityName);
				
			}
			//获取省和城市之间的关系
			Element div = doc.getElementById("cities-wrapper");
			Elements divs = div.children();
			//Elements divs = div.getElementsByTag("div");
			for(int k=0;k<divs.size();k++) {
				String proNum = divs.get(k).attr("id").split("-")[1];
				String proName = proMap.get(proNum);
				Elements cities = divs.get(k).children();
				for(int m=0;m<cities.size();m++) {
					String cityNum = cities.get(m).attr("id").split("-")[1];
					String cityName = cities.get(m).text();
					//System.out.println(proNum+":"+cityNum);
					String getDealer = "http://dealer.jaguar.com.cn/?field_address_administrative_area=&field_address_locality=&province="+proNum+"&city="+cityNum;
					Document dealerDoc = Jsoup.connect(getDealer).get();
					//获取所有经销商
					Element dealerDiv = dealerDoc.getElementsByClass("view-content").get(0);
					Elements dealers = dealerDiv.getElementsByClass("views-row");
					for(int n=0;n<dealers.size();n++) {
						String name = dealers.get(n).getElementsByClass("views-field-title").text();
						String type = name.substring(name.indexOf("(")+1, name.indexOf(")"));
						String address = dealers.get(n).getElementsByClass("views-field-field-address").get(0).getElementsByClass("field-content").text();
						String sellTell = dealers.get(n).getElementsByClass("views-field-field-dian-hua").get(0).getElementsByClass("field-content").text();
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("捷豹");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("捷豹汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(type);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						list.add(agency);
					}
				}
			}
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常",e);
		}
		return list;
	}*/
	public static void main(String[] args) {
		JieBaoQiCheCrawler crawler = new JieBaoQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
