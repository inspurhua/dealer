package crawler.baowo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class BaoWoQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			//获取省份id和name
			String getPro = "http://www.borgward.com.cn/dispose/communal/getDealerProvinceList";
			String proText = HttpConnectionGet.getJson(getPro);
			JSONObject proObject = JSON.parseObject(proText);
			JSONArray provinces = proObject.getJSONArray("data");
			for(int p=0;p<provinces.size();p++) {
				JSONObject pro = provinces.getJSONObject(p);
				String proNum = pro.getString("code");
				String proName = pro.getString("provinceName");
				String proId = pro.getString("provinceId");
				
				
			/*}
			
			
			Document proDoc = Jsoup.connect(getPro).get();
			Element proSelect = proDoc.getElementById("J-province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for (int i = 0; i < proOptions.size(); i++) {
				String proNum = proOptions.get(i).attr("value");
				String proName = proOptions.get(i).text();*/
				//获取城市的id和name
				String getCity = "http://www.borgward.com.cn/dispose/communal/getDealerCityList?provinceId="+proId;
				String cityText = HttpConnectionGet.getJson(getCity);
				JSONObject cityObject = JSON.parseObject(cityText);
				JSONArray cities = cityObject.getJSONArray("data");
				for(int z=0;z<cities.size();z++) {
					JSONObject city = cities.getJSONObject(z);
					String cityNum = city.getString("code");
					String cityName = city.getString("cityName");
				

					String getDealer = "http://www.borgward.com.cn/dispose/web/dealer/getDealer?1509509150000&province="+proNum+"&city="+cityNum;
					
					String dealerText = HttpConnectionGet.getJson(getDealer);
					//System.out.println(dealerText);
					JSONObject dealerObject = JSON.parseObject(dealerText);
					JSONArray dealers = dealerObject.getJSONArray("data");
					for (int j = 0; j < dealers.size(); j++) {
						String name = dealers.getJSONObject(j).getString("dealerName");
						String address = dealers.getJSONObject(j).getString("address");
						String sellTell = dealers.getJSONObject(j).getString("hotline");
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("宝沃");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("宝沃汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType("4S");
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);

					}
					
					
				}
				
				
				
			}

		} catch (Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}

		return list;
	}

	public static void main(String[] args) {
		BaoWoQiCheCrawler crawler = new BaoWoQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		System.out.println("请查看数据库");
	}

}
