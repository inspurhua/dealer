package crawler.dongfeng;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionPost;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class DongFengChengYongCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		
		try {
			String path = "http://www.dfpv.com.cn/dealers/ajax.html";
			//post请求参数拼接
			String proParam = "action=province";
			//发送post请求,并获得所有省份json
			String proJson = HttpConnectionPost.getJson(path, proParam);
			//System.out.println(proJson);
			//解析省份json,获得每个省的编号和省名
			JSONArray proArray = JSON.parseArray(proJson);
			for(int i=0;i<proArray.size();i++) {
				String proNum = proArray.getJSONObject(i).getString("v_pid");
				
				//System.out.println(proNum+":"+proName);

				//用省编号拼接post请求,以获得省中的市.
				String cityParam = "pid="+proNum+"&action=city";
				//获得市的json
				String cityJson = HttpConnectionPost.getJson(path, cityParam);
				//System.out.println(cityJson);
				JSONArray cityArray = JSON.parseArray(cityJson);
				for(int j=0;j<cityArray.size();j++) {
					String cityNum = cityArray.getJSONObject(j).getString("v_pid");
					
					//System.out.println(cityNum+":"+cityName);
					
					
					String dealerUrl = "http://www.dfpv.com.cn/dealers/index/"+cityNum+".html";
					Document dealerPage = Jsoup.connect(dealerUrl).get();
					Elements result = dealerPage.getElementsByClass("dealers_column");
					Elements lis = result.get(0).getElementsByTag("li");
					for(int k=0;k<lis.size();k++) {
						String proName = proArray.getJSONObject(i).getString("v_name");
						String cityName = cityArray.getJSONObject(j).getString("v_name");
						String name = lis.get(k).getElementsByTag("h3").text();
						String sellTell = lis.get(k).getElementsByTag("p").get(0).text().split("：")[1];
						String serviceTell = lis.get(k).getElementsByTag("p").get(1).text().split("：")[1];
						String address = lis.get(k).getElementsByTag("p").get(2).text().split("：")[1];
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("东风");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("东风乘用车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(serviceTell);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
					//System.out.println(dealerPage);
				}
			}
		} catch(Exception e) {
			
		}
		
		
		
		
		
		return list;
		
	}
	public static void main(String[] args) {
		DongFengChengYongCheCrawler crawler = new DongFengChengYongCheCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
		
	}
}
