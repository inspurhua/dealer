package crawler.falali;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class FaLaLiQiChe {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.geocms.it/Server/servlet/S3JXServletCall?parameters=method_name%3DGetObject%26callback%3Dscube.geocms.GeoResponse.execute%26id%3D2%26idLayer%3DD%26query%3D%255BFilterDealer%255D%253D%255BY%255D%26licenza%3Dgeo-ferrarispa%26progetto%3DFerrari-Locator%26lang%3DCHI&encoding=UTF-8";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		int start = dealerStr.indexOf("\"");
		int end = dealerStr.lastIndexOf("}");
		dealerStr = dealerStr.substring(start, end+2);
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		try {
//			String jsonStr = (String)engine.eval("JSON.stringify(JSON.parse("+dealerStr+"));");
			String jsonStr = (String)engine.eval(dealerStr);
			JSONArray dealers = JSON.parseObject(jsonStr).getJSONArray("L").getJSONObject(0).getJSONArray("O");
			for (int i=0;i<dealers.size();i++) {
				JSONObject dealer = dealers.getJSONObject(i);
				JSONObject dealerInfo = dealer.getJSONObject("U");
				String nation = dealerInfo.getString("Nation");
				if (!"CN".equals(nation)) { // 如果不是中国经销商则跳过
					continue;
				}
				String address = dealerInfo.getString("Address");
				String name = dealerInfo.getString("Name");
				String proName = dealerInfo.getString("ProvinceStateExt");
				if(address == null) {
					continue;
				}
				String[] lngAndLat = Location.getLocation(address);
				String lng = lngAndLat[0];
				String lat = lngAndLat[1];
				String cityName = ProCity.getData(lngAndLat)[1];
				
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				

				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("法拉利");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("法拉利汽车");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(null);

				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				//System.out.println(agency);
				list.add(agency);
				
				
			}
			
			
			
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		return list;
	}
	public static void main(String[] args) {
		FaLaLiQiChe crawler = new FaLaLiQiChe();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}
}
