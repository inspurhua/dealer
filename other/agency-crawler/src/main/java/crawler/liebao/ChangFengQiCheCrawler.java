package crawler.liebao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class ChangFengQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try { 
			//获取省份的id和name
			String getPro = "http://www.leopaard.com/index.php?m=content&c=index&a=lists&catid=8";
			Document proDoc = Jsoup.connect(getPro).get();
			Element proSelect = proDoc.getElementById("province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for(int i=1;i<proOptions.size();i++) {
				String proName = proOptions.get(i).text();
				String proNum = proOptions.get(i).attr("value");
				//获取城市的id和name
				String getCity = "https://www.leopaard.com/index.php?m=consultation&c=ajax&a=chooseAjax&province_id="+proNum;
				String cityStr = getJson(getCity);
				JSONArray citys = JSON.parseObject(cityStr).getJSONArray("data");
				for (int j=0;j<citys.size();j++) {
					JSONObject city = citys.getJSONObject(j);
					String cityNum = city.getString("id");
					String cityName = city.getString("name");
					String getDealer = "https://www.leopaard.com/index.php?m=consultation&c=ajax&a=chooseDealerAjax&city_id="+cityNum;
					String dealersStr = getJson(getDealer);
					JSONArray dealers = JSON.parseObject(dealersStr).getJSONArray("data");
					for (int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String name = dealer.getString("name");
						String address = dealer.getString("address");
						String sellTell = dealer.getString("tel");
						String lat = dealer.getString("latitude");
						String lng = dealer.getString("longtitude");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("猎豹");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("长丰汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
				}
			}
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		return list;
	}
	private String getJson(String path) {
		try {
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			//conn.setRequestProperty("Host", "www.leopaard.com");
			//conn.setRequestProperty("Referer", "http://www.leopaard.com/index.php?m=content&c=index&a=lists&catid=8");
			//conn.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
			//conn.setRequestProperty("Connection", "keep-alive");
			//conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36");
			conn.setRequestProperty("", "");
			InputStream in = conn.getInputStream();
			if ("gzip".equals(conn.getContentEncoding())){
				in = new GZIPInputStream(in);
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			StringBuffer total = new StringBuffer(); 
			String line = null;
			while((line = br.readLine())!=null) {
				total.append(line);
			}
			return total.toString();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		return null;
	}
	public static void main(String[] args) {
		ChangFengQiCheCrawler crawler = new ChangFengQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
