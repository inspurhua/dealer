package crawler.zhongxing;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.zql.entity.AgencyEntity;

import util.HttpConnectionPost;
import util.Location;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class ZhongXingQiChe {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.zxauto.com.cn/service/salenet.asp";
		try {
			Document doc = Jsoup.connect(getPro).get();
			Elements provinces = doc.getElementById("provinceid").getElementsByTag("option");
			for (int i = 1; i < provinces.size(); i++) {
				String proName = provinces.get(i).text();
				String proNum = provinces.get(i).attr("value");
				String getDealer = "http://www.zxauto.com.cn/service/salenet.asp?act=search";
				Document dealerDoc = Jsoup.connect(getDealer).data("title", "").data("provinceid", proNum).post();
				Elements tables = dealerDoc.getElementsByClass("searchTable");
				for (Element table : tables) {
					Elements trs = table.getElementsByTag("tr");
					String name = trs.get(0).getElementsByTag("td").get(1).text();
					String address = trs.get(1).getElementsByTag("td").get(1).text();
					String sellTell = trs.get(3).getElementsByTag("td").get(1).text();

					String[] lngAndLat = Location.getLocation(address);
					String lng = lngAndLat[0];
					String lat = lngAndLat[1];
					String cityName = ProCity.getData(lat, lng)[1];

					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);

					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("中兴");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("中兴汽车");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					//System.out.println(agency);
					list.add(agency);

				}

			}

		} catch (IOException e) {

			e.printStackTrace();
		}

		return list;
	}
	public static void main(String[] args) {
		ZhongXingQiChe crawler = new ZhongXingQiChe();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
