package crawler.DS;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class ChangAnBiaoZhiXueTieLongCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getData = "http://www.ds.com.cn/web/cn/api/dealers?lang=cn&type=3";
		String dataText = HttpConnectionGet.getJson(getData);
		JSONObject dataJSON = JSON.parseObject(dataText);
		
		//将JSONObject转为map,遍历省的id和data
		Map<String, Object> areaMap = dataJSON.getJSONObject("arealist");
		//将城市id和name存入一个map
		Map<String, String> cityMap = new HashMap<String, String>();
		//将城市id和省name存入一个map
		Map<String, String> cityProMap = new HashMap<String, String>();
		//遍历values,并转成JSONObject
		Collection<Object> proDatas = areaMap.values();
		for(Object proData : proDatas) {
			JSONObject proObject = null;
			if(proData instanceof JSONObject) {
				proObject= (JSONObject)proData;
			}
			//获取省name
			String proName = proObject.getString("AreaName");
			//将城市JSONobject转成cityDataMap
			Map<String, Object> cityDataMap = proObject.getJSONObject("city");
			Set<Entry<String, Object>> setCity = cityDataMap.entrySet();
			//遍历cityDataMap,获取城市id,城市name
			for(Entry<String, Object> entryCity : setCity) {
				String cityNum = entryCity.getKey();
				String cityName = null;
				if(entryCity.getValue() instanceof JSONObject) {
					JSONObject cityObject = (JSONObject)entryCity.getValue();
					cityName = cityObject.getString("AreaName");
				}
				cityMap.put(cityNum, cityName);
				cityProMap.put(cityNum, proName);
			}
		}
		//获取所有经销商
		JSONArray dealerArray = dataJSON.getJSONArray("dealers");
		for(int i=0;i<dealerArray.size();i++) {
			JSONObject dealer = dealerArray.getJSONObject(i);
			String name = dealer.getString("fullname");
			String address = dealer.getString("address");
			String sellTell = dealer.getString("selltel");
			String serviceTell = dealer.getString("servicetel");
			String cityNum = dealer.getString("city");
			String lng = dealer.getString("lon");
			String lat = dealer.getString("lat");
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			String cityName = cityMap.get(cityNum);
			String proName = cityProMap.get(cityNum);
			int state = 1; 
			if(cityName==null && proName==null) {
				state = 2;
			}
			
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("DS");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("长安标致雪铁龙");

			agency.setnState(state);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);
		}
		return list;
	}
	public static void main(String[] args) {
		ChangAnBiaoZhiXueTieLongCrawler crawler = new ChangAnBiaoZhiXueTieLongCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
