package crawler.hongqi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;
//需要修改cookie和X-CSRF-TOKEN
public class YiQiHongQi {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		List<AgencyEntity> listJXS = getData(1);
		List<AgencyEntity> listFWS = getData(2);
		list.addAll(listJXS);
		list.addAll(listFWS);

		return list;
	}

	List<AgencyEntity> getData(int classId) {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.faw-hongqi.com.cn/Common/Data/mapData";
		String param1 = "ajax=1&class_id="+classId+"&city_name=济南市&pcount=1&kw=";
						 //ajax=1&class_id=1&city_name=&kw=&pcount=1
		String proStr = getJson(getPro, param1);
		//System.out.println(proStr);
		JSONArray provinces = JSON.parseObject(proStr).getJSONArray("provinces");
		for (int i = 0; i < provinces.size(); i++) {
			JSONObject pro = provinces.getJSONObject(i);
			String proNum = pro.getString("province_id");
			String proName = pro.getString("pub_name");

			String getCity = "http://www.faw-hongqi.com.cn/Common/Data/mapCity";
			String param2 = "ajax=1&class_id=" + classId + "&province_id=" + proNum;
			String cityStr = getJson(getCity, param2);
			JSONArray citys = JSON.parseArray(cityStr);
			for (int j = 0; j < citys.size(); j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityNum = city.getString("city_id");
				String cityName = city.getString("pub_name");

				String getDealer = "http://www.faw-hongqi.com.cn/Common/Data/mapDealer";
				String param3 = "ajax=1&class_id=1&city_id=" + cityNum;
				String dealerStr = getJson(getDealer, param3);
				JSONArray dealers = JSON.parseArray(dealerStr);
				for (int k = 0; k < dealers.size(); k++) {
					JSONObject dealer = dealers.getJSONObject(k);
					String address = dealer.getString("address");
					String lat = dealer.getString("lat");
					String lng = dealer.getString("lng");
					String name = dealer.getString("title");
					String sellTell = "";
					String serviceTell = "";
					String type = "";
					if (classId == 1) {
						type = "经销商";
						sellTell = dealer.getString("tel");

					} else {
						type = "服务商";
						serviceTell = dealer.getString("tel");
					}

					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);

					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("红旗");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("一汽红旗");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(serviceTell);
					agency.setsDealerName(name);
					agency.setsDealerType(type);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);

				}

			}

		}
		return list;

	}
	String getJson(String path, String param) {
		PrintWriter pw = null;
		BufferedReader br = null;
		try {
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setConnectTimeout(1000*60);
			conn.setReadTimeout(1000*10);
			conn.setRequestProperty("Accept","*/*");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36");
			conn.setRequestProperty("Referer", "http://www.faw-hongqi.com.cn/server/wd/1");
			conn.setRequestProperty("Origin", "http://www.faw-hongqi.com.cn");
			conn.setRequestProperty("Host", "www.faw-hongqi.com.cn");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			conn.setRequestProperty("Connection", "keep-alive");
			conn.setRequestProperty("Content-Length", "68");
			conn.setRequestProperty("Cookie", "HQ_dmap_user=eyJpdiI6ImRZQVFYVng4Q3MxSkpSV1c3UnJVVGc9PSIsInZhbHVlIjoiVmpSSUFZUnlNRmFHVk9rWTU0djFPcVRnUHhSdFZHMVlEazg3QU56VGlLMUZKcmtYRmwrdFB5WlwvNGNadWw2NjRBQVRaNlRUQzBVcGZIc21rODRMSzdnPT0iLCJtYWMiOiIxZTliYzg4ZmRiNWMyY2Y2N2FjNTNhMmM1MDFlZjU0NDAwYTk5ZGRlNWU5YzcyMDIzMzRlZjIxN2E1Nzk4N2Y1In0%3D; Hm_lvt_a242b494418e8f3239f89e5f9d9a6242=1550552806,1551426096; XSRF-TOKEN=eyJpdiI6IkY4Z1lvY2U0VW5KQldTWWM3ZE9KbVE9PSIsInZhbHVlIjoiQ2lWY1BMZFZuY0tUaDZTbXhpSFVJS1VOT2JBVFhtMjNCSFZwZnBiQVdrMGdBcUJxOHJQS0ZOM01wYWc2Unp0QkxjaEZVdDBBVTVXNXRmNEhSVWNCQ2c9PSIsIm1hYyI6IjkxMTM1MTE2N2UwMTE4ZDdiODU2Mjk0YWVlOGE0OGQwNjQ2YTI1ZDFmYTk5ZGU1Nzk2ZjE4YTgwMTYwOTQxMmQifQ%3D%3D; laravel_session=eyJpdiI6IkxHZ0JIandBaSthU2t5bkpFRE0yaWc9PSIsInZhbHVlIjoiU2pIWkd5TFwvUTdPQ0VldUE1U01oRlozcHJ4RmdJY1dCTUl4TFVDR2pHNCtjT3ozR24yMTV4UGo5aHExaTJxUjcxbWN4TnYzTDBzVEV3VE1PdWdvb0hBPT0iLCJtYWMiOiI5YjQ2YjcyNmRmMTQ2NGFjNjY4MzZhNzcxNzYwYWMyYjEzYjE0YTFkNTdkNjRjMTJhNGUzOThhOTQ4N2Y0Y2RmIn0%3D; HQ_dmap_source=eyJpdiI6Ik1ESmltYjhUK1ptR01tM0VwUHhiTEE9PSIsInZhbHVlIjoiOHZIZEF5N1VCcU1na0tDVjF6aTJ0ZVZLNDJ1eW5YN0FsclcreGp5SnA1QT0iLCJtYWMiOiI1ZmQ5ZDRkMGVjODMyYzkyOTFjMzI1NTkzMmYyZDlmMWNhODIwNjQ0ODg3NDczZTMzMGEzYjQzZDYzYTU5ZjgyIn0%3D; Hm_lpvt_a242b494418e8f3239f89e5f9d9a6242=1551426132");
			conn.setRequestProperty("X-CSRF-TOKEN", "m5p1WMFvtnA9ecPi0WE5QollLgeCqptchNDCNumv");
			conn.setRequestProperty("X-Forwarded-For", "151.201.16.113");
			conn.setRequestMethod("POST");
			
			pw = new PrintWriter(conn.getOutputStream());
			pw.print(param);
			pw.flush();
			
			
			
			String type = conn.getContentEncoding();
			//System.out.println("==11=="+type);
			if ("gzip".equals(type)) {
				GZIPInputStream gzip = new GZIPInputStream(conn.getInputStream());
				
				br = new BufferedReader(new InputStreamReader(gzip));
			} else {
				br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			}
			
			String line = null;
			StringBuilder total = new StringBuilder();
			while ((line=br.readLine()) != null) {
				total.append(line);
			}
			
			return total.toString();
			
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			
		}
		 
		return null;
	}
	
	public static void main(String[] args) {
		YiQiHongQi crawler = new YiQiHongQi();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
