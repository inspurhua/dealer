package crawler.jiliqiche;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年10月11日 下午2:51:00 
* 类说明 
*/
public class JiLiQiChePageprocessor implements PageProcessor {
	private static List<String> address = new ArrayList<>();
	private static List<String> cityName = new ArrayList<>();
	//private static List<String> serviceTell = new ArrayList<>();
	private static List<String> name = new ArrayList<>();
	private static List<String> dnull = new ArrayList<>();
	private static List<String> proName = new ArrayList<>();
	private static List<String> sellTell = new ArrayList<>();
	private static List<String> lng = new ArrayList<>();
	private static List<String> lat = new ArrayList<>();
	private static String purl = "http://www.geely.com/api/geely/official/get/getprovincelist";
	private static String curl = "http://www.geely.com/api/geely/official/get/getcitylist\\?provinceid=[0-9]{2,3}";
	//private static String durl = "http://www.geely.com/api/geely/official/get/GetDealer\\?seriesCode=&province=[0-9]{2,3}&city=[0-9]{6}&keyword=";
	private static List<String> pname = new ArrayList<>();
	private static List<String> pid = new ArrayList<>();
	private static List<String> cname = new ArrayList<>();
	private static List<String> cid = new ArrayList<>();
	@Override
	public void process(Page page) {
		if(page.getUrl().regex(purl).match()) {
		pname = page.getJson().jsonPath("$[*].regionName").all();
		//System.out.println(pname);
		pid = page.getJson().jsonPath("$[*].regionId").all();
		//System.out.println(pid);
		List<String> pidurl = new ArrayList<>();
		for(int i=0;i<pid.size();i++) {
			pidurl.add("http://www.geely.com/api/geely/official/get/getcitylist?provinceid="+pid.get(i));
		}
		page.addTargetRequests(pidurl);
		}else if(page.getUrl().regex(curl).match()) {
		List<String> cname0 = page.getJson().jsonPath("$[*].regionName").all();
		List<String> cid0 = page.getJson().jsonPath("$[*].regionId").all();
		//System.out.println(cname.size()+"=="+cname);
		//System.out.println(cid.size()+"=="+cid);
		cname.addAll(cname0);
		cid.addAll(cid0);
		String cpid = page.getUrl().regex("[0-9]{2,3}").get();
		List<String> cidurl = new ArrayList<>();
		for(int i=0;i<cid0.size();i++) {
			cidurl.add("http://www.geely.com/api/geely/official/get/GetDealer?seriesCode=&province="+cpid+"&city="+cid0.get(i)+"&keyword=");
		}
		page.addTargetRequests(cidurl);
		}else  {
		List<String> name00 = page.getJson().jsonPath("$[*].dealerName").all();
		name.addAll(name00);
		List<String> add00 = page.getJson().jsonPath("$[*].address").all();
		address.addAll(add00);
		List<String> sell00 = page.getJson().jsonPath("$[*].bizPhone").all();
		sellTell.addAll(sell00);
		List<String> coordinates = page.getJson().jsonPath("$[*].coordinates").all();
		String dpid = page.getUrl().regex("province=[0-9]{2,3}").regex("[0-9]{2,3}").get();
		String dcid = page.getUrl().regex("city=[0-9]{6}").regex("[0-9]{6}").get();
		String p = "";
		String c = "";
		for(int i=0;i<pid.size();i++) {
			if(pid.get(i).equals(dpid)) {
				p = pname.get(i);
			}else {}
		}
		for(int i=0;i<cid.size();i++) {
			if(cid.get(i).equals(dcid)) {
				c = cname.get(i);
			}else {}
		}
		for(int i=0;i<name.size();i++) {
			dnull.add("");
			proName.add(p);
			cityName.add(c);
		}
		for(int i=0;i<coordinates.size();i++) {
			if(coordinates.get(i)!=null&&coordinates.get(i).contains(",")) {
				String[] jwd = coordinates.get(i).split(",");
					lng.add(jwd[0]);
					lat.add(jwd[1]);
			}else if(coordinates.get(i)!=null&&coordinates.get(i).contains("，")) {
				String[] jwd = coordinates.get(i).split("，");
					lng.add(jwd[0]);
					lat.add(jwd[1]);
			}else{
				lng.add("");
				lat.add("");
			}
			
		}
		}

	}
	private Site site=Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.geely.com")
			.addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.geely.com/zh-CN/carbuysupport/dealerqueryamap")
			.addHeader("X-Requested-With", "XMLHttpRequest")
			.addHeader("Connection", "keep-alive")			
			//.setCharset("GBK")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(3)
			;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new JiLiQiChePageprocessor())
	.addUrl("http://www.geely.com/api/geely/official/get/getprovincelist")
	.run();
/*	Spider.create(new JiLiQiChePageprocessor())
	.addUrl("http://www.geely.com/api/geely/official/get/getcitylist?provinceid=150")
	.run();*/
	/*Spider.create(new JiLiQiChePageprocessor())
	.addUrl("http://www.geely.com/api/geely/official/get/GetDealer?seriesCode=&province=20&city=100006&keyword=")
	.run();*/
	AgencyEntity2 agency = new AgencyEntity2();
	//agency.setdCloseDate(null);
	//agency.setdOpeningDate(null);
	agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
	//agency.setnBrandID(-1);
	agency.setsBrand("吉利");
	//agency.setnDealerIDWeb(-1);
	agency.setnManufacturerID(-1);
	agency.setsManufacturer("吉利汽车");
	//agency.setnState(1);
	agency.setsAddress(address);
	agency.setsCity(cityName);
	agency.setsCustomerServiceCall(dnull);
	agency.setsDealerName(name);
	agency.setsDealerType(dnull);
	agency.setsProvince(proName);
	agency.setsSaleCall(sellTell);
	agency.setsLongitude(lng);
	agency.setsLatitude(lat);
	//agency.setsControllingShareholder(null);
	//agency.setsOtherShareholders(null);
	new AgencyDao().add(agency);
}
}
 