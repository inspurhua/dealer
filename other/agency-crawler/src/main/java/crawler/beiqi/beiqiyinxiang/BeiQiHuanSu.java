package crawler.beiqi.beiqiyinxiang;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class BeiQiHuanSu {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//获取城市和省
		try {
			//String getPro = "http://hdh5.pinzhidb.com/uload.html";
			String getPro = "http://www.baic-hs.com/jxs/selectProvince.html";
			String proStr = HttpConnectionGet.getJson(getPro);
			String[] provinces = proStr.split("\\$");
			for(String proName: provinces) {
				if(proName.trim().isEmpty()) {
					continue;
				}
				proName = proName.split("=")[0];
				String getCity = "http://hdh5.pinzhidb.com/jxs.html?sf="+proName;
				String cityStr = HttpConnectionGet.getJson(getCity);
				String[] cities = cityStr.split("\\$");
				for (String cityName : cities) {
					if(cityName.trim().isEmpty()) {
						continue;
					}
					cityName = cityName.split("=")[0];
					String getDealer = "http://hdh5.pinzhidb.com/sjx.html?sf="+cityName+"&lb=经销商";
					String dealerStr = HttpConnectionGet.getJson(getDealer);
					String[] dealers = dealerStr.split("\\$");
					for(String dealer: dealers) {
						if(dealer.trim().isEmpty()) {
							continue;
						}
						String[] data = dealer.split("\\|");
						String name = data[0];
						String address = data[1];
						String sellTell = data[2];
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("北汽幻速");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("北汽银翔");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
				}
			}
			
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		
		return list;
	}
	public static void main(String[] args) {
		BeiQiHuanSu crawler = new BeiQiHuanSu();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
/*		System.out.println("抓取完毕,正在存库");

		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");*/
	}

}
