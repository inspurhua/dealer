package crawler.beiqi.beiqiyinxiang;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyEntity;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.utils.HttpConstant;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年12月4日 下午12:15:12 
* 类说明 
*/
public class BeiQiHuanSuPageProcessor implements PageProcessor {
	private static Site site;
	private static List<String> pro;
	private static List<String> city0;
	private static List<String> stuats0;
	private static List<String> type0;
	private static List<AgencyEntity> list = new ArrayList<AgencyEntity>();
	@Override
	public void process(Page page) {
		//System.out.println(page.getHtml().get());
		if(page.getUrl().regex("selectProvince").match()) {
			pro = page.getJson().jsonPath("$[*].shengshi_name").all();
			//System.out.println(pro);
		}else 
			if(page.getUrl().regex("selectCity").match()){
				city0 = page.getJson().jsonPath("$[*].chengshi_name").all();
				//System.out.println(city0);
				stuats0 = page.getJson().jsonPath("$[*].stuats").all();
				//System.out.println(stuats0);
				type0 = page.getJson().jsonPath("$[*].type").all();
				//System.out.println(type0);
		}else 
			if(page.getUrl().regex("selectjxs").match()){
				//System.out.println(page.getHtml().get());
		List<String> name = page.getJson().jsonPath("$[*].dlrname").all();
		//System.out.println(name);
		List<String> address = page.getJson().jsonPath("$[*].addr").all();
		//System.out.println(address);
		List<String> proName = page.getJson().jsonPath("$[*].shengshi_name").all();
		//System.out.println(proName);
		List<String> sellTell = page.getJson().jsonPath("$[*].tell").all();
		//System.out.println(sellTell);
		List<String> cityName = page.getJson().jsonPath("$[*].chengshi_name").all();
		//System.out.println(cityName);
		List<String> type20 = page.getJson().jsonPath("$[*].type").all();
		//System.out.println(type20);
		for(int i=0;i<address.size();i++) {
			String[] lngAndLat = Location.getLocation(address.get(i));
			String lng = lngAndLat[0];
			String lat = lngAndLat[1];
			//System.out.println(lng);
			//System.out.println(lat);
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("北汽幻速");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("北汽银翔");

			agency.setnState(1);
			agency.setsAddress(address.get(i));
			agency.setsCity(cityName.get(i));
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name.get(i));
			agency.setsDealerType(type20.get(i));
			agency.setsProvince(proName.get(i));
			agency.setsSaleCall(sellTell.get(i));
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(null);
			agency.setsOtherShareholders(null);
			list.add(agency);
		}
		}
	}
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.baic-hs.com")
			.addHeader("Accept", "*/*")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.baic-hs.com/distributor.html")
			.addHeader("X-Requested-With", "XMLHttpRequest")
			.addHeader("Connection", "keep-alive")
			.addHeader("Cache-Control", "max-age=0")
			//.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
			//.addHeader("Content-Length", "0")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(5)
			;
	Spider.create(new BeiQiHuanSuPageProcessor())
	.addUrl("http://www.baic-hs.com/jxs/selectProvince.html")
	.run();
	for(String pro0 : pro) {
		if(!pro0.equals("null")) {
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.baic-hs.com")
			.addHeader("Accept", "*/*")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.baic-hs.com/distributor.html")
			.addHeader("X-Requested-With", "XMLHttpRequest")
			.addHeader("Connection", "keep-alive")
			.addHeader("Cache-Control", "max-age=0")
			.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
			//.addHeader("Content-Length", "0")
			.setSleepTime(0)
			.setCycleRetryTimes(5)
			;
	Request request = new Request("http://www.baic-hs.com/jxs/selectCity.html");
	request.setMethod(HttpConstant.Method.POST);
	request.setRequestBody(HttpRequestBody.json("shengshi_name="+pro0, "utf-8"));
	Spider.create(new BeiQiHuanSuPageProcessor())
	.addRequest(request)
	.run();
	for(int i=0;i<city0.size();i++) {
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.baic-hs.com")
			.addHeader("Accept", "*/*")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.baic-hs.com/distributor.html")
			.addHeader("X-Requested-With", "XMLHttpRequest")
			.addHeader("Connection", "keep-alive")
			.addHeader("Cache-Control", "max-age=0")
			.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
			.setSleepTime(0)
			.setCycleRetryTimes(5)
			;
	Request request2 = new Request("http://www.baic-hs.com/jxs/selectjxs.html");
	request2.setMethod(HttpConstant.Method.POST);
	request2.setRequestBody(HttpRequestBody.json("shengshi_name="+pro0+"&chengshi_name="+city0.get(i)+"&stuats="+stuats0.get(i)+"&type="+type0.get(i), "utf-8"));
	Spider.create(new BeiQiHuanSuPageProcessor())
	.addRequest(request2)
	.run();
		}
	//System.out.println("集合====="+list);
	}else {}
	}

	System.out.println("抓取完毕,正在存库");
	for(AgencyEntity agency : list) {
		MybatisTool.save(agency);
	}
	MybatisTool.close();
	System.out.println("请查看数据库");
}
}
 