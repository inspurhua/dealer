package crawler.leinuo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月8日 下午3:43:28 
* 类说明 
*/
public class DongFengLeiNuoPageProcessor implements PageProcessor {
	@Override
	public void process(Page page) {
		if(page.getUrl().regex("GetProvinces").match()) {
			List<String> pid = page.getJson().jsonPath("$[*].Code").all();
			List<String> curl = new ArrayList<>();
			for(int i=0;i<pid.size();i++) {
				curl.add("https://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetCitys&province="+pid.get(i));
			}
			page.addTargetRequests(curl);
		}else if(page.getUrl().regex("GetCitys").match()){
			List<String> cid = page.getJson().jsonPath("$[*].Code").all();
			List<String> durl = new ArrayList<>();
			for(int i=0;i<cid.size();i++) {
				durl.add("https://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetDealers&city="+cid.get(i));
			}
			page.addTargetRequests(durl);
		}else { 
			List<String> name = page.getJson().jsonPath("$[*].FullName").all();
			//System.out.println(name);
			List<String> address = page.getJson().jsonPath("$[*].ADDRESS").all();
			//System.out.println(address);
			List<String> proName = page.getJson().jsonPath("$[*].PROVINCE_NAME").all();
			//System.out.println(proName);
			List<String> cityName = page.getJson().jsonPath("$[*].CITY_NAME").all();
			//System.out.println(cityName);
			List<String> sellTell = page.getJson().jsonPath("$[*].SALES_PHONE").all();
			//System.out.println(sellTell);
			List<String> serviceTell = page.getJson().jsonPath("$[*].AFTER_SALES_PHONE").all();
			//System.out.println(serviceTell);
			List<String> lat = page.getJson().jsonPath("$[*].LATITUDE").all();
			//System.out.println(lat);
			List<String> lng = page.getJson().jsonPath("$[*].LONGITUDE").all();
			//System.out.println(lng);
			List<String> dnull = new ArrayList<>();
			for(int i=0;i<name.size();i++) {
				dnull.add("");
			}
			AgencyEntity2 agency = new AgencyEntity2();
			//agency.setdCloseDate(null);
			//agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			//agency.setnBrandID(-1);
			agency.setsBrand("雷诺");

			//agency.setnDealerIDWeb(-1);
			//agency.setnManufacturerID(-1);
			agency.setsManufacturer("东风雷诺");

			//agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsDealerName(name);
			agency.setsDealerType(dnull);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			new AgencyDao().add(agency);
			//agency.setsControllingShareholder(sControllingShareholder);
			//agency.setsOtherShareholders(sOtherShareholders);
		}
	}
private Site site = Site.me()
						.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
						.addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
						.addHeader("Accept-Encoding", "gzip, deflate, br")
						.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
						.addHeader("Host", "www.dongfeng-renault.com.cn")
						.addHeader("Referer", "https://www.dongfeng-renault.com.cn/zh-CN/Buying/Tools/FindDealer.aspx")
						.addHeader("X-Requested-With", "XMLHttpRequest")
						.setSleepTime(0)
						.setTimeOut(60000)
						.setCycleRetryTimes(3)
						;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new DongFengLeiNuoPageProcessor())
	.addUrl("https://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetProvinces")
	//.addUrl("https://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetCitys&province=02030023")
	//.addUrl("https://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetDealers&city=020300230229")
	.run();
}
}
 