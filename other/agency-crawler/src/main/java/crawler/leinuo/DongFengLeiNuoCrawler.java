package crawler.leinuo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class DongFengLeiNuoCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			String getProv = "http://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetProvinces";
			String proText = HttpConnectionGet.getJson(getProv);
			JSONArray provinces = JSON.parseArray(proText);
			for(int i=0;i<provinces.size();i++) {
				String proNum = provinces.getJSONObject(i).getString("Code");
				String proName = provinces.getJSONObject(i).getString("Name");
				
				//System.out.println(proNum+":"+proName);
				
				String getCity = "http://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetCitys&province="+proNum;
				String cityText = HttpConnectionGet.getJson(getCity);
				JSONArray cities = JSON.parseArray(cityText);
				for(int j=0;j<cities.size();j++) {
					String cityNum = cities.getJSONObject(j).getString("Code");
					String cityName = cities.getJSONObject(j).getString("Name");
					String getDealer = "http://www.dongfeng-renault.com.cn/Ajax/Common.ashx?method=GetDealers&city="+cityNum;
					String dealerText = HttpConnectionGet.getJson(getDealer);
					JSONArray dealers = JSON.parseArray(dealerText);
					for(int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String name = dealer.getString("FullName");
						String sellTell = dealer.getString("SALES_PHONE");
						String serviceTell = dealer.getString("AFTER_SALES_PHONE");
						String address = dealer.getString("ADDRESS");
						String lat = dealer.getString("LATITUDE");
						String lng = dealer.getString("LONGITUDE");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("雷诺");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("东风雷诺");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(serviceTell);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					
					}
				}
			}
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		return list;
	}
	public static void main(String[] args) {
		DongFengLeiNuoCrawler crawler = new DongFengLeiNuoCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
