package crawler.aodi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

@SuppressWarnings("unused")
public class YiQiDaZhongAoDiCrawler2 {
	// private static String all;
	public String getAll() {
		String getData = "https://contact.audi.cn/dictionary_js/map_dealer.js";
		BufferedReader br = null;
		try {
			URL url = new URL(getData);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Host", "contact.audi.cn");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");

			conn.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");

			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");

			conn.setRequestProperty("Referer", "https://contact.audi.cn/dealer_procity.html");

			conn.setRequestProperty("Cookie",
					"_ga=GA1.2.1656224903.1502704062; _sdsat_session_count=2; _sdsat_lt_pages_viewed=4; AMCV_097B467352782F130A490D45%40AdobeOrg=793872103%7CMCIDTS%7C17470%7CMCMID%7C65276108276176759354161138488048705843%7CMCAAMLH-1509959411%7C11%7CMCAAMB-1509959411%7CQmsI9f4tW0xYmrImGgwlXJEuHZFOkf--jG4MjHU3AKYXnCY%7CMCAID%7CNONE; _smt_uid=599171bf.1c3a3187; _sdsat_landing_page=https://contact.audi.cn/|1509354610994; _sdsat_pages_viewed=2; _sdsat_traffic_source=https://contact.audi.cn/; _sdsat_41 Timestamp Session Entry=2017-10-30T09:10:11.015Z; _yatma=1509354611; s_cc=true; _gid=GA1.2.491189140.1509354613; _gat=1; _sdsat_previous_page=local-production/dealer_procity.html");
			// conn.setRequestProperty("If-Modified-Since", "Mon, 16 Oct 2017 07:16:42
			// GMT");
			// conn.setRequestProperty("If-None-Match", "W/\"337566-1508138202000\"");
			// conn.setRequestProperty("Cache-Control", "max-age=1");
			int code = conn.getResponseCode();
			// System.out.println(code);
			if (200 != code) {
				return "";
			}
			// 建立输入流,读取返回的信息
			br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null; // 每行内容
			StringBuffer content = new StringBuffer();
			while ((line = br.readLine()) != null) {
				content.append(line);
			}
			// int start = content.indexOf("type[]");
			int end = content.indexOf("type[0]");
			// System.out.println(start+","+end);
			// 截取所有省份,城市,经销商信息
			String all = content.substring(0, end);
			// System.out.println(all);
			return all;

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}


	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String content = getAll();
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		try {
			String jsonStr = (String) engine.eval(content + "; JSON.stringify(dealers)");
			//System.out.println(jsonStr);
			JSONArray dealers = JSON.parseArray(jsonStr);
			for (int i = 0; i < dealers.size(); i++) {
				JSONArray city = dealers.getJSONArray(i);
				if (city != null && !city.isEmpty() && !"null".equals(city)) {
					for (int j=0;j<city.size();j++) {
						JSONArray dealer = city.getJSONArray(j);
						String name = dealer.getString(1);
						String lng = dealer.getString(2).split(",")[0];
						String lat = dealer.getString(2).split(",")[1];
						String sellTell = dealer.getString(9)+"-"+dealer.getString(3);
						String address = dealer.getString(4);
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						String[] proCity = ProCity.getData(lat, lng);
						String proName = proCity[0];
						String cityName = proCity[1];
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("奥迪");
						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("一汽大众奥迪");
						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						//System.out.println(agency);
						list.add(agency);
						
					}
				}

			}
		} catch (ScriptException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void main(String[] args) {
		YiQiDaZhongAoDiCrawler2 crawler = new YiQiDaZhongAoDiCrawler2();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		System.out.println("请查看数据库");

	}

}
