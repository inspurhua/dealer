package crawler.xuetielong;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class DongFengXueTieLong {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			Document doc = Jsoup.connect("http://www.dongfeng-citroen.com.cn/4s/index.php/dsearch").get();
			Element ul = doc.getElementsByClass("H_dealeritem").get(3);
			Elements ps = ul.getElementsByTag("p");
			for(int i=0;i<ps.size();i++) {
				String proName = ps.get(i).text();
				String proNum = ps.get(i).attr("data-id");
				
				String getCity = "http://www.dongfeng-citroen.com.cn/4s/index.php/api/getCities/"+proNum;
				String cityText = HttpConnectionGet.getJson(getCity);
				JSONArray cities = JSON.parseArray(cityText);
				for(int j=0;j<cities.size();j++) {
					String cityNum = cities.getJSONObject(j).getString("id");
					String cityName = cities.getJSONObject(j).getString("city");
					//System.out.println(cityNum+":"+cityName);
				
					String getDealer = "http://www.dongfeng-citroen.com.cn/4s/index.php/api/getDealersByLocation";
					String param = "city="+cityNum;
					String dealerText = HttpConnectionPost.getJson(getDealer, param);
					JSONObject msg = JSON.parseObject(dealerText);
					JSONArray dealers = msg.getJSONArray("list");
					for(int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String name = dealer.getString("dealer_name");
						String address = dealer.getString("address");
						String sellTell = dealer.getString("sale_tel");
						String serviceTell = dealer.getString("after_tel");
						String lng = dealer.getString("x");
						String lat = dealer.getString("y");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("雪铁龙");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("东风雪铁龙");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(serviceTell);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					}
				}
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		DongFengXueTieLong crawler = new DongFengXueTieLong();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
