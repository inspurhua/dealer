package crawler.jiangling;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class JiangLingQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://suv.jmc.com.cn/index.php/welcome/get_city";
		try {
			Document proDoc = Jsoup.connect(getPro).get();
			Elements proOptions = proDoc.getElementsByTag("option");
			for(int i=1;i<proOptions.size();i++) {
				String proName = proOptions.get(i).text();
				String proNum = proOptions.get(i).attr("value");
				
				String getCity = "http://suv.jmc.com.cn/index.php/welcome/get_city";
				Document cityDoc = Jsoup.connect(getCity)
						.data("type", "2")
						.data("flay", "1")
						.data("main_id", proNum)
						.post();
				Elements cityOptions = cityDoc.getElementsByTag("option");
				for(int j=1;j<cityOptions.size();j++) {
					String cityName = cityOptions.get(j).text();
					
					String getDealer = "http://suv.jmc.com.cn/index.php/welcome/get_dealer";
					String param = "brand_dealer=all&brand=all&type=li&city="+cityName;
					String dealerText = HttpConnectionPost.getJson(getDealer, param);
					JSONObject dealerJSON = JSON.parseObject(dealerText);
					JSONArray dealers = dealerJSON.getJSONArray("data");
					for(int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String name = dealer.getString("dealer_name");
						String sellTell = dealer.getString("dealer_fwtel");
						String address = dealer.getString("dealer_address");
						String lat = dealer.getString("dealer_lat");
						String lng = dealer.getString("dealer_lon");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("江铃");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("江铃汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					
					}
					
				
				}
				
				
			}
			
			
		} catch(Exception e) {
			throw new RuntimeException("爬虫失败", e);
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		JiangLingQiCheCrawler crawler = new JiangLingQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
	}

}
