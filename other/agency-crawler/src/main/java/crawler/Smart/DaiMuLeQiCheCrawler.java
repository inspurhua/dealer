package crawler.Smart;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.zql.entity.AgencyEntity;

import util.Location;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class DaiMuLeQiCheCrawler {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "https://china.smart.com/shoppingtools/dealer.html";
		Document doc = null;
		try {
			doc = Jsoup.connect(getDealer).get();
			Element div = doc.getElementsByClass("content_2 content").get(0);
			String dealerStr = div.html();
			Pattern p = Pattern.compile("<b>\\d+\\s*(.*?)</b>\\s*<p>.*?:(.*?)</p>\\s*<p>.*?:(.*?)</p>");
			Matcher m = p.matcher(dealerStr);
			while(m.find()) {
				String name = m.group(1);
				String address = m.group(2);
				String sellTell = m.group(3);
				String[] lngAndLat = Location.getLocation(address);
				String lng = lngAndLat[0];
				String lat = lngAndLat[1];
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				String[] proAndCity = ProCity.getData(lngAndLat);
				String proName = proAndCity[0];
				String cityName = proAndCity[1];
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("Smart");

				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("戴姆勒汽车");

				agency.setnState(-1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				list.add(agency);
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	public static void main(String[] args) {
		DaiMuLeQiCheCrawler crawler = new DaiMuLeQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

		// crawler.getAgency1();
	}

}
