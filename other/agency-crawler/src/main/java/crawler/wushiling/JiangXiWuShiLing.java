package crawler.wushiling;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class JiangXiWuShiLing {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.jiangxi-isuzu.cn/BrandSupport/dealer.shtml";
		try {
			Document doc = Jsoup.connect(getPro).get();
			Elements provinces = doc.getElementsByClass("cti-vselect").get(0).getElementsByTag("li");
			for (Element pro : provinces) {
				String proNum = pro.attr("data-value");
				String proName = pro.text();
				
	 			String getCity = "http://www.jiangxi-isuzu.cn/BrandSupport/ajax_city_bypid";
	 			String cityStr = HttpConnectionPost.getJson(getCity, "pid="+proNum);
				JSONArray citys = JSON.parseArray(cityStr);
				for (int i =0;i<citys.size();i++) {
					JSONObject city = citys.getJSONObject(i);
					String cityNum = city.getString("city_id");
					String cityName = city.getString("city_name");
					
					String getDealer = "http://www.jiangxi-isuzu.cn/BrandSupport/ajax_dealer_bycid";
					String dealerStr = HttpConnectionPost.getJson(getDealer, "cid="+cityNum);
					JSONArray dealers = JSON.parseArray(dealerStr);
					for(int j=0;j<dealers.size();j++) {
						JSONObject dealer = dealers.getJSONObject(j);
						String name = dealer.getString("dealer_name");
						String address = dealer.getString("dealer_address");
						String sellTell = dealer.getString("dealer_tel");
						String lng = dealer.getString("dealer_lon");
						String lat = dealer.getString("dealer_lat");
						
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);

						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("五十铃");
						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("江西五十铃");
						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);

						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);


						
					}
					
				}
	 			
	 			
				
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		return list;
	}
	public static void main(String[] args) {
		JiangXiWuShiLing crawler = new JiangXiWuShiLing();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
