package crawler.mazida;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class YiQiMaZiDaCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//String url = "http://www.faw-mazda.com/index.php/dealers/Dealer_query";
		String url = "http://cmsworks.faw-mazda.com/index.php/city/api_index";
		
			//获取省的名称和id
			String proText = HttpConnectionGet.getJson(url);
			JSONArray provinces = JSON.parseObject(proText).getJSONArray("content");
			for(int i=0;i<provinces.size();i++) {
				JSONObject pro = provinces.getJSONObject(i);
				String proName = pro.getString("city_name");
				String proNum = pro.getString("city_id");
				
				String getCity = "http://cmsworks.faw-mazda.com/index.php/city/api_index?main_id="+proNum;
				String cityText = HttpConnectionGet.getJson(getCity);
				JSONArray cities = JSON.parseObject(cityText).getJSONArray("content");
				for(int j=0;j<cities.size();j++) {
					JSONObject city = cities.getJSONObject(j);
					String cityName = city.getString("city_name");
					String cityNum = city.getString("city_id");
					
					String getDealer = "http://cmsworks.faw-mazda.com/index.php/sys_dealer/api_index?dealer_province_id="+proNum+"&dealer_city_id="+cityNum;
					String dealerText = HttpConnectionGet.getJson(getDealer);
					JSONArray dealers = JSON.parseObject(dealerText).getJSONArray("content");
					for(int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String name = dealer.getString("dealer_name");
						String address = dealer.getString("dealer_address");
						String sellTell = dealer.getString("dealer_tel");
						String serviceTell = dealer.getString("dealer_fwtel");
						String lng = dealer.getString("dealer_lon");
						String lat = dealer.getString("dealer_lat");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("马自达");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("一汽马自达");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(serviceTell);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
					
					
					
				}
				
				
			}
			
			
			/*try {
			Document doc = Jsoup.connect(url).get();
			Element proSelect = doc.getElementById("testdrv_province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for(int i=1;i<proOptions.size();i++) {
				String proNum = proOptions.get(i).attr("title");
				String proName = proOptions.get(i).text();
				
				String getCity = "http://www.faw-mazda.com/index.php/ajaxlist/Dealer_query/"+proNum+"/";
				Document cityDoc = Jsoup.connect(getCity).get();
				Elements cityOptions = cityDoc.getElementsByTag("option");
				for(int j=1;j<cityOptions.size();j++) {
					String cityNum = cityOptions.get(j).attr("value");
					String cityName = cityOptions.get(j).text();
					//System.out.println(cityNum+":"+cityName);
					
					String getDealer = "http://www.faw-mazda.com/index.php/dealers/Dealer_query/"+proNum+"/"+cityNum+"/";
					Document dealerDoc = Jsoup.connect(getDealer).get();
					Element ul = dealerDoc.getElementById("loadcontent");
					Elements lis = ul.getElementsByTag("li");
					for(int k=0;k<lis.size();k++) {
						Elements ps = lis.get(k).getElementsByTag("p");
						String name = ps.get(1).text().split("：")[1];
						String address = ps.get(2).text().split("：")[1];
						String sellTell = ps.get(3).text().split("：")[1];
						String serviceTell = ps.get(4).text().split("：")[1];
						
						AgencyEntity agency = new AgencyEntity();
						agency.setAddress(address);
						agency.setCity(cityName);
						agency.setName(name);
						agency.setProvince(proName);
						agency.setSellTell(sellTell);
						agency.setServiceTell(serviceTell);
						//利用java.util.Date()中的getTime()方法，获取当前时间的毫秒值，然后传给
						//java.sql.Date()。然后将java.util.Date()存入数据库。
						agency.setDate(new java.sql.Date(new java.util.Date().getTime()));
						agency.setBrand("马自达");
						agency.setCompany("一汽马自达");
						
						//System.out.println(agency);
						list.add(agency);
						
						
					}
					
					
				}
			}
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return list;
	}
	public static void main(String[] args) {
		YiQiMaZiDaCrawler crawler = new YiQiMaZiDaCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
	}

}
