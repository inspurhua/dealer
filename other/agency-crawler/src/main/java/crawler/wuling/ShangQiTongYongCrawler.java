package crawler.wuling;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;
//证书问题
public class ShangQiTongYongCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "https://www.sgmw.com.cn/js/dealer_data.js";
		String proText = HttpConnectionGet.getJson(getPro);
		JSONArray proArray = JSON.parseObject(proText).getJSONArray("citylist");
		for(int i=0;i<proArray.size();i++) {
			String proName = proArray.getJSONObject(i).getString("p");
			JSONArray cityArray = proArray.getJSONObject(i).getJSONArray("c");
			for(int j=0;j<cityArray.size();j++) {
				String cityName = cityArray.getJSONObject(j).getString("n");
				String cityNum = cityArray.getJSONObject(j).getString("nid");
				//获取经销商
				String getDealer = "https://www.sgmw.com.cn/ashx/dealerInfo.ashx?city="+cityNum;
				String dealerText = HttpConnectionGet.getJson(getDealer);
				JSONArray dealerArray = null;
				try {
					dealerArray = JSON.parseObject(dealerText).getJSONArray("data");
				} catch (Exception e) {
					System.out.println("JSON格式不标准");
					System.out.println(cityName+":"+cityNum);
					continue;
				}
				for(int k=0;k<dealerArray.size();k++) {
					JSONObject dealer = dealerArray.getJSONObject(k);
					String address = dealer.getString("address");
					String name = dealer.getString("company");
					String sellTell = dealer.getString("tel");
					String lngAndLatStr = dealer.getString("point");
					String lng = null;
					String lat = null;
					if (lngAndLatStr==null || lngAndLatStr.isEmpty() || lngAndLatStr.split(",").length<2) {
						String[] lngAndLat = Location.getLocation(address);
						lng = lngAndLat[0];
						lat = lngAndLat[1];
					} else {
						String[] lngAndLat = lngAndLatStr.split(",");
						lng = lngAndLat[0];
						lat = lngAndLat[1];
						
					}
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("宝骏");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("上汽通用五菱");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
					
					
				}
				
				
				
			}
			
			
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		ShangQiTongYongCrawler crawler = new ShangQiTongYongCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
