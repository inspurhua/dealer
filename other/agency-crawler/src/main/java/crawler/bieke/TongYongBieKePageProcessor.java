package crawler.bieke;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月2日 下午12:03:28 
* 类说明 
*/
public class TongYongBieKePageProcessor implements PageProcessor {

	@Override
	public void process(Page page) {
		//System.out.println(page.getJson().get());
		List<String> name = page.getJson().jsonPath("$[*].dealerName").all();
		//System.out.println(name);
		List<String> address = page.getJson().jsonPath("$[*].address").all();
		//System.out.println(address);
		List<String> sellTell = page.getJson().jsonPath("$[*].tel").all();
		//System.out.println(sellTell);
		List<String> proName = page.getJson().jsonPath("$[*].provinceName").all();
		//System.out.println(proName);
		List<String> cityName = page.getJson().jsonPath("$[*].cityName").all();
		//System.out.println(cityName);
		List<String> lat = page.getJson().jsonPath("$[*].lat").all();
		//System.out.println(lat);
		List<String> lng = page.getJson().jsonPath("$[*].lng").all();
		//System.out.println(lng);
		List<String> dnull = new ArrayList<>();
		for(String n : name) {
			n = "";
			dnull.add(n);
		}
		AgencyEntity2 agency = new AgencyEntity2();
		//agency.setdCloseDate(null);
		//agency.setdOpeningDate(null);
		agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
		//agency.setnBrandID(-1);
		agency.setsBrand("别克");
		//agency.setnDealerIDWeb(-1);
		//agency.setnManufacturerID(-1);
		agency.setsManufacturer("通用别克");
		//agency.setnState(1);
		agency.setsAddress(address);
		agency.setsCity(cityName);
		agency.setsCustomerServiceCall(dnull);
		agency.setsDealerName(name);
		agency.setsDealerType(dnull);
		agency.setsProvince(proName);
		agency.setsSaleCall(sellTell);
		agency.setsLongitude(lng);
		agency.setsLatitude(lat);
		new AgencyDao().add(agency);
		//agency.setsControllingShareholder(sControllingShareholder);
		//agency.setsOtherShareholders(sOtherShareholders);
	}
private Site site = Site.me()
					.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
					.addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
					.addHeader("Accept-Encoding", "gzip, deflate, br")
					.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
					.addHeader("Host", "www.buick.com.cn")
					.addHeader("Referer", "https://www.buick.com.cn/dealer.html")
					.addHeader("X-Requested-With", "XMLHttpRequest")
					.addHeader("X-Tingyun-Id", "p35OnrDoP8k;r=132087709")
					.setSleepTime(0)
					.setTimeOut(60000)
					.setCycleRetryTimes(3)
					;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new TongYongBieKePageProcessor())
	.addUrl("http://www.buick.com.cn/api/dealer.aspx")
	.run();
}
}
 