package crawler.bieke;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class TongYongBieKe {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>(860);
		String url = "http://www.buick.com.cn/api/dealer.aspx";
		String jsonStr = HttpConnectionGet.getJson(url);
		JSONArray dealers = JSON.parseArray(jsonStr);
		//System.out.println(dealers);
		for (int i = 0; i < dealers.size(); i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String address = dealer.getString("address");
			String cityName = dealer.getString("cityName");
			String name = dealer.getString("dealerName");
			String lat = dealer.getString("lat");
			String lng = dealer.getString("lng");
			String proName = dealer.getString("provinceName");
			String sellTell = dealer.getString("tel");

			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);

			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("别克");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("通用别克");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);

			list.add(agency);

		}

		return list;
	}

	public static void main(String[] args) {
		TongYongBieKe crawler = new TongYongBieKe();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
