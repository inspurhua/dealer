package crawler.siwei;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.AddressComponent;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class HuaChenXinYuan {
	List<AgencyEntity> getAgency(){
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.swmmotors.com.cn/support-2_jxscx.php?do=getsellpoint";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		JSONArray dealers = JSON.parseArray(dealerStr);
		for (int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String name = dealer.getString("sjname");
			String address = dealer.getString("address");
			String sellTell = dealer.getString("phone");
			String cityName = dealer.getString("city");
			String lng = dealer.getString("jingdu");
			String lat = dealer.getString("weidu");
			
			String proName = ProCity.getData(lat, lng)[0];
			
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("斯威");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("华晨鑫源");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);


			
			
		}
		
		return list;
	}
	public static void main(String[] args) {
		HuaChenXinYuan crawler = new HuaChenXinYuan();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}
}
