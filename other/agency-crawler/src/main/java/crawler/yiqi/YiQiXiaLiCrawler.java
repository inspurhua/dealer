package crawler.yiqi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionPost;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class YiQiXiaLiCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			String getPro = "http://www.tjfaw.com/index.php/JXSCX.shtml";
			Document proDoc = Jsoup.connect(getPro).get();
			Element proSelect = proDoc.getElementById("province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for (int i = 1; i < proOptions.size(); i++) {
				String proNum = proOptions.get(i).attr("value");
				String proName = proOptions.get(i).text();

				String getCity = "http://www.tjfaw.com/index.php/getCitys.shtml";
				String paramCity = "provinceID=" + proNum;
				String textCity = HttpConnectionPost.getJson(getCity, paramCity);
				JSONArray cities = JSON.parseArray(textCity);
				for (int j = 0; j < cities.size(); j++) {
					String cityName = cities.getJSONObject(j).getString("city");
					String cityNum = cities.getJSONObject(j).getString("cityID");

					String getDealer = "http://www.tjfaw.com/index.php/getfriend1.shtml";
					String paramDealer = "cityID=" + cityNum;
					String textDealer = HttpConnectionPost.getJson(getDealer, paramDealer);

					JSONArray dealers = JSON.parseArray(textDealer);
					if (dealers == null) {
						continue;
					}
					for (int k = 0; k < dealers.size(); k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String address = dealer.getString("add");
						String name = dealer.getString("title");
						String sellTell = dealer.getString("tell");
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("一汽");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("一汽夏利");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);

					}

				}

			}

		} catch (Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}

		return list;
	}

	public static void main(String[] args) {
		YiQiXiaLiCrawler crawler = new YiQiXiaLiCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}

}
