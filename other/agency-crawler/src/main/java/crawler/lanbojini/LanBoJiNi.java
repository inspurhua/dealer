package crawler.lanbojini;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.ProCity;

public class LanBoJiNi {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "https://www.lamborghini.com/cn-en/dealers/get.json?location=0";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		JSONArray dealers = JSON.parseObject(dealerStr).getJSONArray("item");
		for (int i = 0; i < dealers.size(); i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			JSONObject dealerInfo = dealer.getJSONObject("address");
			String country = dealerInfo.getString("country");
			if (!"China".equals(country)) {
				continue;
			} else {
				String name = dealer.getJSONObject("company").getString("tradeName");
				String address = dealerInfo.getString("address");
				JSONObject coordinates = dealer.getJSONObject("coordinates");
				String lat = coordinates.getString("latitude");
				String lng = coordinates.getString("longitude");
				String sellTell = dealer.getJSONObject("contact").getString("phone");
				String[] proCity = ProCity.getData(lat, lng);
				String proName = proCity[0];
				String cityName = proCity[1];

				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("兰博基尼");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("兰博基尼");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);

				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				System.out.println(agency);
				list.add(agency);

			}

		}

		return list;
	}
	public static void main(String[] args) {
		LanBoJiNi crawler = new LanBoJiNi();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("存库完毕");
	}
}
