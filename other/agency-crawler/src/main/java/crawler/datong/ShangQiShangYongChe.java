package crawler.datong;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.MybatisTool;
import util.TianYanCha;

public class ShangQiShangYongChe {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			//获取省份名和id
			Map<String, String> proAndID = new HashMap<String, String>();
			String provincesText = Jsoup.connect("https://www.saicmaxus.com/api/interface.php")
								.data("action", "getprobydealerinfo")
								.data("series", "-1").ignoreContentType(true).post().body().html();
			JSONArray provinces = JSON.parseObject(provincesText).getJSONArray("rows");
			for(int i=0;i<provinces.size();i++) {
				String proNum = provinces.getJSONObject(i).getString("provinceid");
				String proName = provinces.getJSONObject(i).getString("provincename");
				proAndID.put(proNum, proName);
			}
			
			//获取城市名和id
			Map<String, String> cityAndID = new HashMap<String, String>();
			String citiesText = Jsoup.connect("https://www.saicmaxus.com/api/interface.php")
					.data("action", "getcitybydealerinfo")
					.data("series", "-1")
					.data("pid", "-1").ignoreContentType(true).post().body().html();
			JSONArray cities = JSON.parseObject(citiesText).getJSONArray("rows");
			//System.out.println(citiesText);
			for(int j=0;j<cities.size();j++) {
				String cityNum = cities.getJSONObject(j).getString("CityID");
				String cityName = cities.getJSONObject(j).getString("CityName");
				cityAndID.put(cityNum, cityName);
			}
			
			String dealersText = Jsoup.connect("https://www.saicmaxus.com/api/interface.php")
					.data("action", "getdealerinfo")
					.data("series", "-1")
					.data("pid", "-1")
					.data("cid", "-1").ignoreContentType(true).post().body().html();
			JSONArray dealers = JSON.parseObject(dealersText).getJSONArray("rows");
			for(int k=0;k<dealers.size();k++) {
				JSONObject dealer = dealers.getJSONObject(k);
				String name = dealer.getString("CmyName");
				if(name==null) {
					continue;
				}
				String address = dealer.getString("Address");
				String sellTell = dealer.getString("Phone");
				String proID = dealer.getString("ProvinceID");
				String cityID = dealer.getString("CityID");
				String lng = dealer.getString("north");
				String lat = dealer.getString("east");
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);

				String proName = proAndID.get(proID);
				String cityName = cityAndID.get(cityID);
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("大通");

				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("上汽商用车");

				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				//System.out.println(agency);
				list.add(agency);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	public static void main(String[] args) {
		ShangQiShangYongChe crawler = new ShangQiShangYongChe();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
