package crawler.biaozhi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyEntity;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import util.Location;
import util.MybatisTool;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年12月5日 上午11:16:42 
* 类说明 
*/
public class DongFengBiaoZhiPageProcessor implements PageProcessor {
	private static List<String> pro10 = new ArrayList<>();
	private static List<String> city10 = new ArrayList<>();
	private static List<String> cityid10 = new ArrayList<>();
	private static List<String> dealerid00;
	private static String proName ="";
	private static String cityName ="";
	private static List<AgencyEntity> list = new ArrayList<AgencyEntity>();
	@Override
	public void process(Page page) {
		if(page.getUrl().regex("index").match()) {
		List<String> pro00 = page.getHtml().xpath("div[@class='allCity']/div[@class='cityGroup']/div[@class='cityProvince']/text()").all() ;
		for(int i=0;i<pro00.size();i++) {
			List<String> city00 = page.getHtml().xpath("div[@class='allCity']/div[@class='cityGroup']["+(i+1)+"]/ul/li/a/text()").all() ;
			List<String> cityid00 = page.getHtml().xpath("div[@class='allCity']/div[@class='cityGroup']["+(i+1)+"]/ul/li/a").regex("[0-9]{4}").all() ;
			for(int j=0;j<city00.size();j++) {
				pro10.add(pro00.get(i));
			}
			city10.addAll(city00);
			cityid10.addAll(cityid00);
		}
		}else
			if(page.getUrl().regex("ajax").match()) {
			dealerid00 = page.getHtml().xpath("option/@value").all();
			}else {
				String name = page.getHtml().xpath("div[@class='moshop_name']/text()").get();
				List<String> nn3 = page.getHtml().xpath("div[@class='row']/div[@class='position_right']/ul/li/text()").all();
				String address = "";
				String sellTell = "";
				String serviceTell = "";
				for(String n : nn3) {
					if(n.contains("地址")) {
						address = n.replace("地址：", "");
					}
					if(n.contains("销售热线")) {
						sellTell = n.replace("销售热线：", "");
					}
					if(n.contains("服务热线")) {
						serviceTell = n.replace("服务热线：", "");
					}
				}
				String[] lngAndLat = Location.getLocation(address);
				String lng = lngAndLat[0];
				String lat = lngAndLat[1];
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("标致");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("东风标致");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(serviceTell);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(null);
				agency.setsOtherShareholders(null);
				list.add(agency);

			}
	}
	private Site site = Site.me()
							.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0")
							.addHeader("Host", "dealer.peugeot.com.cn")
							.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
							.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
							.addHeader("Accept-Encoding", "gzip, deflate")
							.addHeader("Referer", "http://www.peugeot.com.cn/")
							.addHeader("Connection", "keep-alive")
							.addHeader("Upgrade-Insecure-Requests", "1")
							.addHeader("Cache-Control", "max-age=0")
							.setSleepTime(10)
							.setCycleRetryTimes(5);
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new DongFengBiaoZhiPageProcessor())
	.addUrl("http://dealer.peugeot.com.cn/index.php")
	.run();
	for(int i=0;i<cityid10.size();i++) {
		Spider.create(new DongFengBiaoZhiPageProcessor())
		.addUrl("http://dealer.peugeot.com.cn/ajax.php?cid="+cityid10.get(i)+"&action=dealer")
		.run();
		for(int j=1;j<dealerid00.size();j++) {
			proName = pro10.get(i);
			cityName = city10.get(i);
			Spider.create(new DongFengBiaoZhiPageProcessor())
			.addUrl("http://dealer.peugeot.com.cn/dealer/"+dealerid00.get(j))
			.run();
			
		}
	}
	System.out.println("抓取完毕,正在存库");
	for(AgencyEntity agency : list) {
		MybatisTool.save(agency);
	}
	MybatisTool.close();
	System.out.println("请查看数据库");
}
}
 