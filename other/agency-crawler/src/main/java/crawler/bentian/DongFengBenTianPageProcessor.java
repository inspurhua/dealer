package crawler.bentian;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.utils.HttpConstant;
import util.Location;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月8日 下午4:33:10 
* 类说明 
*/
public class DongFengBenTianPageProcessor implements PageProcessor {
	private static Site site ;
	private static String purl = "http://www.dongfeng-honda.com/dot_query.shtml";
	private static String curl00 = "http://www.dongfeng-honda.com/index/get_city_bypid/[0-9]{1,10}";
	private static List<String> pid;
	private static List<String> pname;
	private static List<String> cpname = new ArrayList<>();
	private static String cpname10;
	private static List<String> cname = new ArrayList<>();
	private static String dpname00;
	private static String dcname00;
	@Override
	public void process(Page page) {
		if(page.getUrl().get().equals(purl)) {
			pid  = page.getHtml().xpath("select[@name='province']/option[@value!=0]/@province_id").all();
			//System.out.println(pid);
			pname  = page.getHtml().xpath("select[@name='province']/option[@value!=0]/text()").all();
			//System.out.println(pname);
		}else if(page.getUrl().regex(curl00).match()){
			//System.out.println(page.getHtml().get());
			List<String> cname00  = page.getHtml().xpath("option[@value!=0]/text()").all();
			//System.out.println(cname00);
			cname.addAll(cname00);
			//System.out.println(cname00);
			for(int i=0;i<cname00.size();i++) {
				cpname.add(cpname10);
			}
		}else {
			List<String> name = page.getHtml().xpath("section[@class='stores']/ul/li/h4/text()").all();
			//System.out.println(name);
			List<String> address = page.getHtml().xpath("section[@class='stores']/ul/li/p[1]/text()").all();
			//System.out.println(address);
			List<String> sellTell = page.getHtml().xpath("section[@class='stores']/ul/li/p[2]/text()").regex("[0-9-()（）]{7,18}").all();
			//System.out.println(sellTell);
			List<String> serviceTell = page.getHtml().xpath("section[@class='stores']/ul/li/p[3]/text()").regex("[0-9-()（）]{7,18}").all();
			//System.out.println(serviceTell);
			List<String> lng = new ArrayList<>();
			List<String> lat = new ArrayList<>();
			List<String> proName = new ArrayList<>();
			List<String> cityName = new ArrayList<>(); 
			List<String> dnull = new ArrayList<>(); 
			for(int i=0;i<name.size();i++) {
				proName.add(dpname00);
				cityName.add(dcname00);
				String[] lngAndLat = Location.getLocation(address.get(i));
				String lng00 = lngAndLat[0];
				String lat00 = lngAndLat[1];
				lng.add(lng00);
				lat.add(lat00);
				dnull.add("");
			}
			AgencyEntity2 agency = new AgencyEntity2();
			//agency.setdCloseDate(null);
			//agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			//agency.setnBrandID(-1);
			agency.setsBrand("本田");
			//agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("东风本田");
			//agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsDealerName(name);
			agency.setsDealerType(dnull);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			//agency.setsControllingShareholder(sControllingShareholder);
			//agency.setsOtherShareholders(sOtherShareholders);
			new AgencyDao().add(agency);
		}
	}
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
			.addHeader("Connection", "keep-alive")
			.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
			.addHeader("Host", "www.dongfeng-honda.com")
			.addHeader("Referer", "http://www.dongfeng-honda.com/serviceitems.shtml")
			.addHeader("Upgrade-Insecure-Requests", "1")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(3)
			;
	Spider.create(new DongFengBenTianPageProcessor())
	.addUrl("http://www.dongfeng-honda.com/dot_query.shtml")
	.run();
	for(int i=0;i<pid.size();i++) {
	cpname10 = pname.get(i);
	 site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
			.addHeader("Accept", "*/*")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.dongfeng-honda.com/dot_query.shtml")
			.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
			.setSleepTime(0)
			.setCycleRetryTimes(3)
			;
	Request request = new Request("http://www.dongfeng-honda.com/index/get_city_bypid/"+pid.get(i));
	//Request request = new Request("http://www.dongfeng-honda.com/index/get_city_bypid/1");
	request.setMethod(HttpConstant.Method.POST);
	request.setRequestBody(HttpRequestBody.json("dealer_type=dot_query&ajax=true","utf-8"));
	Spider.create(new DongFengBenTianPageProcessor())
	.addRequest(request)
	.run();
	}
	for(int j=0;j<cname.size();j++) {
	//dpname00 = "安徽";
	//dcname00 = "安庆";
	dpname00 = cpname.get(j);
	dcname00 = cname.get(j);
	//String curl = "http://www.dongfeng-honda.com/dot_query.shtml?province=安徽&city=安庆";
	String curl = "http://www.dongfeng-honda.com/dot_query.shtml?province="+dpname00+"&city="+dcname00;
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
			.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
			.addHeader("Connection", "keep-alive")
			.addHeader("Host", "www.dongfeng-honda.com")
			.addHeader("Upgrade-Insecure-Requests", "1")
			.addHeader("Referer", curl)
			.setSleepTime(0)
			.setCycleRetryTimes(3)
			;
	Spider.create(new DongFengBenTianPageProcessor())
	.addUrl(curl)
	.run();
	}
}
}
 