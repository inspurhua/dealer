package crawler.bentian;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class GuangQiBenTian {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			String urlData = "https://www.ghac.cn/js/Official/staticData/p_c_dealers_data.js";
			String jsStr = HttpConnectionGet.getJson(urlData).replaceAll("\\$([\\s\\S]*)\\(global_dealers_data\\)", "");
			String proStr = (String) engine.eval(jsStr + " JSON.stringify(global_province_data);");
			String cityStr = (String) engine.eval(jsStr + " JSON.stringify(global_city_data);");
			String dealerStr = (String) engine.eval(jsStr + " JSON.stringify(global_dealers_data);");
			JSONArray pros = JSON.parseArray(proStr);
			JSONArray citys = JSON.parseArray(cityStr);

			JSONArray dealers = JSON.parseArray(dealerStr);
			for (int i = 0; i < dealers.size(); i++) {
				JSONObject dealer = dealers.getJSONObject(i);
				String name = dealer.getString("REGISTRATION_NAME");
				String lng = dealer.getString("LONGITUDE");
				String lat = dealer.getString("LATITUDE");
				String address = dealer.getString("ADDRESS");
				String sellTell = dealer.getString("SALES_PHONE");
				String serviceTell = dealer.getString("AFTER_SALES_PHONE");
				String cityNum = dealer.getString("CITY_ID");
				String cityName = null;
				String proName = null;
				String proNum = null;
				for (int j = 0; j < citys.size(); j++) {
					JSONObject city = citys.getJSONObject(j);
					boolean flag = city.containsValue(cityNum);
					if (flag) {
						cityName = city.getString("CITY_NAME");
						proNum = city.getString("PROVINCE_ID");
						break;
					}
				}
				for (int k = 0; k < pros.size(); k++) {
					JSONObject pro = pros.getJSONObject(k);
					boolean flag = pro.containsValue(proNum);
					if (flag) {
						proName = pro.getString("PROVINCE_NAME");
						break;
					}
				}
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);

				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("本田");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("广汽本田");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(serviceTell);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);

				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);

				list.add(agency);

			}

		} catch (ScriptException e) {
			e.printStackTrace();
		}

		return list;
	}

	public static void main(String[] args) {
		GuangQiBenTian crawler = new GuangQiBenTian();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
