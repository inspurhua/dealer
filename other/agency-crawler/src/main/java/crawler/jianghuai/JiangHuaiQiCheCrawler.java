package crawler.jianghuai;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;
public class JiangHuaiQiCheCrawler {
	List<String> names = new ArrayList<String>();
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//从js文件中获取所有数据
		String getData = "http://www.jac.com.cn/r/cms/www/red/js/provinceandcity3.js";
		String allData = HttpConnectionGet.getJson(getData);
		//截取省份信息(不包含台湾省)
		int startPro = allData.indexOf("cityArray[0]");
		int endPro = allData.indexOf("cityArray[33]");
		String proData = allData.substring(startPro, endPro);
		//cityArray[0] = new Array("北京市","北京"); 
		//System.out.println(proData);
		//利用正则表达式,获取小括号里的信息
		String regexPro = "(?<=\\().*?(?=\\))";
		Pattern proPatt = Pattern.compile(regexPro);
		Matcher proMat = proPatt.matcher(proData);
		//将省和城市存入一个map中
		Map<String, String> proMap = new HashMap<String, String>();
		while(proMat.find()) {
			//System.out.println(proMat.group());//"北京市","北京"
			String[] proAndCity = proMat.group().replaceAll("\"", "").split(",");
			//System.out.println(proAndCity[1]);
			proMap.put(proAndCity[0], proAndCity[1].trim());
		}
		//瑞风list
		//从js中获取瑞风车系
		int startRF = allData.indexOf("if(code=='瑞风')");
		int endRF = allData.indexOf("if(code=='和悦')");
		String textRF = allData.substring(startRF, endRF);
		List<String> listRF = getSeries(textRF);
		//System.out.println(listRF);
		list.addAll(getDealer(proMap, "瑞风", listRF));
		//和悦List
		int startHY = allData.indexOf("if(code=='和悦')");
		int endHY = allData.indexOf("if(code=='iEV')");
		String textHY = allData.substring(startHY, endHY);
		List<String> listHY = getSeries(textHY);
		//System.out.println(listHY);
		list.addAll(getDealer(proMap, "和悦", listHY));

		//iEV
		int startIEV = allData.indexOf("if(code=='iEV')");
		int endIEV = allData.indexOf("if(code=='星锐')");
		String textIEV = allData.substring(startIEV, endIEV);
		List<String> listIEV = getSeries(textIEV);
		//System.out.println(listIEV);
		list.addAll(getDealer(proMap, "iEV", listIEV));
		
		//星锐
		int startXR = allData.indexOf("if(code=='星锐')");
		int endXR = allData.indexOf("if(code=='帅铃')");
		String textXR = allData.substring(startXR, endXR);
		List<String> listXR = getSeries(textXR);
		//System.out.println(listXR);
		list.addAll(getDealer(proMap, "星锐", listXR));

		//帅铃
		int startSL = allData.indexOf("if(code=='帅铃')");
		int endSL = allData.indexOf("if(code=='骏铃')");
		String textSL = allData.substring(startSL, endSL);
		List<String> listSL = getSeries(textSL);
		//System.out.println(listSL);
		list.addAll(getDealer(proMap, "帅铃", listSL));

		//骏铃
		int startJL = allData.indexOf("if(code=='骏铃')");
		int endJL = allData.indexOf("if(code=='康铃')");
		String textJL = allData.substring(startJL, endJL);
		List<String> listJL = getSeries(textJL);
		//System.out.println(listJL);
		list.addAll(getDealer(proMap, "骏铃", listJL));

		//康铃
		int startKL = allData.indexOf("if(code=='康铃')");
		int endKL = allData.indexOf("if(code=='格尔发')");
		String textKL = allData.substring(startKL, endKL);
		List<String> listKL = getSeries(textKL);
		//System.out.println(listKL);
		list.addAll(getDealer(proMap, "康铃", listKL));

		//格尔发
		int startGEF = allData.indexOf("if(code=='格尔发')");
		int endGEF = allData.indexOf("$(\"#CarHtml\").html(carHTML);");
		String textGEF = allData.substring(startGEF, endGEF);
		List<String> listGEF = getSeries(textGEF);
		//System.out.println(listGEF);
		list.addAll(getDealer(proMap, "格尔发", listGEF));

		//对List去重
		List<AgencyEntity> listTemp = new ArrayList<AgencyEntity>();
		for(AgencyEntity agency : list) {
			if(!listTemp.contains(agency)) {
				listTemp.add(agency);
			}
			
		}
		
		
		
		return listTemp;
	}
	//利用正则取车系
	public List<String> getSeries(String data) {
		List<String> list = new ArrayList<String>();
		
		String regex = "(?<=value=').*?(?=')";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(data);
		while(m.find()) {
			list.add(m.group());
		}
		return list;
	}
	//获取经销商
	public List<AgencyEntity> getDealer(Map<String, String> proMap, String brand, List<String> listSeries) {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		for(String series : listSeries) {
			Set<Entry<String, String>> set = proMap.entrySet();
			for(Entry<String, String> entry : set) {
				String proName = entry.getKey();
				String[] cities = entry.getValue().split("\\|");
				for(String city : cities) {
					String getDealer = "http://www.jac.com.cn/jacservice/searchdealers?city="+city+"&prince="+proName+"&type=2&jacmodel="+series+"&jacbrand="+brand;
					String dealerText = HttpConnectionGet.getJson(getDealer);
					if(dealerText.isEmpty()) {
						continue;
					}
					JSONArray dealerArray = JSON.parseArray(dealerText);
					for(int i=0;i<dealerArray.size();i++) {
						String name = dealerArray.getJSONObject(i).getString("DealerName");
						if (names.contains(name)) {
							continue;
						} else {
							names.add(name);
						}
						String sellTell = dealerArray.getJSONObject(i).getString("Salestell");
						String address = dealerArray.getJSONObject(i).getString("Addr");
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("江淮");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("江淮汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(city);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					}
				}
			}
		}
		return list;
	}
	
	public static void main(String[] args) {
		JiangHuaiQiCheCrawler crawler = new JiangHuaiQiCheCrawler();
		System.out.println("爬虫开始...");
		//AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		//CreateTable.addTable(tableName);

/*		for(AgencyEntity agency : agencys) {
			dao.save(agency);
		}*/
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
