package crawler.volvo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class VolvoCrawler {
	public List<AgencyEntity> getAgency() {
		
		new Thread(()->{
			System.out.println("");
		}).start();
		
		
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String path = "https://www.volvocars.com/data/dealers?marketSegment=%2Fzh-cn&expand=Services%2CUrls&format=json&northToSouthSearch=False&filter=MarketId+eq+%27cn%27+and+LanguageId+eq+%27zh%27";
		String text = HttpConnectionGet.getJson(path);
		JSONArray dealers = JSON.parseArray(text);
		for(int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String proName = dealer.getString("District");
			String cityName = dealer.getString("City");
			String name = dealer.getString("AddressLine1");
			String address = dealer.getString("AddressLine2");
			String sellTell = dealer.getString("Phone");
			JSONObject lngAndLat = dealer.getJSONObject("GeoCode");
			String lng = lngAndLat.getString("Longitude");
			String lat = lngAndLat.getString("Latitude");
			
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("沃尔沃");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("沃尔沃");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);

			
			list.add(agency);
		
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		VolvoCrawler crawler = new VolvoCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
