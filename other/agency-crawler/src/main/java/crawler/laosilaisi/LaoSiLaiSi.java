package crawler.laosilaisi;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;

public class LaoSiLaiSi {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.rolls-roycemotorcars.com.cn/etc/designs/rollsroyce-website/assets/js/cn/dealers.js";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		System.out.println("===="+dealerStr);
		JSONArray dealers = JSON.parseArray(dealerStr);
		for (int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String type = dealer.getString("type");
			String name = dealer.getString("title");
			String address = dealer.getString("address");
			String proName = dealer.getString("province");
			String cityName = dealer.getString("city");
			String sellTell = "";
			String serviceTell = "";
			if ("dealer".equals(type)) {
				sellTell = dealer.getString("telephone");
			} else {
				serviceTell = dealer.getString("telephone");
			}
			String lng = dealer.getString("longitude");
			String lat = dealer.getString("latitude");
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("劳斯莱斯");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("劳斯莱斯汽车");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			System.out.println(agency);
			list.add(agency);
			
		}
		return list;
	}
	public static void main(String[] args) {
		LaoSiLaiSi crawler = new LaoSiLaiSi();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
/*		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");*/
		
	}
}
