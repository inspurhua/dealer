package crawler.laosilaisi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import util.HttpConnectionGet;
import util.MybatisTool;

public class LaoSiLaiSiPageProcessor implements PageProcessor {
private static 	List<AgencyEntity> list = new ArrayList<AgencyEntity>();
	@Override
	public void process(Page page) {
		String dealerStr = page.getJson().get();
		JSONArray dealers = JSON.parseArray(dealerStr);
		//System.out.println(dealers.size());
		for (int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String type = dealer.getString("type");
			String name = dealer.getString("title");
			String address = dealer.getString("address");
			String proName = dealer.getString("province");
			String cityName = dealer.getString("city");
			String sellTell = "";
			String serviceTell = "";
			if ("dealer".equals(type)) {
				sellTell = dealer.getString("telephone");
			} else {
				serviceTell = dealer.getString("telephone");
			}
			String lng = dealer.getString("longitude");
			String lat = dealer.getString("latitude");
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("劳斯莱斯");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("劳斯莱斯汽车");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			//System.out.println(agency);
			list.add(agency);
			
		}
	}
	private Site site = Site.me();
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new LaoSiLaiSiPageProcessor())
	.addUrl("http://www.rolls-roycemotorcars.com.cn/etc/designs/rollsroyce-website/assets/js/cn/dealers.js")
	.run();
	System.out.println("抓取完毕,正在存库");
	for(AgencyEntity agency : list) {
		MybatisTool.save(agency);
	}
	MybatisTool.close();
	System.out.println("请查看数据库");
	
}
}
