package crawler.baoma;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class HuaChenBaoMaCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		// 获取城市和省份的字符串
		String getProAndCity = "https://cn-digital2-app.bmw.com.cn/dlo/init";
		String proAndCityText = getJSON(getProAndCity);
		//System.out.println(proAndCityText);
		// 将字符串截取为JSON格式
		int startPro = proAndCityText.indexOf("{\"");
		int endPro = proAndCityText.length() - 1;
		String proJSONText = proAndCityText.substring(startPro, endPro);
		// 用JSON提取省份的JSONArray
		JSONArray proArray = JSON.parseObject(proJSONText).getJSONObject("responseBody").getJSONArray("provinces");
		// 遍历proArray,将省份id和name存到一个map中
		Map<String, String> proMap = new HashMap<String, String>();
		for (int i = 0; i < proArray.size(); i++) {
			String proNum = proArray.getJSONObject(i).getString("id");
			String proName = proArray.getJSONObject(i).getString("nz");
			proMap.put(proNum, proName);
		}

		// 获取城市名和id
		JSONArray cityArray = JSON.parseObject(proJSONText).getJSONObject("responseBody").getJSONArray("cities");
		for (int j = 0; j < cityArray.size(); j++) {
			String cityNum = cityArray.getJSONObject(j).getString("id");
			String cityName = cityArray.getJSONObject(j).getString("nz");
			String proNum = cityArray.getJSONObject(j).getString("pv");
			String proName = proMap.get(proNum);
			// 根据省份id和城市id,获取经销商列表
			String getDealer = "https://cn-digital2-app.bmw.com.cn/dlo/outlet?callbackparam=CALLBACK&province=" + proNum
					+ "&city=" + cityNum + "&serviceCodes=&_=1501638785880";
			String dealerText = getJSON(getDealer);
			//System.out.println(dealerText);
			// 截取字符串,使之符合JSON格式
			int startDealer = dealerText.indexOf("{\"");
			int endDealer = dealerText.length() - 1;
			String dealerJSONText = dealerText.substring(startDealer, endDealer);
			JSONArray dealerArray = JSON.parseObject(dealerJSONText).getJSONObject("responseBody")
					.getJSONArray("outlets");
			for (int k = 0; k < dealerArray.size(); k++) {
				JSONObject dealer = dealerArray.getJSONObject(k);
				String name = dealer.getString("nz");
				String address = dealer.getString("az");
				String sellTell = dealer.getString("tel");
				String type = null;
				String display = dealer.getJSONObject("rtlFmt").getString("display");
				if ("1".equals(display)) {
					type = dealer.getJSONObject("rtlFmt").getString("textZh");;
				}
				String lng = dealer.getString("lnb");
				String lat = dealer.getString("ltb");
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				JSONArray serviceCodes = dealer.getJSONArray("serviceCodes");
				StringBuffer remarks = new StringBuffer();
				if (serviceCodes.contains("20")) {
					remarks.append("二手车认证/");
				}
				if (serviceCodes.contains("03")) {
					remarks.append("大客户服务/");
				}
				if (serviceCodes.contains("05")) {
					remarks.append("BMWi 标示/");
				}
				if (serviceCodes.contains("06")) {
					remarks.append("BMWm 标示/");
				}
				
				
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("宝马");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("华晨宝马");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(type);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				agency.setsRemarks(remarks.toString());
				
				System.out.println(agency);
				list.add(agency);

			}

		}

		return list;
	}

	public String getJSON(String strURL) {

		String all = null;
		try {
			URL url = new URL(strURL);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setRequestProperty("Accept", "*/*");
			httpConn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			httpConn.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8");
			httpConn.setRequestProperty("Connection", "keep-alive");
			httpConn.setRequestProperty("Cookie",
					"bw=5000Kbps+; AMCVS_B52D1CFE5330949C0A490D45%40AdobeOrg=1; __clickidc=150163680119569638; AMCV_B52D1CFE5330949C0A490D45%40AdobeOrg=2096510701%7CMCIDTS%7C17381%7CMCMID%7C91358386290842717202012529904130445322%7CMCAAMLH-1502241599%7C11%7CMCAAMB-1502241599%7CNRX38WO0n5BH8Th-nqAG_A%7CMCOPTOUT-1501643999s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-17388%7CvVersion%7C2.0.0; bmwdtm_hq_vs=1501636801; s_sq=%5B%5BB%5D%5D; bmwdtm_hq_sid=h721gBx4mUpe; bmwdtm_hq_pcg=fastlane%7Cfastlane%20%3E%20dealer-locator%7Cfastlane%20%3E%20dealer-locator%7Cfastlane%20%3E%20dealer-locator%7Cdealer-locator; s_lv=1501638672779; s_lv_s=First%20Visit; s_cc=true; bmwdtm_hq_cc=true; mbox=session#bd36bc6ddd784d838c22cee0e16cfe91#1501640647|PC#bd36bc6ddd784d838c22cee0e16cfe91.24_5#1502848387|check#true#1501638847");
			httpConn.setRequestProperty("Host", "cn-digital2-app.bmw.com.cn");
			httpConn.setRequestProperty("Referer", "http://www.bmw.com.cn/zh/fastlane/dealer-locator.html");
			httpConn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36");

			BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			String line = null;

			while ((line = reader.readLine()) != null) {

				// System.out.println(line);
				all += line;
			}
		} catch (Exception e) {
			throw new RuntimeException("获取城市和省份异常", e);
		}
		return all;
	}

	public static void main(String[] args) {
		HuaChenBaoMaCrawler crawler = new HuaChenBaoMaCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}

}
