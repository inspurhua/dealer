package crawler.luhuqiche;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class LuHuQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//String getDealer = "http://v2.dealer.inzenclients.net/thedealers/landrover?refresh=1";
		String getDealer = "http://dealer.landrover.com.cn/index.php?s=/LRDealer/api/getDealerList&is_extend=21";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		JSONArray dealerArray = JSON.parseObject(dealerText).getJSONArray("data");
		for(int i=0;i<dealerArray.size();i++) {
			JSONObject dealer = dealerArray.getJSONObject(i);
			String cityName = dealer.getString("city_name");
			String proName = dealer.getString("province_name");
			String name = dealer.getString("dealer_name");
			String address = dealer.getString("addr");
			String sellTell = dealer.getString("sales_phone_landrover");
			String serviceTell = dealer.getString("service_phone_landrover");
			String lng = dealer.getString("longitude");
			String lat = dealer.getString("latitude");
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("路虎");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("路虎汽车");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);
		}
		
		return list;
	}
	public static void main(String[] args) {
		LuHuQiCheCrawler crawler = new LuHuQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
