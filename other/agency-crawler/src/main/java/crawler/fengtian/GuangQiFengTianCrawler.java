package crawler.fengtian;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;
//证书问题
public class GuangQiFengTianCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		try {
			// 获取省份的id和name
			String getPro = "https://www.gac-toyota.com.cn/js/newprovincecitydealer/data/provinceData.js";
			String proDataStr = HttpConnectionGet.getJson(getPro);
			String proStr = (String) engine.eval(proDataStr + " JSON.stringify(provinceJson)");
			JSONArray pros = JSON.parseArray(proStr);
			Map<String, String> proInfo = new HashMap<String, String>();
			for (int i = 0; i < pros.size(); i++) {
				JSONObject pro = pros.getJSONObject(i);
				String proNum = pro.getString("value");
				String proName = pro.getString("name");
				proInfo.put(proNum, proName);
			}

			// 获取城市id和name
			String getCity = "https://www.gac-toyota.com.cn/js/newprovincecitydealer/data/cityData.js";
			String cityDataStr = HttpConnectionGet.getJson(getCity);
			String cirysStr = (String) engine.eval(cityDataStr + " JSON.stringify(cityJson)");
			JSONArray citys = JSON.parseArray(cirysStr);
			Map<String, String[]> cityInfo = new HashMap<String, String[]>();
			for (int j = 0; j < citys.size(); j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityNum = city.getString("value");
				String cityName = city.getString("name");
				String proNum = city.getString("parent");
				String proName = proInfo.get(proNum);
				String[] proAndCity = { proName, cityName };
				cityInfo.put(cityNum, proAndCity);
			}
			// 获取经销商
			String getDealer = "https://www.gac-toyota.com.cn/js/newprovincecitydealer/data/dealerData.js";
			String dealerDataStr = HttpConnectionGet.getJson(getDealer);
			String dealersStr = (String) engine.eval(dealerDataStr + " JSON.stringify(dealerJson)");

			JSONArray dealers = JSON.parseArray(dealersStr);
			for (int k = 0; k < dealers.size(); k++) {
				JSONObject dealer = dealers.getJSONObject(k);
				String cityNum = dealer.getString("City");
				if(!cityNum.equals("")) {
				String name = dealer.getString("DealerName");
				String address = dealer.getString("Address");
				String sellTell = dealer.getString("Tel");
				String lng = dealer.getString("Longitude");
				String lat = dealer.getString("Latitude");
				String[] proAndCity = cityInfo.get(cityNum);
				System.out.println("=="+proAndCity);
				String proName = proAndCity[0];
				String cityName = proAndCity[1];
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("丰田");

				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("广汽丰田");

				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				System.out.println(agency);
				list.add(agency);
			}else {}
			}

		} catch (

		Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}

		return list;
	}

	public static void main(String[] args) {
		System.out.println("爬虫开始...");
		GuangQiFengTianCrawler crawler = new GuangQiFengTianCrawler();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}

}
