package crawler.fengtian;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import crawler.bieke.TongYongBieKe;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class YiQiFengTian {
	List<AgencyEntity> getAgency() {
		/*
		 * "id": "277", "name": "济南明星", "lng": "116.923150", "lat": "36.656642",
		 * "province": "山东", "city": "济南", "address": "济南市槐荫区经十西路28438号",
		 * "phone_service": "0531-87517799", "phone_seal":
		 * "0531-87513599|0531-87518899", "website": "jnmx",
		 */
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
//		String getPro = "http://www.ftms.com.cn/app/dealer/province";
		String getPro= "https://www.ftms.com.cn/website/Maintenance/getProvince";
		String proStr = HttpConnectionGet.getJson(getPro);
		JSONArray provinces = JSON.parseArray(proStr);
		for (int i = 1; i < provinces.size(); i++) {
			JSONObject pro = provinces.getJSONObject(i);
			String proName = pro.getString("name");

			String getDealer = "http://www.ftms.com.cn/app/dealer/city_stro?province=" + proName;
			String dealerStr = HttpConnectionGet.getJson(getDealer);
			JSONArray citys = JSON.parseArray(dealerStr);
			for (int j = 0; j < citys.size(); j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityName = city.getString("name");
				JSONArray dealers = city.getJSONArray("dealer");
				if ((dealers == null) || (dealers.isEmpty())) {
					continue;
				}
				for (int k = 0; k < dealers.size(); k++) {
					JSONObject dealer = dealers.getJSONObject(k);
					String name = dealer.getString("name");
					String address = dealer.getString("address");
					String sellTell = dealer.getString("phone_seal");
					String serviceTell = dealer.getString("phone_service");
					String lng = dealer.getString("lng");
					String lat = dealer.getString("lat");

					List<String> shareholder = TianYanCha.getShareholder(name+"丰田汽车");
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);

					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("丰田");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("一汽丰田");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(serviceTell);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);

					list.add(agency);

				}
			}
		}
		return list;
	}
	public static void main(String[] args) {
		YiQiFengTian crawler = new YiQiFengTian();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
