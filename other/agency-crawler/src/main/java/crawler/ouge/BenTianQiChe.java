package crawler.ouge;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.AddressComponent;
import util.Location;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class BenTianQiChe {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			String url = "http://www.acura.com.cn/zh-CN/dealer/dealers";
			Document doc = Jsoup.connect(url).get();
			Elements pros = doc.getElementsByClass("layer_box");
			for (Element pro : pros) {
				Elements dealers = pro.getElementsByClass("layer_com");
				for(Element dealer : dealers) {
					String name = dealer.getElementsByTag("strong").get(0).text();
					Elements ps = dealer.getElementsByTag("p");
					String address = ps.get(0).text().split("：")[1];
					String sellTell = ps.get(1).text().split("：")[1];
					
					String[] lngAndLat = Location.getLocation(address);
					String lng = lngAndLat[0];
					String lat = lngAndLat[1];
					String[] proCity = ProCity.getData(lat, lng);
					String proName = proCity[0];
					String cityName = proCity[1];
					
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("讴歌");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("本田汽车");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					
					list.add(agency);
				}
			}
		} catch(Exception e) {
			System.out.println("爬虫出错");
			e.printStackTrace();
		}
		return list;
	}
	
	public static void main(String[] args) {
		BenTianQiChe crawler = new BenTianQiChe();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
