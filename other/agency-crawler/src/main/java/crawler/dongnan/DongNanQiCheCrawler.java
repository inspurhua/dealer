package crawler.dongnan;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class DongNanQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try { 
			String getPro = "http://www.soueast-motor.com/content/index/12";
			Document proDoc = Jsoup.connect(getPro).get();
			Element proSelect = proDoc.getElementById("province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for(int i=0;i<proOptions.size();i++) {
				String proName = proOptions.get(i).text();
				
				//获取根据省名城市
				String getCity = "http://www.soueast-motor.com/content/getcityj";
				Document cityDoc = Jsoup.connect(getCity).data("province", proName).post();
				Elements cityOptions= cityDoc.getElementsByTag("option");
				for(int j=0;i<cityOptions.size();i++) {
					String cityName = cityOptions.get(j).text();
					
					//获取经销商
					String getDealer = "http://www.soueast-motor.com/content/shaixuan";
					String paramDealer = "province="+proName+"&city="+cityName;
					String dealerText = HttpConnectionPost.getJson(getDealer, paramDealer);
					//解析经销商json字符串
					JSONArray dealerArray = JSON.parseArray(dealerText);
					for(int k=0;k<dealerArray.size();k++) {
						JSONArray dealers = dealerArray.getJSONArray(k);
						String name = dealers.getString(6);
						String address = dealers.getString(4);
						String sellTell = dealers.getString(5);
						String lng = dealers.getString(0);
						String lat = dealers.getString(1);
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("东南");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("东南汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					}
					
					
				}
				
				
				
				
			}
			
			
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		
		return list;
	}
	public static void main(String[] args) {
		DongNanQiCheCrawler crawler = new DongNanQiCheCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
