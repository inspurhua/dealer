package crawler.hafu;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class ChangChengQiCheCrawler {
	public List<AgencyEntity> getAgency() throws UnsupportedEncodingException, ScriptException {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.gwm.com.cn/statics/gwm-cn/js/map/dealersShop.js";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		String dealerStr = (String)engine.eval(dealerText + " JSON.stringify(_CIADealersShopData)");
		JSONArray dealers = JSON.parseArray(dealerStr);
		for (int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String name = dealer.getString("Name");
			String dealerId = dealer.getString("DealersID");
			String address = dealer.getString("Address");
			String sellTell = dealer.getString("ShopSaleTel");
			String lng = dealer.getString("BaiduMapLng");
			String lat = dealer.getString("BaiduMapLat");
			String cityName = dealer.getString("City");
			String proName = dealer.getString("Province");
			List<String> shareholder = TianYanCha.getShareholder(dealerId);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("哈弗");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("长城汽车");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);
			
			
		}
	/*	List<String> names = JsonPath.read(dealerText, "$.[*].Name");
		List<String> addresses = JsonPath.read(dealerText, "$.[*].Address");
		List<String> cities = JsonPath.read(dealerText, "$.[*].City");
		List<String> provinces = JsonPath.read(dealerText, "$.[*].Province");
		List<String> tells = JsonPath.read(dealerText, "$.[*].ShopSaleTel");
		for(int i=0;i<names.size();i++) {
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("哈弗");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("哈弗汽车");

			agency.setnState(1);
			agency.setsAddress(addresses.get(i));
			agency.setsCity(cities.get(i));
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(names.get(i));
			agency.setsDealerType(null);
			agency.setsProvince(provinces.get(i));
			agency.setsSaleCall(tells.get(i));
			list.add(agency);
		}
		*/
		return list;
		
	}
	
	
	
	public static void main(String[] args) throws UnsupportedEncodingException, ScriptException {
		ChangChengQiCheCrawler crawler = new ChangChengQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
				
	}

}
