package crawler.jiulong;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.conn.HttpHostConnectException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class JiuLong {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.joylong.net/index.php?s=/online/stores.html";
		try {
			Document doc = Jsoup.connect(getPro).get();
			Element select = doc.getElementsByClass("select").get(2);
			Elements provinces = select.getElementsByTag("option");
			System.out.println(provinces.text());
			for (int i=1;i<provinces.size();i++) {
				Element pro = provinces.get(i);
				String proNum = pro.attr("value");
				String proName = pro.text();
				
				String getCiyt = "http://www.joylong.net/index.php?s=/region/lists.html&province="+proNum;
				String cityStr = HttpConnectionGet.getJson(getCiyt);
				JSONArray citys = JSON.parseArray(cityStr);
				for (int j=0;j<citys.size();j++) {
					JSONObject city = citys.getJSONObject(j);
					String cityNum = city.getString("code");
					String cityName = city.getString("title");
					
					String getDealer = "http://www.joylong.net/index.php?s=/online/getagencylist.html&city="+cityNum;
					String dealerStr = HttpConnectionGet.getJson(getDealer);
					JSONArray dealers = JSON.parseArray(dealerStr);
					for (int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String address = dealer.getString("address");
						System.out.println(address);
						if ((address == null) || (address.isEmpty())) {
							continue;
						}
						String name = dealer.getString("agency_title");
						String sellTell = dealer.getString("telphone");
						
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						

						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("九龙");
						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("九龙汽车");
						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);

						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						System.out.println(agency);
						list.add(agency);
						
						
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	public static void main(String[] args) {
		JiuLong crawler = new JiuLong();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
