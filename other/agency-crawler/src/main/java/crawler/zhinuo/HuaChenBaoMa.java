package crawler.zhinuo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class HuaChenBaoMa {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "https://www.zhinuo.com.cn/js/map.js";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		dealerStr = dealerStr.split("function addMarker")[0];
		String str = null;
		try {
			str = (String) engine.eval(dealerStr + " JSON.stringify(markerArr);");
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		//System.out.println(str);
		JSONArray dealers = JSON.parseArray(str);
		for (int i=0;i<dealers.size();i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String name = dealer.getString("title");
			String dealerInfo = dealer.getString("content");
			String address = dealerInfo.split("<br/>")[0];
			String sellTell = dealerInfo.split("<br/>")[1].split("：")[1];
			String[] lngAndLat = dealer.getString("point").split("\\|");
			String lng = lngAndLat[0];
			String lat = lngAndLat[1];
			String[] proCity = ProCity.getData(lat, lng);
			String proName = proCity[0];
			String cityName = proCity[1];
			
			
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);

			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("之诺");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("华晨宝马");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			//System.out.println(agency);
			list.add(agency);

			
			
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		HuaChenBaoMa crawler = new HuaChenBaoMa();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
