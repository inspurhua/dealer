package crawler.daoqi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.MybatisTool;
import util.TianYanCha;

public class KeLaiSiLeCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.chrysler.com.cn/js/dealer_new.xml";
		//1.创建SAXReader对象
		SAXReader reader = new SAXReader();
		try {
			//2.获取Document对象
			Document doc = reader.read(getDealer);
			//3.获取根元素
			Element root = doc.getRootElement();
			//4.从根元素下,获取需要的元素
			List<Element> dealers = root.elements("dealer");
			for(Element dealer : dealers) {
				String province = dealer.elementText("province");
				String city = dealer.elementText("city");
				String name = dealer.elementText("name");
				String sellTell = dealer.elementText("phone");
				String address = dealer.elementText("address").trim();
				String[] lngAndLat = dealer.elementText("map").split(",");
				String lng = lngAndLat[0];
				String lat = lngAndLat[1];
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				
				
				
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("道奇");

				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("克莱斯勒");

				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(city);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(province);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				list.add(agency);
			}
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		return list;
	}
	public static void main(String[] args) {
		KeLaiSiLeCrawler crawler = new KeLaiSiLeCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
