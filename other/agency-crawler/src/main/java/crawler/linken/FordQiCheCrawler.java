package crawler.linken;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class FordQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		
		try {
			String getProCity = "https://www.lincoln.com.cn/content/lincoln/cn/zh_cn/configuration/application-and-services/provinceCityDropDowns.lincolnmultiFieldDropdown.data";
			String text = HttpConnectionGet.getJson(getProCity);
			JSONArray provinces = JSON.parseArray(text);
			for(int i=0;i<provinces.size();i++) {
				String proName = provinces.getJSONObject(i).getString("provinceKey");
				
				JSONArray cities = provinces.getJSONObject(i).getJSONArray("cityList");
				for(int j=0;j<cities.size();j++) {
					String cityName = cities.getJSONObject(j).getString("cityKey");
					
					//获取城市坐标
					String getLocation = "https://restapi.amap.com/v3/geocode/geo?key=bf60b752050544f237667a726a8b6f7f&s=rsv3&platform=JS&logversion=2.0&sdkversion=1.3&appname=https%3A%2F%2Fwww.lincoln.com.cn%2Flocate-dealer%2F&csid=A77DACEF-757E-4990-8478-42ED1A6FA6F2&address="+proName+cityName;
					//得到含有json的字符串
					String locationText = HttpConnectionGet.getJson(getLocation);
					System.out.println(locationText);
					int start = locationText.indexOf("(")+1;
					int end = locationText.length()-1;
					//取到json
					locationText = locationText.substring(start, end);
					//System.out.println(locationText);
					JSONObject locationJson = JSON.parseObject(locationText);
					JSONArray locationArray = locationJson.getJSONArray("geocodes");
					if(locationArray.size()==0) {
						continue;
					}
					String location = locationArray.getJSONObject(0).getString("location");

					//System.out.println(location);
					
					String getDealer = "https://yuntuapi.amap.com/datasearch/around?callback=jQuery1111017964046498552189_1500950485696&s=rsv3&key=8a8fca79bdaa496c2f164dd8477914d1&tableid=59129d297bbf197dd16a17a7&limit=50&radius=50000&filter=AdministrativeArea:"+proName+"+Locality:"+cityName+"&center="+location;
					String dealerText = HttpConnectionGet.getJson(getDealer).trim();
					int startDealer = dealerText.indexOf("(")+1;
					int endDealer = dealerText.length()-1;
					dealerText = dealerText.substring(startDealer, endDealer);
					JSONArray dealers = JSON.parseObject(dealerText).getJSONArray("datas");
					for(int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
					
						String name = dealer.getString("_name");
						String address = dealer.getString("_address");
						String sellTell = dealer.getString("telephone");
						String[] lngAndLat = Location.getLocation(address);
						String lng = lngAndLat[0];
						String lat = lngAndLat[1];
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("林肯");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("福特汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
					
					
				}
				
			
			}
			
			
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		
		return list;
	}
	public static void main(String[] args) {
		FordQiCheCrawler crawler = new FordQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
