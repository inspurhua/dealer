package crawler.sikeda;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import util.Location;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月5日 上午10:20:02 
* 类说明 
*/
public class ShangHaiDaZhongPageProcessor implements PageProcessor {
	private static List<String> pname;
	private static List<String> pid;
	private static String dpname;
	@Override
	public void process(Page page) {
		if(page.getUrl().regex("https://mediacommons.eskoda.com.cn/skoda_mediacommons/provinces.interface").match()) {
		pname = page.getJson().jsonPath("$[*].name").all();
		pid = page.getJson().jsonPath("$[*].code").all();
		}else {
		//System.out.println(page.getJson().get());
		List<String> name = page.getJson().jsonPath("$[*].dealerName").all();
		//System.out.println(name);
		List<String> address = page.getJson().jsonPath("$[*].address").all();
		//System.out.println(dpname);
		List<String> cityName = page.getJson().jsonPath("$[*].cityName").all();
		List<String> sellTell  = new ArrayList<>();
		List<String> lng = new ArrayList<>();
		List<String> lat = new ArrayList<>();
		List<String> dnull = new ArrayList<>();
		List<String> proName = new ArrayList<>();
		for(int i=0;i<address.size();i++) {
			String tel00 = page.getJson().jsonPath("$["+i+"]").get();
			String tel10 = "";
			if(tel00.contains("tel")) {
				tel10 = page.getJson().jsonPath("$["+i+"].tel").get();
			}else {
				tel10 = "";
			}
			//System.out.println(tel10);
			sellTell.add(tel10);
			String[] lngAndLat = Location.getLocation(address.get(i));
			String lng00 = lngAndLat[0];
			String lat00 = lngAndLat[1];
			lng.add(lng00);
			lat.add(lat00);
			proName.add(dpname);
			String n00 = "";
			dnull.add(n00);
		}
		AgencyEntity2 agency = new AgencyEntity2();
		//agency.setdCloseDate(null);
		//agency.setdOpeningDate(null);
		agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
		//agency.setnBrandID(-1);
		agency.setsBrand("斯柯达");

		//agency.setnDealerIDWeb(-1);
		//agency.setnManufacturerID(-1);
		agency.setsManufacturer("上汽大众");

		//agency.setnState(1);
		agency.setsAddress(address);
		agency.setsCity(cityName);
		agency.setsCustomerServiceCall(dnull);
		agency.setsDealerName(name);
		agency.setsDealerType(dnull);
		agency.setsProvince(proName);
		agency.setsSaleCall(sellTell);
		agency.setsLongitude(lng);
		agency.setsLatitude(lat);
		new AgencyDao().add(agency);
		//agency.setsControllingShareholder(sControllingShareholder);
		//agency.setsOtherShareholders(sOtherShareholders);
		}

	}
private Site site = Site.me()
					.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
					.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
					.addHeader("Accept-Encoding", "gzip, deflate, br")
					.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
					.addHeader("Cache-Control", "max-age=0")
					.addHeader("Connection", "keep-alive")
					.addHeader("Host", "mediacommons.eskoda.com.cn")
					.addHeader("Upgrade-Insecure-Requests", "1")
					.addHeader("Referer", "https://www.skoda.com.cn/finddealer/index.html")
					.setCharset("utf-8")
					.setSleepTime(0)
					.setTimeOut(60000)
					.setCycleRetryTimes(3)
					;

	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ShangHaiDaZhongPageProcessor())
	//.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/secondleveldealers/alldealers.interface?provinceid=120000")
	.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/provinces.interface")
	.run();
	//.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/provinces.interface?jsoncallback=jQuery111107375759899499382_1541384280553&_=1541384280555")
		   //https://mediacommons.eskoda.com.cn/skoda_mediacommons/provinces.interface?jsoncallback=jQuery11110015033310647521003_1541582684761&_=1541582684763
	//.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/secondleveldealers/alldealers.interface?jsoncallback=jQuery111107375759899499382_1541384280553&provinceid=130000&_=1541384280557")
	//.addUrl("http://www.skoda.com.cn/assets/js/apps/dealerdata.js")
	for(int i=0;i<pid.size();i++) {
		dpname = pname.get(i);
		Spider.create(new ShangHaiDaZhongPageProcessor())
		.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/secondleveldealers/alldealers.interface?provinceid="+pid.get(i))
		.run();
	}
}
}
 