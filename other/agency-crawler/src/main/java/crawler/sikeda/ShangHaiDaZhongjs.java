package crawler.sikeda;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyEntity;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import util.Location;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月5日 上午10:20:02 
* 类说明 
*/
public class ShangHaiDaZhongjs implements PageProcessor {
	//private static List<String> pname;
	//private static List<String> pid;
	@Override
	public void process(Page page) {
		List<String> sou = new ArrayList<>();
		List<String> wei = new ArrayList<>();
 		//省
		sou.add("_DATA.province");
		wei.add("_DATA.city");
		//市
		sou.add("_DATA.city");
		wei.add("_DATA.district");
		//经销商
		sou.add("_DATA.dealer");
		wei.add("_DATA.xy");
		//经纬度
		sou.add("_DATA.xy");
		wei.add(".xy");
		List<String> name = new ArrayList<>();
		List<String> dpid = new ArrayList<>();
		List<String> dcid = new ArrayList<>();
		List<String> dpname = new ArrayList<>();
		List<String> dcname = new ArrayList<>();
		List<String> address = new ArrayList<>();
		List<String> jwdnameid = new ArrayList<>();
		List<String> dwname = new ArrayList<>();
		List<String> djname = new ArrayList<>();
		List<String> phone1 = new ArrayList<>();
		List<String> phone2 = new ArrayList<>();
		List<String> pid00 = new ArrayList<>();
		List<String> pname00 = new ArrayList<>();
		List<String> cid00 = new ArrayList<>();
		List<String> cname00 = new ArrayList<>();
		List<String> jwid00 = new ArrayList<>();
		List<String> jing00 = new ArrayList<>();
		List<String> wei00 = new ArrayList<>();
		for(int sw=0;sw<sou.size();sw++) {
			//for(int sw=3;sw<4;sw++) {
			//System.out.println("===="+sou.get(sw)+"=======");
/*			if(sou.get(sw).equals("province")) {
				System.out.println("当前是省信息+==");
			}else {
				System.out.println("市===");
			}*/
			//System.out.println("========头===="+"window.DEALERS"+sou.get(sw)+"([\\s\\S]*)window.DEALERS"+wei.get(sw));
			String r2 = "";
			if(wei.get(sw).equals(".xy")) {
				r2 = "([\\s\\S]*)";
				//r2 = "\"[0-9]{1,10}(-[0-9]{1,10}){0,1}\":\\s*\\[\"\\s*(http://dealer.skoda.com.cn/[a-z]{1,10}/\"){0,1},\\s*\"[0-9]{1,10}.[0-9]{1,10},[0-9]{1,10}.[0-9]{1,10}\"]";
			}else {
				r2 = "\\[\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+\\s+\\]";
			}
		String n00 = page.getHtml()
				.regex("window.DEALERS"+sou.get(sw)+"([\\s\\S]*)window.DEALERS"+wei.get(sw))
				//.regex("\\[\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+\\s+\\]")
				.regex(r2)
				//.regex("\\[([\\s\\S])*\\]")
				//.replace("=", "")
				.get();
		//System.out.println(n00);
		String[] n01 = n00.split("],");
		for(int i=0;i<n01.length;i++) {
			//System.out.println("=1=="+n01[i]);
			String[] n02 = n01[i]
					.replace("[", "")
					.replace("]", "")
					.replace("\"", "")
					.replaceAll("\\s*", "")
					.replace("{", "")
					.replace("}", "")
					.replace("=", "")
					.replaceAll("http://dealer.skoda.com.cn/[a-z]{1,10}/", "")
					.replace(":", "")
					.split(",")
					;
			for(int j=0;j<n02.length;j++) {
				//System.out.println(n02[j]);
				if(sou.get(sw).equals("_DATA.province")) {
				if(j==0) {
					pid00.add(n02[j]);
					//System.out.println(n02[j]);
				}else {}
				if(j==1) {
					pname00.add(n02[j]);
					//System.out.println(n02[j]);
				}else {}
				}else 
				if(sou.get(sw).equals("_DATA.city")) {
					if(j==0) {
						cid00.add(n02[j]);
						//System.out.println(n02[j]);
					}else {}
					if(j==1) {
						cname00.add(n02[j]);
						//System.out.println(n02[j]);
					}else {}
				}else 
				if(sou.get(sw).equals("_DATA.dealer")) {
					if(j==0) {
						jwdnameid.add(n02[j]);
						//System.out.println("经纬度id=="+n02[j]);
					}else {}
					if(j==1) {
						name.add(n02[j]);
						//System.out.println("经销商名称=="+n02[j]);
					}else {}
					if(j==3) {
						dpid.add(n02[j]);
						//System.out.println("省id=="+n02[j]);
					}else {}
					if(j==4) {
						dcid.add(n02[j]);
						//System.out.println("市id=="+n02[j]);
					}else {}
					if(j==9) {
						address.add(n02[j]);
						//System.out.println("地址=="+n02[j]);
					}else {}
					if(j==6) {
						phone1.add(n02[j]);
						//System.out.println("销售电话=="+n02[j]);
					}else {}
					if(j==10) {
						phone2.add(n02[j]);
						//System.out.println("售后电话=="+n02[j]);
					}else {}
				}else 
				if(sou.get(sw).equals("_DATA.xy")) {
					if(j==0) {
						jwid00.add(n02[j]);
						//System.out.println(n02[j]);
					}else {}
					if(j==1) {
						wei00.add(n02[j]);
						//System.out.println(n02[j]);
					}else {}
					if(j==2) {
						jing00.add(n02[j]);
						//System.out.println(n02[j]);
					}else {}
				}else {}
			}
		}
		}
		for(int i=0;i<dpid.size();i++) {
			for(int p=0;p<pid00.size();p++) {
				if(dpid.get(i).equals(pid00.get(p))) {
					dpname.add(pname00.get(p));
					//System.out.println(pid00.get(p)+"=="+pname00.get(p));
				}else {}
			}
			for(int c=0;c<cid00.size();c++) {
				if(dcid.get(i).equals(cid00.get(c))) {
					dcname.add(cname00.get(c));
					//System.out.println(cid00.get(c)+"=="+cname00.get(c));
				}else {}
			}
			for(int jw=0;jw<jwid00.size();jw++) {
				if(jwdnameid.get(i).equals(jwid00.get(jw))) {
					dwname.add(wei00.get(jw));
					djname.add(jing00.get(jw));
				}else {}
			}
		}
		
		for(int j=0;j<name.size();j++) {
		System.out.println("\n===================");
		System.out.println(dpname.get(j));
		System.out.println(dcname.get(j));
		System.out.println(name.get(j));
		System.out.println(address.get(j));
		System.out.println(phone1.get(j));
		System.out.println(phone2.get(j));
		System.out.println(dwname.get(j));
		System.out.println(djname.get(j));
		}
		
		
		//经销商
/*		String n10 = page.getHtml()
				.regex("window.DEALERS_DATA.dealer([\\s\\S]*)window.DEALERS_DATA.xy")
				.regex("\\[\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+\\s+\\]")
				//.regex("\\[([\\s\\S])*\\]")
				//.replace("=", "")
				.get();*/
		//System.out.println("=="+n10);
/*		List<String> n20 = page.getHtml().regex("window.DEALERS_DATA.dealer([\\s\\S]*)window.DEALERS_DATA.xy")
							.regex("\\[\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+\\s+\\]").all();
		System.out.println(n20.get(0));*/
/*		String r = "\\s\\[\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+,\\s.+\\s+\\]\\s";
		Pattern p = Pattern.compile(r);
		Matcher m = p.matcher(n10);
		while(m.find()) {
			System.out.println("=========-----======");
			System.out.println(m.group());
			System.out.println("=======555555555====");
		}*/

		/*String[] n20 = n10.split("],");
		for(int i=0;i<n20.length;i++) {
			//System.out.println("=========-----======");
			//System.out.println(n20[i]);
			String[] n21 = n20[i]
					.replace("[", "")
					.replace("]", "")
					.replace("\"", "")
					.replaceAll("\\s*", "")
					.split(",");
			for(int j=0;j<n21.length;j++) {
				//第一个
				//System.out.println("=========-----======");
				if(j==1) {
					//System.out.println("="+n21[j]);
					
				}else {}
			}
		}*/
		/*List<String> name = JsonPath.read(n10, "$[*].data[*].");
		System.out.println(name);*/
		//System.out.println(page.getHtml().get());
		/*String regex = "\\[\"\\d*?\", \"\\W*?\", \"\\d*?\", \"\\d*?\", \"\\d*?\", .*?\\]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(page.getHtml().get());
		while(m.find()) {
			System.out.println(m.group());
		}*/
		//System.out.println(page.getJson().regex("\\(([\\s\\S]+)\\)").get());
/*		if(page.getUrl().regex("https://mediacommons.eskoda.com.cn/skoda_mediacommons/provinces.interface?jsoncallback=jQuery111107375759899499382_1541384280553&_=1541384280555").match()) {
		String n = page.getJson().regex("\\(([\\s\\S]+)\\)").get();
		//System.out.println(n);
		List<String> pname = JsonPath.read(n, "$[*].name");
		System.out.println(pname);
		List<String> pid = JsonPath.read(n, "$[*].code");
		System.out.println(pid);
		}else {
		String n = page.getJson().regex("\\(([\\s\\S]+)\\)").get();
		//System.out.println(n);
		List<String> name = JsonPath.read(n, "$[*].dealerName");
		System.out.println(name);
		List<String> address = JsonPath.read(n, "$[*].address");
		System.out.println(address);
		List<String> cityName = JsonPath.read(n, "$[*].cityName");
		System.out.println(cityName);
		List<String> sellTell  = JsonPath.read(n, "$[*].tel");
		System.out.println(sellTell);
		List<String> lng = new ArrayList<>();
		List<String> lat = new ArrayList<>();
		List<String> dnull = new ArrayList<>();
		for(String add : address) {
			String[] lngAndLat = Location.getLocation(add);
			String lng00 = lngAndLat[0];
			String lat00 = lngAndLat[1];
			lng.add(lng00);
			lat.add(lat00);
			String n00 = "";
			dnull.add(n00);
		}*/
/*		AgencyEntity2 agency = new AgencyEntity2();
		//agency.setdCloseDate(null);
		//agency.setdOpeningDate(null);
		agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
		//agency.setnBrandID(-1);
		agency.setsBrand("斯柯达");

		//agency.setnDealerIDWeb(-1);
		//agency.setnManufacturerID(-1);
		agency.setsManufacturer("上汽大众");

		//agency.setnState(1);
		agency.setsAddress(address);
		agency.setsCity(cityName);
		agency.setsCustomerServiceCall(dnull);
		agency.setsDealerName(name);
		agency.setsDealerType(dnull);
		agency.setsProvince(proName);
		agency.setsSaleCall(sellTell);
		agency.setsLongitude(lng);
		agency.setsLatitude(lat);*/
		//agency.setsControllingShareholder(sControllingShareholder);
		//agency.setsOtherShareholders(sOtherShareholders);
		//List<String> name = JsonPath.read(n, "$[*].dealerName");
		//System.out.println(name);
		
		//}
/*		String[] lngAndLat = Location.getLocation("北京市海淀区安宁庄东路15号");
		String lng00 = lngAndLat[0];
		String lat00 = lngAndLat[1];
		System.out.println(lng00);
		System.out.println(lat00);*/
	}
private Site site = Site.me()
					.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
					.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
					.addHeader("Accept-Encoding", "gzip, deflate, br")
					.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
					.addHeader("Cache-Control", "max-age=0")
					.addHeader("Connection", "keep-alive")
					//.addHeader("Host", "mediacommons.eskoda.com.cn")
					.addHeader("Host", "www.skoda.com.cn")
					.addHeader("Upgrade-Insecure-Requests", "1")
					.addHeader("Referer", "https://www.skoda.com.cn/finddealer/index.html")
					.setCharset("utf-8")
					.setSleepTime(0)
					.setCycleRetryTimes(3)
					;

	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ShangHaiDaZhongjs())
	//.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/provinces.interface?jsoncallback=jQuery111107375759899499382_1541384280553&_=1541384280555")
	//.addUrl("https://mediacommons.eskoda.com.cn/skoda_mediacommons/secondleveldealers/alldealers.interface?jsoncallback=jQuery111107375759899499382_1541384280553&provinceid=130000&_=1541384280557")
	.addUrl("http://www.skoda.com.cn/assets/js/apps/dealerdata.js")
	.run();
	/*for(int i=0;i<pid.size();i++) {
		
	}*/
}
}
 