package crawler.sikeda;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class ShangHaiDaZhongCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getAll = "http://www.skoda.com.cn/assets/js/apps/dealerdata.js";
		String all = HttpConnectionGet.getJson(getAll);
		Map<String, String> proMap = getPro(all);
		Map<String, String> cityMap = getCity(all);
		//获取经销商正则
		String regex = "\\[\"\\d*?\", \"\\W*?\", \"\\d*?\", \"\\d*?\", \"\\d*?\", .*?\\]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(all);
		while(m.find()) {
			System.out.println("1");
			JSONArray dealerArray = JSON.parseArray(m.group());
			String name = dealerArray.getString(1);
			String proNum = dealerArray.getString(3);
			String proName = proMap.get(proNum);
			String cityNum = dealerArray.getString(4);
			String cityName = cityMap.get(cityNum);
			String sellTell = dealerArray.getString(6);
			String serviceTell = dealerArray.getString(10);
			String address = dealerArray.getString(9);
			String[] lngAndLat = Location.getLocation(address);
			String lng = lngAndLat[0];
			String lat = lngAndLat[1];
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("斯柯达");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("上海大众");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(serviceTell);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);
			System.out.println(list);
			
		}
		
		
		
		
		return list;
	}
	//利用正则.从all中取出省份的id和name
	public Map<String, String> getPro(String all) {
		Map<String, String> proMap = new HashMap<String, String>();
		//正则
		String regex = "\\[\"\\d*?\", \"\\W*?\", \"\\d*?\", \"\\W*?\", \"\\W*?\", \"\\d*?\"\\]";
		//System.out.println(regex);
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(all);
		while(m.find()) {
			JSONArray proArray = JSON.parseArray(m.group());
			String proNum = proArray.getString(0);
			String proName = proArray.getString(1);
			proMap.put(proNum, proName);
		}
		
		return proMap;
	}
	
	//利用正则,获取城市的id和name
	public Map<String, String> getCity(String all) {
		Map<String, String> cityMap = new HashMap<String, String>();
		//正则
		String regex = "\\[\"\\d*?\", \"\\W*?\", \"\\d*?\"\\]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(all);
		while(m.find()) {
			JSONArray cityArray = JSON.parseArray(m.group());
			String cityNum = cityArray.getString(0);
			String cityName = cityArray.getString(1);
			cityMap.put(cityNum, cityName);
		}
		
		return cityMap;
	}
	
	public static void main(String[] args) {
		ShangHaiDaZhongCrawler crawler = new ShangHaiDaZhongCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		//CreateTable.addTable(tableName);
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
