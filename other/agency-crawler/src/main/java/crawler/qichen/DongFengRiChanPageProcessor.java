package crawler.qichen;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class DongFengRiChanPageProcessor implements PageProcessor {
	private static List<String> pid = new ArrayList<>();
	private static List<String> pname = new ArrayList<>();
	private static List<String> cid = new ArrayList<>();
	private static List<String> ccid = new ArrayList<>();
	private static List<String> cpid = new ArrayList<>();
	private static List<String> cname = new ArrayList<>();
	
	private static List<String> name  = new ArrayList<>();
	private static List<String> address  = new ArrayList<>();
	private static List<String> sellTell  = new ArrayList<>();
	private static List<String> lng  = new ArrayList<>();
	private static List<String> lat  = new ArrayList<>();
private static	List<AgencyEntity> list = new ArrayList<AgencyEntity>();
	@Override
	public void process(Page page) {
		if(page.getUrl().regex("ProvinceFilter").match()) {
			List<String> pid00 = page.getJson().jsonPath("$.[*].ItemID").all();
			pid.addAll(pid00);
			//System.out.println(pid.size()+"=="+pid);
			List<String> pname00 = page.getJson().jsonPath("$.[*].Name").all();
			pname.addAll(pname00);
			//System.out.println(pname.size()+"=="+pname);
			List<String> url = new ArrayList<>();
			for(int i=0;i<pid00.size();i++) {
				url.add("http://www.venucia.com/Ajax/AjaxSupport.ashx?method=CityFilter&ProvinceID="+pid00.get(i)+"&Brand=2");
			}
			page.addTargetRequests(url);
		}else 
			if(page.getUrl().regex("ProvinceID").match()) {
				List<String> cid00 = page.getJson().jsonPath("$.[*].ItemID").all();
				//System.out.println(cid00.size()+"=="+cid00);
				cid.addAll(cid00);
				List<String> cname00 = page.getJson().jsonPath("$.[*].Name").all();
				cname.addAll(cname00);
				//System.out.println(cname00.size()+"=="+cname00);
				String nn = page.getUrl().get()
						.replace("http://www.venucia.com/Ajax/AjaxSupport.ashx?method=CityFilter&ProvinceID=", "")
						.replace("&Brand=2", "");
				//System.out.println(nn);
				List<String> url = new ArrayList<>();
				for(int i=0;i<cid00.size();i++) {
					url.add("http://www.venucia.com/Ajax/AjaxSupport.ashx?method=LoadDealerData&brand=2&selpro="+nn+"&selcity="+cid00.get(i));
					}
				//System.out.println("=url=="+url);
				page.addTargetRequests(url);
		}else {
			//System.out.println(page.getJson().get());
			List<String> name00  = page.getJson().jsonPath("$.[*].Name").all();
			//System.out.println(name00);
			List<String> address00  = page.getJson().jsonPath("$.[*].Address").all();
			List<String> sellTell00  = page.getJson().jsonPath("$.[*].SalesPhone").all();
			List<String> lng00  = page.getJson().jsonPath("$.[*].Longitude").all();
			List<String> lat00  = page.getJson().jsonPath("$.[*].Latitude").all();
			for(int i=0;i<name00.size();i++) {
				String ccid00 = page.getUrl().regex("selcity.*").replace("selcity=", "").get();
				ccid.add(ccid00);
				String cpid00 = page.getUrl().regex("selpro=.*&").replace("selpro=", "").replace("&", "").get();
				cpid.add(cpid00);
			}
			name.addAll(name00);
			address.addAll(address00);
			sellTell.addAll(sellTell00);
			lng.addAll(lng00);
			lat.addAll(lat00);
		}
	}
	private Site site = Site.me()
			.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0")
			.setCycleRetryTimes(3)
			.setSleepTime(0)
			.setTimeOut(60000);
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new DongFengRiChanPageProcessor())
	.addUrl("http://www.venucia.com/Ajax/AjaxSupport.ashx?method=ProvinceFilter")
	//.addUrl("http://www.venucia.com/Ajax/AjaxSupport.ashx?method=CityFilter&ProvinceID=0ac5e21c-3519-404c-9642-61278de7329a&Brand=2")
	//.addUrl("https://www.venucia.com/Ajax/AjaxSupport.ashx?method=LoadDealerData&brand=2&selpro=0ac5e21c-3519-404c-9642-61278de7329a&selcity=c012dd15-546f-4ac4-b099-38e854df6e45")
	.thread(10)
	.run();
	//System.out.println(name.size()+"=="+name);
	//System.out.println(address.size()+"=="+address);
	//System.out.println(sellTell.size()+"=="+sellTell);
	//System.out.println(lng.size()+"=="+lng);
	//System.out.println(lat.size()+"=="+lat);
	//System.out.println(cpid.size()+"==="+cpid);
	//System.out.println(ccid.size()+"==="+ccid);
/*	for(int i=0;i<pid.size();i++) {
		String getCity = "http://www.venucia.com/Ajax/AjaxSupport.ashx?method=CityFilter&ProvinceID="+pid.get(i)+"&Brand=2";
		Spider.create(new DongFengRiChanPageProcessor())
		.addUrl(getCity)
		//.thread(10)
		.run();
		
	}*/
	List<String> cityName = new ArrayList<>();
	List<String> proName = new ArrayList<>();
	List<String> dnull = new ArrayList<>();
	for(int i=0;i<cpid.size();i++) {
		for(int p=0;p<pid.size();p++) {
			if(cpid.get(i).equals(pid.get(p))) {
				proName.add(pname.get(p));
			}else {}
		}
		for(int c=0;c<cid.size();c++) {
			if(ccid.get(i).equals(cid.get(c))) {
				cityName.add(cname.get(c));
			}else {}
		}
		dnull.add("");
	}
	//System.out.println(proName.size()+"==="+proName);
	//System.out.println(cityName.size()+"==="+cityName);
	AgencyEntity2 agency = new AgencyEntity2();
	//agency.setdCloseDate(null);
	//agency.setdOpeningDate(null);
	agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
	//agency.setnBrandID(-1);
	agency.setsBrand("启辰");

	//agency.setnDealerIDWeb(-1);
	agency.setnManufacturerID(-1);
	agency.setsManufacturer("东风日产");

	//agency.setnState(1);
	agency.setsAddress(address);
	agency.setsCity(cityName);
	agency.setsCustomerServiceCall(dnull);
	agency.setsDealerName(name);
	agency.setsDealerType(dnull);
	agency.setsProvince(proName);
	agency.setsSaleCall(sellTell);
	agency.setsLongitude(lng);
	agency.setsLatitude(lat);
	new AgencyDao().add(agency);

	//agency.setsControllingShareholder(sControllingShareholder);
	//agency.setsOtherShareholders(sOtherShareholders);0
}
}
