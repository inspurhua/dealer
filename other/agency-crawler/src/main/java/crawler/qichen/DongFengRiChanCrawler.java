package crawler.qichen;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class DongFengRiChanCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.venucia.com/Ajax/AjaxSupport.ashx?method=ProvinceFilter";
		String proText = HttpConnectionGet.getJson(getPro);
		JSONArray provinces = JSON.parseArray(proText);
		System.out.println(provinces);
		for(int i=0;i<provinces.size();i++) {
			JSONObject province = provinces.getJSONObject(i);
			String proNum = province.getString("ItemID");
			String proName = province.getString("Name");
			//System.out.println(proNum+":"+proName);
			//System.out.println(provinces.size());
			
			String getCity = "https://www.venucia.com/Ajax/AjaxSupport.ashx?method=CityFilter&ProvinceID="+proNum+"&Brand=2";
			String cityText = HttpConnectionGet.getJson(getCity);
			JSONArray cities = JSON.parseArray(cityText);
			for(int j=0;j<cities.size();j++) {
				JSONObject city = cities.getJSONObject(j);
				String cityNum = city.getString("ItemID");
				String cityName = city.getString("Name");
				//System.out.println(cityNum+":"+cityName);
			
				String getDealer = "https://www.venucia.com/Ajax/AjaxSupport.ashx?method=LoadDealerData&brand=2&selpro="+proNum+"&selcity="+cityNum;
				String dealerText = HttpConnectionGet.getJson(getDealer);
				Map<String, Object> map = JSON.parseObject(dealerText);
				Collection<Object> dealers = map.values();
				for (Object dealerInfo : dealers) {
					JSONObject dealer = (JSONObject)dealerInfo;
					String name = dealer.getString("Name");
					String address = dealer.getString("Address");
					String sellTell = dealer.getString("SalesPhone");
					String lng = dealer.getString("Longitude");
					String lat = dealer.getString("Latitude");
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("启辰");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("东风日产");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
				}
			}
		}
		return list;
	}
	public static void main(String[] args) {
		DongFengRiChanCrawler crawler = new DongFengRiChanCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
/*		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();*/
		System.out.println("请查看数据库");
	}

}
