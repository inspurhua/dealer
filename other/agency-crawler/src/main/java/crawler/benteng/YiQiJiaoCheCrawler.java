package crawler.benteng;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class YiQiJiaoCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			// 根据url,获取返回的省份id和name的字符串
			String getPro = "http://api.faw-benteng.com/ajax/v1_getprov.php";
			String proText = HttpConnectionGet.getJson(getPro);
			// 使用json解析
			JSONArray proArray = JSON.parseArray(proText);
			for (int i = 0; i < proArray.size(); i++) {
				String proNum = proArray.getJSONObject(i).getString("id");
				String proName = proArray.getJSONObject(i).getString("name");
				// 根据省份id,获取城市的id和name字符串
				String getCity = "http://api.faw-benteng.com/ajax/v1_getcity.php?prov=" + proNum;
				String cityText = HttpConnectionGet.getJson(getCity);
				JSONArray cityArray = JSON.parseArray(cityText);
				for (int j = 0; j < cityArray.size(); j++) {
					String cityNum = cityArray.getJSONObject(j).getString("id");
					String cityName = cityArray.getJSONObject(j).getString("name");

					// 根据省份id和城市id,获取经销商
					String getDealer = "http://api.faw-benteng.com/ajax/v1_getdealer.php?city=" + cityNum;
					String dealerText = HttpConnectionGet.getJson(getDealer);

					Map<String, Object> dealerMap = JSON.parseObject(dealerText);
					Collection<Object> dealers = dealerMap.values();
					for (Object dealerObj : dealers) {
						Map<String,String> dealer = (Map<String,String>)dealerObj;
						String type = dealer.get("attr");
						String name = dealer.get("biz_name");
						String sellTell = dealer.get("sale_phone");
						String serviceTell = dealer.get("serv_phone");
						String address = dealer.get("address");
						String lng = dealer.get("lon");
						String lat = dealer.get("lat");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("奔腾");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("一汽轿车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(serviceTell);
						agency.setsDealerName(name);
						agency.setsDealerType(type);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);

					}
					
				}
			}

		} catch (Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}

		return list;
	}

	public static void main(String[] args) {
		YiQiJiaoCheCrawler crawler = new YiQiJiaoCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}

}
