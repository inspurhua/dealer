package crawler.changan;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年10月10日 下午2:47:02 
* 类说明 
*/
/**
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月12日 上午11:03:43 
* 类说明 
*/ 
public class ChanganQichePageProcessor implements PageProcessor {
	private static String pname0 = "";
	private static String cname0 = "";
	private static List<String> pname = new ArrayList<>();
	private static List<String> cname = new ArrayList<>();
	private static List<String> cid = new ArrayList<>();
	private static List<String> address = new ArrayList<>();
	private static List<String> cityName = new ArrayList<>();
	/**private static List<String> serviceTell = new ArrayList<>();*/
	private static List<String> name = new ArrayList<>();
	private static List<String> dnull = new ArrayList<>();
	private static List<String> proName = new ArrayList<>();
	private static List<String> sellTell = new ArrayList<>();
	private static List<String> lng = new ArrayList<>();
	private static List<String> lat = new ArrayList<>();
	private static String url = "http://www.changan.com.cn/cache/dealer/dealer_[0-9]{6}_json.js";
	@Override
	public void process(Page page) {
		if(page.getUrl().regex(url).match()) {
		String c ="["+ page.getHtml().regex("\\[([\\s\\S]*)\\]").get()+"]";
		List<String> name00 = JsonPath.read(c, "$[*].dealer_name");
		name.addAll(name00);
		List<String> add00 = JsonPath.read(c, "$[*].address");
		address.addAll(add00);
		List<String> sell00 = JsonPath.read(c, "$[*].contact_phone");
		sellTell.addAll(sell00);
		List<String> jwd = JsonPath.read(c, "$[*].map_position");
		for(int i=0;i<jwd.size();i++) {
			if(jwd.get(i)!=null) {
			String[] jwd0 = jwd.get(i).split(",");
			lng.add(jwd0[0]);
			lat.add(jwd0[1].replaceAll("\\s*", ""));
			}else {
				lng.add("");
				lat.add("");
			}
		}
		for(int i=0;i<name00.size();i++) {
			dnull.add("");
			proName.add(pname0);
			cityName.add(cname0);
		}
		}else {
		String n = page.getHtml().regex("\\[([\\s\\S]*)\\]").get();
		List<String> pname1 = JsonPath.read(n, "$.city_list[*].region_name");
		for(int i=0;i<pname1.size();i++) {
			List<String> cname1 = JsonPath.read(n, "$.city_list["+i+"].children[*].region_name");
			for(int j=0;j<cname1.size();j++) {
				pname.add(pname1.get(i));
			}
			cname.addAll(cname1);
			List<String> cid1 = JsonPath.read(n, "$.city_list["+i+"].children[*].region_code");
			cid.addAll(cid1);
		}
		}
		}
	private static Site site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.changan.com.cn")
			.addHeader("Accept", "*/*")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.changan.com.cn/dealer.shtml?navActive=0")
			.addHeader("Connection", "keep-alive")
			.addHeader("Cache-Control", "max-age=0")
			.setCharset("utf8")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(3)
			;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ChanganQichePageProcessor())
	.addUrl("http://www.changan.com.cn/cache/car_model_json.js")
	//.addUrl("http://www.changan.com.cn/cache/dealer/dealer_654000_json.js")
	//.addUrl("http://www.changan.com.cn/cache/dealer/dealer_110100_json.js?_=1539153742360")
	.run();
	for(int i=0;i<cname.size();i++) {
		pname0 = pname.get(i);
		cname0 = cname.get(i);
		Spider.create(new ChanganQichePageProcessor())
		.addUrl("http://www.changan.com.cn/cache/dealer/dealer_"+cid.get(i)+"_json.js")
		.run();
	}
	AgencyEntity2 agencyEntity = new AgencyEntity2();
	//agencyEntity.setdCloseDate(null);
	//agencyEntity.setdOpeningDate(null);
	agencyEntity.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
	//agencyEntity.setnBrandID(-1);
	agencyEntity.setsBrand("长安");
	//agencyEntity.setnDealerIDWeb(-1);
	//agencyEntity.setnManufacturerID(-1);
	agencyEntity.setsManufacturer("长安汽车");
	//agencyEntity.setnState(1);
	agencyEntity.setsAddress(address);
	agencyEntity.setsCity(cityName);
	agencyEntity.setsCustomerServiceCall(dnull);
	agencyEntity.setsDealerName(name);
	agencyEntity.setsDealerType(dnull);
	agencyEntity.setsProvince(proName);
	agencyEntity.setsSaleCall(sellTell);
	agencyEntity.setsLongitude(lng);
	agencyEntity.setsLatitude(lat);
	//agencyEntity.setsControllingShareholder(null);
	//agencyEntity.setsOtherShareholders(null);
	new AgencyDao().add(agencyEntity);
}
}
 