package crawler.changan;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;
//有问题
public class ChangAnQiCheCrawler {
	public List<AgencyEntity> getAgencyDealer(String series) {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.changan.com.cn/api.php?op=dealer&act=get_province_list&brand=1&carseries=CD101";
		//String getPro = "http://www.changan.com.cn/cache/car_model_json.js";
		String proStr = HttpConnectionGet.getJson(getPro);
		//System.out.println("aa==="+proStr);
		JSONArray provinces = JSON.parseArray(proStr);
		for (int i=0;i<provinces.size();i++) {
			JSONObject pro = provinces.getJSONObject(i);
			String proNum = pro.getString("region_id");
			System.out.println(proNum);
			String proName = pro.getString("region_name");
			String getCity = "http://www.changan.com.cn/api.php?op=dealer&act=get_city_list&province="+proNum+"&brand=1&carseries=CD101";
			String cityStr = HttpConnectionGet.getJson(getCity);
			JSONArray citys = JSON.parseArray(cityStr);
			for (int j=0;j<citys.size();j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityNum = city.getString("region_id");
				String cityName = city.getString("region_name");
				
				Map<String, String> data = new HashMap<String, String>();
				data.put("car_series", "CD101");
				data.put("checktype", "agency");
				data.put("cityname", cityName);
				data.put("dealer_brand", "1");
				data.put("region_id_1", proNum);
				data.put("region_id_1_val", proNum);
				data.put("region_id_2", cityNum);
				data.put("region_id_2_val", cityNum);

				String getDealer = "http://www.changan.com.cn/api.php?op=querysale";
				String param = "car_series="+series+"&checktype=agency&cityname="+cityName+"&dealer_brand=1&dealer_brand_val=1&region_id_1="+proNum+"&region_id_1_val="+proNum+"&region_id_2="+cityNum+"&region_id_2_val="+cityNum;
				String dealerStr = HttpConnectionPost.getJson(getDealer, param);
				JSONObject dealerObject = JSON.parseObject(dealerStr);
				int  count = 0;
				try {					
					count = dealerObject.getIntValue("number");
				} catch (Exception e) {
					System.out.println(proName);
					System.out.println(cityName);
					e.printStackTrace();
				}
				for (int k=0;k<count;k++) {
					JSONObject dealer = dealerObject.getJSONObject(String.valueOf(k));
					String lng = null;
					String lat = null;
					String address = dealer.getString("address");
					if (dealer.getString("map_position")!=null) {
						String[] lngAndLat = dealer.getString("map_position").split(",");
						if (lngAndLat.length==2) {
							lng = lngAndLat[0];
							lat = lngAndLat[1];
						}
					}
					String sellTell = dealer.getString("contact_phone");
					String name = dealer.getString("dealer_name");
					
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("长安");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("长安汽车");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
					System.out.println(agency);
				}
			}
		}
		return list;
	}

	
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list1 = getAgencyDealer("CD101");
		List<AgencyEntity> list2 = getAgencyDealer("V301");
		for(AgencyEntity agency: list2) {
			if(!list1.contains(agency)) {
				list1.add(agency);
			}
		}
		return list1;
	}
	
	public static void main(String[] args) {
		ChangAnQiCheCrawler crawler = new ChangAnQiCheCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

	}

}
