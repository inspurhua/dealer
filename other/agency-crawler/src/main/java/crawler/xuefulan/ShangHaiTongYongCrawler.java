package crawler.xuefulan;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class ShangHaiTongYongCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.chevrolet.com.cn/data/dealer.json";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		//得到所有的省
		JSONArray provinces = JSON.parseArray(dealerText);
		System.out.println(provinces);
		for(int i=0;i<provinces.size();i++) {
			String proName = provinces.getJSONObject(i).getString("name");
			//得到省中的城市
			JSONArray cities = provinces.getJSONObject(i).getJSONArray("data");
			for(int j=0;j<cities.size();j++) {
				String cityName = cities.getJSONObject(j).getString("name");
				//得到城市中的经销商
				JSONArray dealers = cities.getJSONObject(j).getJSONArray("data");
				for(int k=0;k<dealers.size();k++) {
					JSONObject dealer = dealers.getJSONObject(k);
					String name = dealer.getString("name");
					String address = dealer.getString("addr");
					String sellTell = dealer.getString("phone");
					String[] lngAndLat = dealer.getString("retlng").split(",");
					String lng = lngAndLat[1];
					String lat = lngAndLat[0];
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("雪弗兰");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("上海通用");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType("特约经销商");
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
				}
			}
		}
		String getService = "http://www.chevrolet.com.cn/data/service-shop.json";
		String serviceText = HttpConnectionGet.getJson(getService);
		JSONArray proService = JSON.parseArray(serviceText);
		for(int x=0;x<proService.size();x++) {
			String proName = proService.getJSONObject(x).getString("name");
			JSONArray cityService = proService.getJSONObject(x).getJSONArray("data");
			for(int y=0;y<cityService.size();y++) {
				String cityName = cityService.getJSONObject(y).getString("name");
				JSONArray services = cityService.getJSONObject(y).getJSONArray("data");
				for(int z=0;z<services.size();z++) {
					JSONObject service = services.getJSONObject(z);
					String address = service.getString("addr");
					String name = service.getString("name");
					String serviceTell = service.getString("phone");
					String[] lngAndLat = service.getString("retlng").split(",");
					String lng = lngAndLat[1];
					String lat = lngAndLat[0];
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("雪弗兰");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("上海通用");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType("特约服务站");
					agency.setsProvince(proName);
					agency.setsSaleCall(serviceTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
					
				}
			}
		}
		return list;
	}
	public static void main(String[] args) {
		ShangHaiTongYongCrawler crawler = new ShangHaiTongYongCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
