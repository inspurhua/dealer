package crawler.xuefulan;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月8日 上午11:46:29 
* 类说明 
*/
public class ShangHaiTongYongPageProcessor implements PageProcessor {

	@Override
	public void process(Page page) {
		String n ="["+ page.getHtml().regex("\\[([\\s\\S]*)\\]").get()+"]";
		//System.out.println(n);
		List<String> dpname = JsonPath.read(n, "$[*].name");
		//System.out.println(n1);
		List<String> name = new ArrayList<>();
		List<String> address = new ArrayList<>();
		List<String> cityName = new ArrayList<>();
		List<String> proName = new ArrayList<>();
		List<String> sellTell = new ArrayList<>();
		List<String> type = new ArrayList<>();
		List<String> lng = new ArrayList<>();
		List<String> lat = new ArrayList<>();
		List<String> dnull = new ArrayList<>();
		for(int p=0;p<dpname.size();p++) {
			List<String> dcname = JsonPath.read(n, "$["+p+"].data[*].name");
			for(int c=0;c<dcname.size();c++) {
				name.addAll(JsonPath.read(n, "$["+p+"].data["+c+"].data[*].name"));
				address.addAll(JsonPath.read(n, "$["+p+"].data["+c+"].data[*].addr"));
				sellTell.addAll(JsonPath.read(n, "$["+p+"].data["+c+"].data[*].phone"));
				List<String> retlng=JsonPath.read(n, "$["+p+"].data["+c+"].data[*].retlng");
				for(int i=0;i<retlng.size();i++) {
					proName.add(dpname.get(p));
					cityName.add(dcname.get(c));
					String[] jw00 = retlng.get(i).replace(" ", "").split(",");
					lng.add(jw00[0]);
					lat.add(jw00[1]);
					if(page.getUrl().get().contains("dealer")) {
						type.add("特约经销商");
					}else {
						type.add("特约服务站");
					}
					dnull.add("");
				}
			}
		}
		AgencyEntity2 agency = new AgencyEntity2();
		//agency.setdCloseDate(null);
		//agency.setdOpeningDate(null);
		agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
		//agency.setnBrandID(-1);
		agency.setsBrand("雪弗兰");

		//agency.setnDealerIDWeb(-1);
		//agency.setnManufacturerID(-1);
		agency.setsManufacturer("上海通用");

		//agency.setnState(1);
		agency.setsAddress(address);
		agency.setsCity(cityName);
		agency.setsCustomerServiceCall(dnull);
		agency.setsDealerName(name);
		agency.setsDealerType(type);
		agency.setsProvince(proName);
		agency.setsSaleCall(sellTell);
		agency.setsLongitude(lng);
		agency.setsLatitude(lat);
		new AgencyDao().add(agency);
		//agency.setsControllingShareholder(sControllingShareholder);
		//agency.setsOtherShareholders(sOtherShareholders);
	}
private static Site site = Site.me()
							.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
							.addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
							.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
							.addHeader("Accept-Encoding", "gzip, deflate, br")
							.addHeader("Host", "www.chevrolet.com.cn")
							.addHeader("Referer", "https://www.chevrolet.com.cn/dealer/")
							//.addHeader("X-Requested-With", "XMLHttpRequest")
							//.addHeader("XMLHttpRequest", "p35OnrDoP8k;r=648659388")
							.addHeader("Connection", "keep-alive")
							.setSleepTime(0)
							.setTimeOut(60000)
							.setCycleRetryTimes(3)
							//.setCharset("gbk")
							;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ShangHaiTongYongPageProcessor())
	.addUrl("https://www.chevrolet.com.cn/data/dealer.json"
			,"http://www.chevrolet.com.cn/data/service-shop.json")
	//.addUrl("http://www.chevrolet.com.cn/data/service-shop.json")
	.run();
}
}
 