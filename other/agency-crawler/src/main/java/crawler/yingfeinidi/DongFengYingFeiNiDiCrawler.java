package crawler.yingfeinidi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.AddressComponent;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class DongFengYingFeiNiDiCrawler {	
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			String getDealerId = "http://www.infiniti.com.cn/dealer-finder.html";
			String dealerText = HttpConnectionGet.getJson(getDealerId);
			int start = dealerText.indexOf("[{");
			int end = dealerText.indexOf("}]}")+2;
			dealerText = dealerText.substring(start, end);
			JSONArray dealerArray = JSON.parseArray(dealerText);
			for(int i=0;i<dealerArray.size();i++) {
				String dealerId = dealerArray.getJSONObject(i).getString("id");
				
				//获取经销商详细信息
				String getDealer = "http://www.infiniti.com.cn/content/infiniti_prod/zh_CN/index/dealer-finder/jcr:content/par3/columns/find_a_dealer_china_.extendeddealer.json/dealerId/"+dealerId+"/data.json";
				String dealer = HttpConnectionGet.getJson(getDealer);
				JSONObject dealerObject = JSON.parseObject(dealer);
				String name = dealerObject.getString("tradingName");
				String address = dealerObject.getJSONObject("address").getString("addressLine1");
				String sellTell = dealerObject.getJSONObject("contact").getString("phone");
				String city = dealerObject.getJSONObject("address").getString("city");
				String type = dealerObject.getJSONArray("dealerServices").getJSONObject(0).getString("name");
				JSONObject lngAndLat = dealerObject.getJSONObject("geolocation");
				String lng = lngAndLat.getString("longitude");
				String lat = lngAndLat.getString("latitude");
				String[] proCity = ProCity.getData(lat, lng);
				String proName = proCity[0];
				
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("英菲尼迪");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("东风英菲尼迪");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(city);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(type);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				
				list.add(agency);
				
				
				
			}
			
			
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		DongFengYingFeiNiDiCrawler crawler = new DongFengYingFeiNiDiCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
