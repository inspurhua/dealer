package crawler.siming;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class DongFengBenTian {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getCity = "http://www.dongfeng-honda-ciimo.com/scripts/api_list.php?op=city";
		String cityStr = HttpConnectionGet.getJson(getCity);
		JSONArray provinces = JSON.parseArray(cityStr);
		for (int i = 0; i < provinces.size(); i++) {
			JSONObject province = provinces.getJSONObject(i);
			String proName = province.getString("pname");
			JSONArray citys = province.getJSONArray("item");
			for (int j = 0; j < citys.size(); j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityNum = city.getString("cid");
				String cityName = city.getString("name");

				String getDealer = "http://www.civic.com.cn/api_list.php?op=dealer&city_id=" + cityNum;
				String dealerStr = HttpConnectionGet.getJson(getDealer);
				JSONArray dealers = JSON.parseArray(dealerStr);
				//System.out.println(dealers);
				for (int k = 0; k < dealers.size(); k++) {
					JSONObject dealer = dealers.getJSONObject(k);
					String name = dealer.getString("name");
					String address = dealer.getString("address");
					String sellTell = dealer.getString("tel");
					String serviceTell = dealer.getString("fwtel");
					String lat = dealer.getString("lon");
					String lng = dealer.getString("lat");

					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);

					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("思铭");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("东风本田");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(serviceTell);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					//System.out.println(agency);
					list.add(agency);

				}

			}

		}

		return list;
	}
	public static void main(String[] args) {
		DongFengBenTian crawler = new DongFengBenTian();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");

		
		
	}
}
