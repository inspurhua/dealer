package crawler.siming;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.utils.HttpConstant;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月9日 上午10:16:44 
* 类说明 
*/
/**
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月12日 上午11:05:20 
* 类说明 
*/ 
public class DongFengBenTianPageProcessor implements PageProcessor {
	private static Site site;
	private static List<String> pid;
	private static List<String> pname;
	//private static String cpname00 ;
	//private static List<String> cpname10 = new ArrayList<>();
	private static List<String> cid;
	private static List<String> cname;
	private static String dcname00;
	private static String dpname00;
	private static List<String> address = new ArrayList<>();
	private static List<String> cityName = new ArrayList<>();
	private static List<String> serviceTell = new ArrayList<>();
	private static List<String> name = new ArrayList<>();
	private static List<String> dnull = new ArrayList<>();
	private static List<String> proName = new ArrayList<>();
	private static List<String> sellTell = new ArrayList<>();
	private static List<String> lng = new ArrayList<>();
	private static List<String> lat = new ArrayList<>();
	@Override
	public void process(Page page) {
		//System.out.println(page.getJson().get());
		if(page.getUrl().regex("city").match()) {
			pid = page.getJson().jsonPath("$[*].pid").all();
			System.out.println(pid);
			pname = page.getJson().jsonPath("$[*].pname").all();
			System.out.println(pname);
		}else if(page.getUrl().regex("ajax").match()){
			cname = page.getJson().jsonPath("$[*].v_name").all();
			System.out.println(cname);
			cid = page.getJson().jsonPath("$[*].v_pid").all();
			System.out.println(cid);
/*			for(int i=0;i<cname.size();i++) {
				cpname10.add(cpname00);
			}*/
		}else {
			System.out.println(dpname00);
			System.out.println(dcname00);
			String n = page.getHtml().regex("\\(([\\s\\S]*)\\)").get();
			List<String> name00 = JsonPath.read(n, "$.dealer_info[*].dealer_name");
			System.out.println(name00);
			name.addAll(name00);
			List<String> add = JsonPath.read(n, "$.dealer_info[*].dealer_adr");
			System.out.println(add);
			address.addAll(add);
			List<String> p1 = JsonPath.read(n, "$.dealer_info[*].dealer_tel_1");
			System.out.println(p1);
			sellTell.addAll(p1);
			List<String> p2 = JsonPath.read(n, "$.dealer_info[*].dealer_tel_2");
			System.out.println(p2);
			serviceTell.addAll(p2);
			List<String> jw00 = JsonPath.read(n, "$.dealer_info[*].dealer_xy");
			//System.out.println(jw00);
			//List<String> jing = new ArrayList<>();
			//List<String> wei = new ArrayList<>();
			for(int i=0;i<jw00.size();i++) {
				proName.add(dpname00);
				cityName.add(dcname00);
				String[] jw10 = jw00.get(i).split(",");
				lng.add(jw10[0]);
				lat.add(jw10[1]);
				dnull.add("");
			}
		}
	}

	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
			.addHeader("Host", "www.civic.com.cn")
			.addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.civic.com.cn/")
			.addHeader("X-Requested-With", "XMLHttpRequest")
			.addHeader("Connection", "keep-alive")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(3)
			;
	Spider.create(new DongFengBenTianPageProcessor())
	.addUrl("http://www.civic.com.cn/api_list.php?op=city")
	.run();
	for(int p=0;p<pid.size();p++) {
	site = Site.me()
				.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
				.addHeader("Host", "www.civic.com.cn")
				.addHeader("Accept", "*/*")
				.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
				.addHeader("Accept-Encoding", "gzip, deflate")
				.addHeader("Referer", "http://www.civic.com.cn/civicmap/map.html")
				.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
				.addHeader("X-Requested-With", "XMLHttpRequest")
				.addHeader("Connection", "keep-alive")
				.setSleepTime(0)
				.setCycleRetryTimes(3)
				;
	Request request = new Request("http://www.civic.com.cn/index.php/buy/ajax.html");
	request.setMethod(HttpConstant.Method.POST);
	request.setRequestBody(HttpRequestBody.json("pid="+pid.get(p)+"&action=city", "utf-8"));
	Spider.create(new DongFengBenTianPageProcessor())
	.addRequest(request)
	.run();
	for(int i=0;i<cid.size();i++) {
		dpname00 = pname.get(p);
		dcname00 = cname.get(i);
	site = Site.me()
				.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
				.addHeader("Host", "www.civic.com.cn")
				.addHeader("Accept", "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01")
				.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
				.addHeader("Accept-Encoding", "gzip, deflate")
				.addHeader("Referer", "http://www.civic.com.cn/civicmap/map.html")
				.addHeader("X-Requested-With", "XMLHttpRequest")
				.addHeader("Connection", "keep-alive")
				.setSleepTime(0)
				.setCycleRetryTimes(3)
				;
	Spider.create(new DongFengBenTianPageProcessor())
	.addUrl("http://www.civic.com.cn/index.php/rv_iframenewcivic/rv_dealer_greiz/"+cid.get(i))
	.run();
	}
	}
/*	for(int i=0;i<cid.size();i++) {
		dcname00 = cname.get(i);
		dpname00 = cpname10.get(i);
	site = Site.me()
				.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
				.addHeader("Host", "www.civic.com.cn")
*/				//.addHeader("Accept", "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01")
/*				.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
				.addHeader("Accept-Encoding", "gzip, deflate")
				.addHeader("Referer", "http://www.civic.com.cn/civicmap/map.html")
				.addHeader("X-Requested-With", "XMLHttpRequest")
				.addHeader("Connection", "keep-alive")
				.setSleepTime(0)
				.setCycleRetryTimes(3)
				;
	Spider.create(new DongFengBenTianPageProcessor())
	.addUrl("http://www.civic.com.cn/index.php/rv_iframenewcivic/rv_dealer_greiz/"+cid.get(i))
	.run();
	}*/
		AgencyEntity2 agency = new AgencyEntity2();
		//agency.setdCloseDate(null);
		//agency.setdOpeningDate(null);
		agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
		//agency.setnBrandID(-1);
		agency.setsBrand("思铭");
		//agency.setnDealerIDWeb(-1);
		//agency.setnManufacturerID(-1);
		agency.setsManufacturer("东风本田");
		//agency.setnState(1);
		agency.setsAddress(address);
		agency.setsCity(cityName);
		agency.setsCustomerServiceCall(serviceTell);
		agency.setsDealerName(name);
		agency.setsDealerType(dnull);
		agency.setsProvince(proName);
		agency.setsSaleCall(sellTell);
	
		agency.setsLongitude(lng);
		agency.setsLatitude(lat);
		//agency.setsControllingShareholder(sControllingShareholder);
		//agency.setsOtherShareholders(sOtherShareholders);
		new AgencyDao().add(agency);
}
}
 