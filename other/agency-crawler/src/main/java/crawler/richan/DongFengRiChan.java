package crawler.richan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MyX509TrustManager;
import util.MybatisTool;
import util.TianYanCha;
//证书问题 WARNING: Hostname is not matched for cert.
public class DongFengRiChan {
	List<AgencyEntity> getAgency() throws InterruptedException {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "https://www.dongfeng-nissan.com.cn/Ajax/AjaxSupport.ashx?method=ProvinceFilter";
		String proStr = getJson(getPro);
		JSONArray provinces = JSON.parseArray(proStr);
		for (int i = 0; i < provinces.size(); i++) {
			JSONObject province = provinces.getJSONObject(i);
			String proNum = province.getString("ItemID");
			String proName = province.getString("Name");

			String getCity = "https://www.dongfeng-nissan.com.cn/Ajax/AjaxSupport.ashx?method=CityFilter&ProvinceID="
					+ proNum + "&Brand=1";
			String cityStr = getJson(getCity);
			JSONArray citys = JSON.parseArray(cityStr);
/*			if (citys == null) {
				System.out.println(getCity);
			}*/
			for (int j = 0; j < citys.size(); j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityNum = city.getString("ItemID");
				String cityName = city.getString("Name");
				String getDealer = "https://www.dongfeng-nissan.com.cn/Ajax/AjaxSupport.ashx?method=LoadDealerData&brand=1&selpro="
						+ proNum + "&selcity=" + cityNum;
				// System.out.println("=="+getDealer);
				String dealers = getJson(getDealer);
				Map<String, Object> map = (Map<String, Object>) JSON.parseObject(dealers);
				Collection<Object> collection = map.values();
				for (Object obj : collection) {
					JSONObject dealer = JSON.parseObject(obj.toString());
					String name = dealer.getString("Name");
					String address = dealer.getString("Address");
					String sellTell = dealer.getString("SalesPhone");
					String lng = dealer.getString("Longitude");
					String lat = dealer.getString("Latitude");

					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);

					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("日产");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("东风日产");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					//System.out.println(agency);
					list.add(agency);

				}

			}

		}

		return list;
	}
	
	String getJson(String path) {
		try {
			Thread.sleep(1000*5);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		BufferedReader br = null;

		try {
			SSLContext sslcontext = SSLContext.getInstance("SSL", "SunJSSE");
			sslcontext.init(null, new TrustManager[] { new MyX509TrustManager() }, new java.security.SecureRandom());
			HostnameVerifier ignoreHostnameVerifier = new HostnameVerifier() {
				public boolean verify(String s, SSLSession sslsession) {
					System.out.println("WARNING: Hostname is not matched for cert.");
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(ignoreHostnameVerifier);
			HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
			
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(1000*30);
			// 设置通用属性
			conn.setRequestProperty("Connection", "Keep-Alive");// 维持长连接
			conn.setRequestProperty("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
			conn.setRequestProperty("Cookie", "route=cfaad62b14308c271be7e68447fdcb13; ASP.NET_SessionId=w0yex5dlaewmsnzpg4xcox11; SC_ANALYTICS_GLOBAL_COOKIE=fdba524207374811b033750a37324917; SC_ANALYTICS_SESSION_COOKIE=5C20556A400E47948870801312086C50|1|w0yex5dlaewmsnzpg4xcox11; NTKF_T2D_CLIENTID=guest3763E676-7792-D8B3-9758-637141C3E996; nTalk_CACHE_DATA={uid:kf_9255_ISME9754_guest3763E676-7792-D8,tid:1530676724162919}; _smt_uid=5b3c45f4.4b08724e; _pk_ref.2.d369=%5B%22%22%2C%22%22%2C1530682720%2C%22https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DksNtCG2czLSkhBA2yVRakBkrMpCGXX14DBczPJkF2tDlUwQgwkMXj3MVfHKtKYXL%26ck%3D6252.3.148.255.157.188.143.210%26shh%3Dwww.baidu.com%26wd%3D%26eqid%3D9d08ef4b0003dd41000000045b3c45f0%22%5D; _pk_id.2.d369=f32607d1231f634c.1530676724.2.1530682720.1530682720.; _pk_ses.2.d369=*");
			conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			conn.connect();// 可以省了,使用下面的urlConn.getOutputStream()会自动connect
			int code = conn.getResponseCode();
			if (code != 200) {
				System.out.println(code);
				return "";
			}
			// 建立输入流,读取返回的信息
			br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null; // 每行内容
			StringBuffer content = new StringBuffer();
			while ((line = br.readLine()) != null) {
				content.append(line);
				content.append("\r");
			}
			// System.out.println(content);
			return content.toString();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}
	
	public static void main(String[] args) throws InterruptedException {
		DongFengRiChan crawler = new DongFengRiChan();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
