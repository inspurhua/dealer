package crawler.kaiyi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;
//-1
public class KaiYiQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> allList = new ArrayList<AgencyEntity>();
		String getProDealer = "http://www.cowinhome.com/data/tms/website_pc/html/scripts/sys_zone.json";
		List<AgencyEntity> dealerList = getData("经销商", getProDealer);
		System.out.println("666==="+dealerList);
		String getProService = "http://www.cowinhome.com/data/tms/website_pc/html/scripts/sys_servicer_zone.json";
		List<AgencyEntity> serviceList = allList = getData("服务商", getProService);
		allList.addAll(serviceList);
		allList.addAll(dealerList);
		return allList;
	}
	public List<AgencyEntity> getData(String type, String url) {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		try {
			//String getPro = "http://www.cowinhome.com/data/tms/website_pc/html/scripts/sys_zone.json";
			String proAndCity = HttpConnectionGet.getJson(url);
			//以#切,省委单位切分
			String[] provinces = proAndCity.split("#");
			//以$切,获得省份以及其城市
			for(String str : provinces) {
				String[] province = str.split("\\$");
				System.out.println(province[0]+"==0=="+province.length);
				//如果没有省份和城市,则跳过
				if(province.length<2) {
					//System.out.println("777777777777777777777777777777");
					continue;
				}
				//提取省和城市
				String proName = province[0];
				//System.out.println(proName);
				String[] cities = province[1].split("\\|");
				for(String cityName : cities) {
					String getDealer = null;
					if("经销商".equals(type)) {
						getDealer = "http://www.cowinhome.com/prj/distributor/dealer?province="+proName+"&city="+cityName+"&pageSize=4&range=50000&pageNum=1";
					}else if("服务商".equals(type)){
						getDealer = "http://www.cowinhome.com/prj/servicer/dealer?province="+proName+"&city="+cityName+"&pageSize=4&range=50000&pageNum=1";
					}
					String dealerText = HttpConnectionGet.getJson(getDealer);
					//解析JSON
					//System.out.println(JSON.parseObject(dealerText).getJSONArray("list"));
					JSONArray dealerArray = JSON.parseObject(dealerText).getJSONArray("list");
					if(dealerArray==null||dealerArray.isEmpty()) {
						continue;
					}
					for(int i=0;i<dealerArray.size();i++) {
						JSONObject dealerObject = dealerArray.getJSONObject(i);
						String name = dealerObject.getString("distributorName");
						String address = dealerObject.getString("address");
						String sellTell = dealerObject.getString("salePhone");
						String serviceTell = dealerObject.getString("servicePhone");
						String lng = dealerObject.getString("longitude");
						String lat = dealerObject.getString("latitude");
						proName = dealerObject.getString("province");
						cityName = dealerObject.getString("city");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("凯翼");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("凯翼汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(serviceTell);
						agency.setsDealerName(name);
						agency.setsDealerType(type);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		return list;
	}
	public static void main(String[] args) {
		KaiYiQiCheCrawler crawler = new KaiYiQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
	}

}
