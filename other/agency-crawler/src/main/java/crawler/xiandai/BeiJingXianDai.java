package crawler.xiandai;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;
//证书问题
public class BeiJingXianDai {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//获取省份
		String getPro = "https://www.beijing-hyundai.com.cn/datacenter/static/js/Province.js";
		String proStr = HttpConnectionGet.getJson(getPro).split("=")[1];
		JSONArray provinces = JSON.parseArray(proStr);
		Integer proNum = null;
		//获取城市
		String getCity = "https://www.beijing-hyundai.com.cn/datacenter/static/js/City.js";
		String cityStr = HttpConnectionGet.getJson(getCity).split("=")[1];
		JSONArray citys = JSON.parseArray(cityStr);
		Integer cityNum = null;
		//获取经销商
		String getDealer = "https://www.beijing-hyundai.com.cn/datacenter/static/js/District.js";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("Nashorn");
		try {
			String dealersJSON = (String)engine.eval(dealerStr+" JSON.stringify(District)");
			JSONArray dealers = JSON.parseArray(dealersJSON);
			for (int j=0;j<dealers.size();j++) {
				JSONObject dealer = dealers.getJSONObject(j);
				if(dealer!=null) {
					String address = dealer.getString("Address");
					String name = dealer.getString("DisName");
					String sellTell = dealer.getString("Sales_tel");
					cityNum = dealer.getInteger("CityID");
					String lng = dealer.getString("Lng");
					String lat = dealer.getString("Lat");
					String serviceTell = dealer.getString("Tel");
					String type = dealer.getString("DisSort");
					String cityName = null;
					String proName = null;
					for (int x = 0;x<citys.size();x++) {
						JSONObject cityJSON = citys.getJSONObject(x);
						if (cityJSON.containsValue(cityNum)) {
							cityName = cityJSON.getString("CityName");
							proNum = cityJSON.getInteger("ProID");
							break;
						}
					}
					for (int y=0;y<provinces.size();y++) {
						JSONObject pro = provinces.getJSONObject(y);
						if (pro.containsValue(proNum)) {
							proName = pro.getString("ProName");
							break;
						}
					}
					
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("现代");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("北京现代");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(serviceTell);
					agency.setsDealerName(name);
					agency.setsDealerType(type);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
					
				}
			}
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public static void main(String[] args) {
		BeiJingXianDai crawler = new BeiJingXianDai();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");	}
}
