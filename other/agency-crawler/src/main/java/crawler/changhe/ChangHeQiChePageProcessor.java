package crawler.changhe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月2日 下午4:22:44 
* 类说明 
*/
public class ChangHeQiChePageProcessor implements PageProcessor {
	private static List<String> pid ;
	private static List<String> pname ;
	private static List<String> cpid ;
	private static List<String> cid;
	private static List<String> cname ;
	@Override
	public void process(Page page) {
/*		List<String> pid =null;
		List<String> pname =null;
		List<String> cpid =null;
		List<String> cid = null ;
		List<String> cname =null;*/
		if(page.getUrl().regex("province").match()) {
		String n = "["+page.getJson().regex("\\[([\\s\\S]+)").get();
		pid = JsonPath.read(n, "$[*].ProID");
		//System.out.println(pid);
		pname = JsonPath.read(n, "$[*].ProName");
		//System.out.println(pname);
		page.addTargetRequest("https://www.changheauto.com/Api/Area/city_list/type/1.js");
		}else if(page.getUrl().regex("city").match()) {
		String n = "["+page.getJson().regex("\\[([\\s\\S]+)").get();
		cpid = JsonPath.read(n, "$[*].ProID");
		//System.out.println(cpid);
		cid = JsonPath.read(n, "$[*].CityID");
		//System.out.println(cid);
		cname = JsonPath.read(n, "$[*].CityName");
		//System.out.println(cname);
		page.addTargetRequest("https://www.changheauto.com/Api/Area/district_list/type/1.js");
		}else {
		String n = "["+page.getJson().regex("\\[([\\s\\S]+)").get();
		List<String> dcid = JsonPath.read(n, "$[*].CityID");
		List<String> cityName = new ArrayList<>();
		List<String> proName = new ArrayList<>();
		for(int i=0;i<dcid.size();i++) {
			for(int c=0;c<cid.size();c++)
			if(dcid.get(i).equals(cid.get(c))) {
				for(int p=0;p<pid.size();p++) {
					if(cpid.get(c).equals(pid.get(p))) {
						proName.add(pname.get(p));
						cityName.add(cname.get(c));
					}else {}
				}
			}else {}
		}
		List<String> name = JsonPath.read(n, "$[*].DisName");
		//System.out.println(name);
		List<String> address = JsonPath.read(n, "$[*].Address");
		//System.out.println(address);
		List<String> sellTell = JsonPath.read(n, "$[*].Tel");
		//System.out.println(sellTell);
		List<String> salestel = JsonPath.read(n, "$[*].Sales_tel");
		//System.out.println(salestel);
		List<String> lng = JsonPath.read(n, "$[*].Lng");
		//System.out.println(lng);
		List<String> lat = JsonPath.read(n, "$[*].Lat");
		//System.out.println(lat);
		List<String> dnull = new ArrayList<>();
		for(String n2 : name) {
			n2 = "";
			dnull.add(n2);
		}
		AgencyEntity2 agency = new AgencyEntity2();
		//agency.setdCloseDate(null);
		//agency.setdOpeningDate(null);
		agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
		//agency.setnBrandID(-1);
		agency.setsBrand("昌河");
		//agency.setnDealerIDWeb(-1);
		//agency.setnManufacturerID(-1);
		agency.setsManufacturer("昌河汽车");

		//agency.setnState(1);
		agency.setsAddress(address);
		agency.setsCity(cityName);
		agency.setsCustomerServiceCall(salestel);
		agency.setsDealerName(name);
		agency.setsDealerType(dnull);
		agency.setsProvince(proName);
		agency.setsSaleCall(sellTell);
		agency.setsLongitude(lng);
		agency.setsLatitude(lat);
		new AgencyDao().add(agency);
		//agency.setsControllingShareholder(sControllingShareholder);
		//agency.setsOtherShareholders(sOtherShareholders);
		
		}
	}
private Site site = Site.me()
					.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
					.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
					.addHeader("Accept-Encoding", "gzip, deflate, br")
					.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
					.addHeader("Connection", "keep-alive")
					.addHeader("Host", "www.changheauto.com")
					.addHeader("Upgrade-Insecure-Requests", "1")
					.setSleepTime(0)
					.setTimeOut(60000)
					.setCycleRetryTimes(3)
					;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ChangHeQiChePageProcessor())
	.addUrl("https://www.changheauto.com/Api/Area/province_list/type/1.js")
	//.addUrl("https://www.changheauto.com/Api/Area/city_list/type/1.js")
	//.addUrl("https://www.changheauto.com/Api/Area/district_list/type/1.js")
	.run();
}
}
 