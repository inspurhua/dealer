package crawler.changhe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class ChangHeQiCheCrawler {
	public List<AgencyEntity> getAgency(){
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//获取省份json
		String getPro = "http://www.changheauto.com/Api/Area/province_list/type/1.js";
		String proText = HttpConnectionGet.getJson(getPro);
		proText = proText.substring(proText.indexOf("["));
		JSONArray proArray = JSON.parseArray(proText);
		//将省份的id和name存入一个map
		Map<String, String> proMap = new HashMap<String, String>();
		for(int i=0;i<proArray.size();i++) {
			String proNum = proArray.getJSONObject(i).getString("ProID");
			String proName = proArray.getJSONObject(i).getString("ProName");
			proMap.put(proNum, proName);
		}
		//获取城市的id和name
		String getCity = "http://www.changheauto.com/Api/Area/city_list/type/1.js";
		String cityText = HttpConnectionGet.getJson(getCity);
		cityText = cityText.substring(cityText.indexOf("["));
		JSONArray cityArray = JSON.parseArray(cityText);
		//将城市id和name存入一个map中
		Map<String, String> cityMap = new HashMap<String, String>();
		//将城市id和省份id存入一个map中
		Map<String, String> pro_cityMap = new HashMap<String, String>();
		for(int j=0;j<cityArray.size();j++) {
			String cityNum = cityArray.getJSONObject(j).getString("CityID");
			String cityName = cityArray.getJSONObject(j).getString("CityName");
			String proNum = cityArray.getJSONObject(j).getString("ProID");
			cityMap.put(cityNum, cityName);
			pro_cityMap.put(cityNum, proNum);
			
		}
		//获取所有经销商json
		String getDealer = "http://www.changheauto.com/Api/Area/district_list/type/1.js";
		String dealerText = HttpConnectionGet.getJson(getDealer).trim();
		dealerText = dealerText.substring(dealerText.indexOf("["), dealerText.length()-1);
		JSONArray dealerArray = JSON.parseArray(dealerText);
		for(int i=0;i<dealerArray.size();i++) {
			JSONObject dealer = dealerArray.getJSONObject(i);
			String name = dealer.getString("DisName");
			String sellTell = dealer.getString("Sales_tel");
			String address = dealer.getString("Address");
			String typeNum = dealer.getString("Dealer_type");
			String lng = dealer.getString("Lng");
			String lat = dealer.getString("Lat");
			String type = null;
			if("1".equals(typeNum)) {
				type = "4S店";
			}
			String cityNum = dealerArray.getJSONObject(i).getString("CityID");
			String cityName = cityMap.get(cityNum);
			String proNum = pro_cityMap.get(cityNum);
			String proName = proMap.get(proNum);
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("昌河");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("昌河汽车");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(type);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			
			list.add(agency);
			
		}
		
		
		
		
		return list;
	}
	public static void main(String[] args) {
		ChangHeQiCheCrawler crawler = new ChangHeQiCheCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
		
	}

}
