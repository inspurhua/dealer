package crawler.mashaladi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.AddressComponent;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.ProCity;
import util.TianYanCha;

public class MaShaLaDi {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealerID = "https://www.geocms.it/Server/servlet/S3JXServletCall?parameters=method_name%3DGetObject%26callback%3Data%26id%3D1%26clear%3Dtrue%26query%3D((%255Bsales%255D%253D%255Btrue%255D%2520OR%2520%255Bassistance%255D%253D%255Btrue%255D)%2520AND%2520%255BcountryIsoCode2%255D%253D%255BCN%2520OR%2520cn%255D)%2520AND%2520%255BcountryIsoCode2%255D%253D%255BCN%2520OR%2520cn%255D%26licenza%3Dgeo-maseratispa%26progetto%3DDealerLocator%26lang%3Dzh&encoding=UTF-8";
		String dealerIDStr = HttpConnectionGet.getJson(getDealerID);
		dealerIDStr = getJSONStr(dealerIDStr);
		JSONArray dealers = JSON.parseObject(dealerIDStr).getJSONArray("L").getJSONObject(0).getJSONArray("O");
		for (int i = 0; i < dealers.size(); i++) {
			String dealerID = dealers.getJSONObject(i).getString("id");
			boolean flag = dealerID.matches("\\d+");
			if (!flag) {
				continue;
			}
			String getDealer = "https://www.geocms.it/Server/servlet/S3JXServletCall?parameters=method_name%3DBalloonOnDemand%26callback%3Dscube.geocms.GeoResponse.execute%26idLayer%3DDealer%26idObject%3D"
					+ dealerID + "%26licenza%3Dgeo-maseratispa%26progetto%3DDealerLocator%26lang%3Dzh&encoding=UTF-8";
			String dealerText = HttpConnectionGet.getJson(getDealer);
			dealerText = getJSONStr(dealerText);
			JSONObject dealerInfo = JSON.parseObject(dealerText).getJSONArray("L").getJSONObject(0).getJSONArray("O")
					.getJSONObject(0);
			JSONObject dealer = dealerInfo.getJSONObject("U");
			String address = dealer.getString("address");
			String name = dealer.getString("dealername");
			String proName = dealer.getString("province");
			String cityName = dealer.getString("city");
			String sellTell = dealer.getString("phone");
			String lng = dealer.getString("long");
			String lat = dealer.getString("lat");

			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);

			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("玛莎拉蒂");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("玛莎拉蒂");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);

			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			//System.out.println(agency);
			list.add(agency);

		}

		return list;
	}

	String getJSONStr(String dealerStr) {
		try {

			int start = dealerStr.indexOf("\"");
			int end = dealerStr.lastIndexOf("}") + 2;
			dealerStr = dealerStr.substring(start, end);
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("js");

			dealerStr = (String) engine.eval(dealerStr);
		} catch (ScriptException e) {

			e.printStackTrace();
			System.out.println(dealerStr);
		}
		return dealerStr;

	}

	public static void main(String[] args) {
		MaShaLaDi crawler = new MaShaLaDi();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
