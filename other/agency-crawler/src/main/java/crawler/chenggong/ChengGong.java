package crawler.chenggong;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class ChengGong {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.chenggongauto.com/admin/index.php?m=api&c=index&type=linkage&catId=0";
		String proStr = HttpConnectionGet.getJson(getPro);
		JSONArray provinces = JSON.parseArray(proStr);
		for (int i=0;i<provinces.size();i++) {
			JSONObject pro = provinces.getJSONObject(i);
			String proName = pro.getString("name");
			String proNum = pro.getString("id");
			JSONArray citys = pro.getJSONArray("child");
			for (int j=0;j<citys.size();j++) {
				JSONObject city = citys.getJSONObject(j);
				String cityName = city.getString("name");
				String cityNum = city.getString("id");
				
				String getDealer = "http://www.chenggongauto.com/admin/index.php?m=api&c=index&type=storeList&&p=1&p_size=100&provinceId="+proNum+"&cityId="+cityNum;
				String dealerStr = HttpConnectionGet.getJson(getDealer);
				JSONArray dealers = JSON.parseObject(dealerStr).getJSONArray("list");
				for (int k=0;k<dealers.size();k++) {
					JSONObject dealer = dealers.getJSONObject(k);
					String lng = dealer.getString("lng");
					String lat = dealer.getString("lat");
					String dealerInfo = dealer.getString("description");
					String name = dealer.getString("title");
					String[] strs = dealerInfo.split("\r");
					String sellTell = "";
					String address = "";
					boolean flag = true;
					for (String s : strs) {
						if (s.contains("电话") && flag) {
							sellTell = s.split("：")[1].replaceAll("<.*>", "").replaceAll("&nbsp;", " ");
							flag = false;
						}
						if (s.contains("地址")) {
							address = s.split("：")[1].replaceAll("<.*>", "").replaceAll("&nbsp;", " ");
						}
					}
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					

					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("成功");
					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("成功汽车");
					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);

					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
					
					
				}
				
				
			}
			
		}
		
		
		
		
		return list;
	}
	public static void main(String[] args) {
		ChengGong crawler = new ChengGong();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
