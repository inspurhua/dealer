package crawler.qirui;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.HttpConnectionPost;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class QiRuiCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.chery.cn/Umbraco/Surface/ajax/GetProvinces";
		String proText = HttpConnectionGet.getJson(getPro);
		JSONArray proArray = JSON.parseArray(proText);
		for(int i=0;i<proArray.size();i++) {
			String proNum = proArray.getJSONObject(i).getString("PROVINCE_ID");
			String proName = proArray.getJSONObject(i).getString("PROVINCE_NAME");
			
			String getCity = "http://www.chery.cn/Umbraco/Surface/ajax/GetCitysByProvinceId";
			String proParam = "provinceid="+proNum;
			String cityText = HttpConnectionPost.getJson(getCity, proParam);
			JSONArray cities = JSON.parseArray(cityText);
			for(int j=0;j<cities.size();j++) {
				String cityNum = cities.getJSONObject(j).getString("CITY_ID");
				String cityName = cities.getJSONObject(j).getString("CITY_NAME");
				
				//获取经销商json
				String getDealer = "http://www.chery.cn/Umbraco/Surface/ajax/GetDealersAndProvider";
				String cityParam = "PROVINCE_ID="+proNum+"&CITY_ID="+cityNum;
				String dealerText = HttpConnectionPost.getJson(getDealer, cityParam);
				JSONArray dealers = JSON.parseArray(dealerText);
				for(int k=0;k<dealers.size();k++) {
					String name = dealers.getJSONObject(k).getString("Name");
					String address = dealers.getJSONObject(k).getString("Address");
					String sellTell = dealers.getJSONObject(k).getString("SalesPhone");
					String type = dealers.getJSONObject(k).getString("Type");
					String[] lngAndLat = Location.getLocation(address);
					String lng = lngAndLat[0];
					String lat = lngAndLat[1];
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					if("2".equals(type)) {
						type = "服务商";
					} else {
						type = "经销商";
					}
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("奇瑞");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("奇瑞汽车");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
				}
			}
		} 
		return list;
	}
	
	public static void main(String[] args) {
		QiRuiCrawler crawler = new QiRuiCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
