package crawler.jinbei;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
//编码 请求头
/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年10月11日 下午4:47:42 
* 类说明 
*/
public class HuaChenJinBeiPageProcessor implements PageProcessor {
	private static List<String> address = new ArrayList<>();
	private static List<String> cityName = new ArrayList<>();
	//private static List<String> serviceTell = new ArrayList<>();
	private static List<String> name = new ArrayList<>();
	private static List<String> dnull = new ArrayList<>();
	private static List<String> type = new ArrayList<>();
	private static List<String> proName = new ArrayList<>();
	private static List<String> sellTell = new ArrayList<>();
	private static List<String> lng = new ArrayList<>();
	private static List<String> lat = new ArrayList<>();
	private static final String url = "http://www.jinbei.com/buycar/dealer";
	private static Site site;
	@Override
	public void process(Page page) {
		if(page.getUrl().regex(url).match()) {
			List<String> nid = page.getHtml().xpath("ul[@id='province']/li").regex("[0-9]{1,3}").all();
			List<String> nidurl = new ArrayList<>();
			for(int i=0;i<nid.size();i++) {
				site = Site.me()
						.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
						.addHeader("Host", "www.jinbei.com")
						.addHeader("Accept", "*/*")
						.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
						.addHeader("Accept-Encoding", "gzip, deflate")
						.addHeader("Referer", "http://www.jinbei.com/buycar/dealer")
						.addHeader("X-Requested-With", "XMLHttpRequest")
						.addHeader("Connection", "keep-alive")
						.setSleepTime(0)
						.setCycleRetryTimes(3)
						;
				nidurl.add("http://www.jinbei.com/getdealer/getDealer?province="+nid.get(i)+"&city=&ageny=0&repair=0");
			}
			page.addTargetRequests(nidurl);
		}else {
			List<String> name00 = page.getJson().jsonPath("$[*].shop_name").all();
			name.addAll(name00);
			List<String> address00 = page.getJson().jsonPath("$[*].shop_addr").all();
			address.addAll(address00);
			List<String> type00 = page.getJson().jsonPath("$[*].shop_type").all();
			type.addAll(type00);
			List<String> sellTell00 = page.getJson().jsonPath("$[*].shop_phone").all();
			sellTell.addAll(sellTell00);
			List<String> proName00 = page.getJson().jsonPath("$[*].province").all();
			proName.addAll(proName00);
			List<String> cityName00 = page.getJson().jsonPath("$[*].city").all();
			cityName.addAll(cityName00);
			List<String> lng00 = page.getJson().jsonPath("$[*].point.lng").all();
			lng.addAll(lng00);
			List<String> lat00 = page.getJson().jsonPath("$[*].point.lat").all();
			lat.addAll(lat00);
			for(int i=0;i<name.size();i++) {
				dnull.add("");
			}
		}
	}
/*	private Site site=Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0")
			//.setCharset("GBK")
			.setSleepTime(0)
			//.setTimeOut(300)
			.setCycleRetryTimes(3)
			;*/
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.jinbei.com")
			.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://www.jinbei.com/")
			.addHeader("Connection", "keep-alive")
			.addHeader("Upgrade-Insecure-Requests", "1")
			.addHeader("Cache-Control", "max-age=0")
			.setSleepTime(0)
			.setTimeOut(60000)
			.setCycleRetryTimes(3)
			;
	Spider.create(new HuaChenJinBeiPageProcessor())
	.addUrl("http://www.jinbei.com/buycar/dealer")
	//.addUrl("http://www.jinbei.com/getdealer/getDealer?province=73&city=&ageny=0&repair=0")
	.run();
	AgencyEntity2 agency = new AgencyEntity2();
	//agency.setdCloseDate(null);
	//agency.setdOpeningDate(null);
	agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
	//agency.setnBrandID(-1);
	agency.setsBrand("金杯");

	//agency.setnDealerIDWeb(-1);
	//agency.setnManufacturerID(-1);
	agency.setsManufacturer("华晨金杯");
	//agency.setnState(1);
	agency.setsAddress(address);
	agency.setsCity(cityName);
	agency.setsCustomerServiceCall(dnull);
	agency.setsDealerName(name);
	agency.setsDealerType(type);
	agency.setsProvince(proName);
	agency.setsSaleCall(sellTell);
	agency.setsLongitude(lng);
	agency.setsLatitude(lat);
	//agency.setsControllingShareholder(null);
	//agency.setsOtherShareholders(null);
	new AgencyDao().add(agency);
}
}
 