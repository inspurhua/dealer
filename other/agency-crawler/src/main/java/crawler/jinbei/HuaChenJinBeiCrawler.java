package crawler.jinbei;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;
//404·
public class HuaChenJinBeiCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		//String getPro = "http://www.jinbei.com/GetDProvList.ashx";
		String getPro = "http://www.jinbei.com/buycar/dealer";
		String proText = HttpConnectionGet.getJson(getPro);
		JSONArray provinces = JSON.parseArray(proText);
		System.out.println(provinces);
		for(int i=0;i<provinces.size();i++) {
			String proName = provinces.getJSONObject(i).getString("ProvinceName");
			
			String getCity = "http://www.jinbei.com/GetDCityList.ashx?provname="+proName;
			String cityText = HttpConnectionGet.getJson(getCity);
			JSONArray cities = JSON.parseArray(cityText);
			for(int j=0;j<cities.size();j++) {
				String cityName = cities.getJSONObject(j).getString("CityName");
			
				String getDealer = "http://www.jinbei.com/GetDealerList.ashx?cityname="+cityName;
				String dealerText = HttpConnectionGet.getJson(getDealer);
				JSONArray dealers = JSON.parseArray(dealerText);
				for(int k=0;k<dealers.size();k++) {
					JSONObject dealer = dealers.getJSONObject(k);
					String name = dealer.getString("DealerName");
					String sellTell = dealer.getString("Tel");
					String address = dealer.getString("Address");
					String lng = dealer.getString("Longitude");
					String lat = dealer.getString("Latitude");
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("金杯");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("华晨金杯");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
				
				
				}
				
			
			}
			
		
		}
		
		return list;
	}
	public static void main(String[] args) {
		HuaChenJinBeiCrawler crawler = new HuaChenJinBeiCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
