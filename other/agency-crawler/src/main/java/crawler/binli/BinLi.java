package crawler.binli;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.omg.CosNaming.BindingListHolder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.ProCity;

/*Exception in thread "main" java.lang.IndexOutOfBoundsException: Index: 1, Size: 1
at java.util.ArrayList.rangeCheck(Unknown Source)
at java.util.ArrayList.get(Unknown Source)
at com.alibaba.fastjson.JSONArray.getJSONObject(JSONArray.java:245)
at crawler.binli.BinLi.getAgency(BinLi.java:32)
at crawler.binli.BinLi.main(BinLi.java:71)
*/

public class BinLi {
	List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "https://www.bentleymotors.com/content/brandmaster/master/pom/_jcr_content.api.json";
		String dealerStr = HttpConnectionGet.getJson(getDealer);
		JSONArray dealers = JSON.parseObject(dealerStr).getJSONArray("dealers");
		for (int i = 0; i < dealers.size(); i++) {
			JSONObject dealer = dealers.getJSONObject(i);
			String live = dealer.getString("live");
			String region = dealer.getString("region");
			if ("China HK & MC".equals(region) && "true".equals(live)) {
				System.out.println("========"+dealer.toString());
				String name = dealer.getString("dealerName");
				System.out.println("==========2======"+dealer.getJSONArray("addresses"));
				JSONObject info = dealer.getJSONArray("addresses").getJSONObject(1);
				String lng = info.getString("baiduLong");
				String lat = info.getString("baiduLat");
				String sellTell = info.getJSONArray("departments").getJSONObject(0).getString("phone");
				String[] proCity = ProCity.getData(lat, lng);
				String proName = proCity[0];
				String cityName = proCity[1];
				String address = info.getJSONObject("street").getString("default");

				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("宾利");
				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("宾利汽车");
				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(null);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);

				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				System.out.println(agency);
				list.add(agency);
			}
		}
		return list;
	}

	public static void main(String[] args) {
		BinLi crawler = new BinLi();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for (AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
