package crawler.yema;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class SiChuanQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String debug = null;
		try {
			String getPro = "http://www.yemaauto.cn/html/user_449.html";
			Document proDoc = Jsoup.connect(getPro).get();
			Element proSelect = proDoc.getElementById("province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for(int i=1;i<proOptions.size();i++) {
				String proName = proOptions.get(i).text();
				String proNum = proOptions.get(i).attr("value");
				
				String getCity = "http://www.yemaauto.cn/index_city.php";
				Document cityDoc = Jsoup.connect(getCity).data("provinceid",proNum).post();
				Elements cityOptions = cityDoc.getElementsByTag("option");
				for(int j=1;j<cityOptions.size();j++) {
					String cityName = cityOptions.get(j).text();
					String cityNum = cityOptions.get(j).attr("value");
					
					
					String getDealer = "http://www.yemaauto.cn/data.php";
					String param = "provinceid="+proNum+"&cityid="+cityNum;
					String dealerText = HttpConnectionPost.getJson(getDealer, param);
					debug = dealerText;
					JSONArray dealers = JSON.parseArray(dealerText);
					//System.out.println(dealerText);
					for(int k=0;k<dealers.size();k++) {
						JSONObject dealer = dealers.getJSONObject(k);
						String name = dealer.getString("store_name");
						if(name==null) {
							continue;
						}
						String address = dealer.getString("address");
						String sellTell = dealer.getString("xsdh");
						String lng = dealer.getString("longitude");
						String lat = dealer.getString("latitude");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("野马");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("四川汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
						
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println(debug);
		}
		return list;
	}
	public static void main(String[] args) {
		SiChuanQiCheCrawler crawler = new SiChuanQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
