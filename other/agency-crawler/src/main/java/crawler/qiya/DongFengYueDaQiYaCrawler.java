package crawler.qiya;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class DongFengYueDaQiYaCrawler {
	public List<AgencyEntity> getAgency() throws ScriptException {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.dyk.com.cn/public/dyk/js/allDealersData.js?1234";
		String dealersVarStr = HttpConnectionGet.getJson(getDealer);
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("Nashorn");
		String dealersStr = (String) engine.eval(dealersVarStr + " JSON.stringify(dealersData)");
		Map<String, Object> dealersMap = JSON.parseObject(dealersStr);
		Collection<Object> proDealer = dealersMap.values();
		Stream<Object> stream = proDealer.stream().filter(obj->((List<Map<String, String>>)obj).size()>0);
		stream.forEach(obj->{
			((List<Map<String, String>>)obj).forEach(dealer->{
				String sellTell = dealer.get("sale_phone");
				String name = dealer.get("name");
				String address = dealer.get("address");
				String[] latAndLng = dealer.get("place").split(",");
				String lng = latAndLng[1];
				String lat = latAndLng[0];
				String proName = dealer.get("province_name");
				String cityName = dealer.get("city_name");
				String type = dealer.get("类别");
				List<String> shareholder = TianYanCha.getShareholder(name);
				String sControllingShareholder = shareholder.get(0);
				String sOtherShareholders = shareholder.get(1);
				
				
				AgencyEntity agency = new AgencyEntity();
				agency.setdCloseDate(null);
				agency.setdOpeningDate(null);
				agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
				agency.setnBrandID(-1);
				agency.setsBrand("起亚");

				agency.setnDealerIDWeb(-1);
				agency.setnManufacturerID(-1);
				agency.setsManufacturer("东风悦达起亚");

				agency.setnState(1);
				agency.setsAddress(address);
				agency.setsCity(cityName);
				agency.setsCustomerServiceCall(null);
				agency.setsDealerName(name);
				agency.setsDealerType(type);
				agency.setsProvince(proName);
				agency.setsSaleCall(sellTell);
				agency.setsLongitude(lng);
				agency.setsLatitude(lat);
				agency.setsControllingShareholder(sControllingShareholder);
				agency.setsOtherShareholders(sOtherShareholders);
				list.add(agency);
				
				
			});
		});
		return list;
		/*//proDealer.forEach(System.out::println);
		
		
		//System.out.println(dealersStr);
		try {
			String getPro = "http://www.dyk.com.cn/find_a_dealer";
			Document proDoc = Jsoup.connect(getPro).get();
			Element proSelect = proDoc.getElementById("province");
			Elements proOptions = proSelect.getElementsByTag("option");
			for(int i=1;i<proOptions.size();i++) {
				String proName = proOptions.get(i).text();
				String proNum = proOptions.get(i).attr("value");
				
				String getCity = "http://www.dyk.com.cn/index/get_city_bypid/"+proNum;
				Document cityDoc = Jsoup.connect(getCity)
									.data("dealer_type", "dot_query")
									.data("ajax", "true")
									.post();
				Elements cityOptions = cityDoc.getElementsByTag("option");
				for(int j=1;j<cityOptions.size();j++) {
					String cityName = cityOptions.get(j).text();
					String cityNum = cityOptions.get(j).attr("value");
					
					String getDeqaler = "http://www.dyk.com.cn/public/dyk/js/allDealersData.js?1234";
					String dealerText = HttpConnectionGet.getJson(getDealer);
					dealerText = dealerText.substring(dealerText.indexOf("{"));
					JSONObject dealerObject = JSON.parseObject(dealerText);
					JSONArray dealerArray = dealerObject.getJSONArray(cityNum);
					for(int k=0;k<dealerArray.size();k++) {
						String name = dealerArray.getJSONObject(k).getString("name");
						String address = dealerArray.getJSONObject(k).getString("address");
						String sellTell = dealerArray.getJSONObject(k).getString("sale_phone");
						String type = dealerArray.getJSONObject(k).getString("类别");
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("起亚");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("东风悦达起亚");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(type);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						list.add(agency);
						
					}
					
				}
				
				
			}
			
			
			
		} catch(Exception e) {
			throw new RuntimeException("爬虫异常", e);
		}
		
		
		
		
		return list;*/
	}
	
	public static void main(String[] args) throws ScriptException {
		DongFengYueDaQiYaCrawler crawler = new DongFengYueDaQiYaCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
