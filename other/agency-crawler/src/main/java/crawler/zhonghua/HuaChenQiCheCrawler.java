package crawler.zhonghua;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class HuaChenQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://www.zhonghuacar.com/dealer-search";
		String proText = HttpConnectionGet.getJson(getPro);
		int start = proText.indexOf("[{");
		int end = proText.indexOf("}];")+2;
		proText = proText.substring(start, end);
		JSONArray proArray = JSON.parseArray(proText);
		for(int i=0;i<proArray.size();i++) {
			String proName = proArray.getJSONObject(i).getString("name");
			JSONArray cityArray = proArray.getJSONObject(i).getJSONArray("cities");
			for(int j=0;j<cityArray.size();j++) {
				String cityNum = cityArray.getJSONObject(j).getString("cid");
				String cityName = cityArray.getJSONObject(j).getString("name");
				//获取经销商
				String getDealer = "http://www.zhonghuacar.com/index.php?option=com_api&task=dealers.getDealers";
				String param = "cityid="+cityNum;
				String dealerText = HttpConnectionPost.getJson(getDealer, param);
				JSONArray dealerArray = JSON.parseArray(dealerText);
				for(int k=0;k<dealerArray.size();k++) {
					JSONObject dealer = dealerArray.getJSONObject(k);
					String name = dealer.getString("name");
					String address = dealer.getString("address");
					String sellTell = dealer.getString("tel");
					String lng = dealer.getString("lon");
					String lat = dealer.getString("lat");
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("中华");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("华晨汽车");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
					
				}
			}
			
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		HuaChenQiCheCrawler crawler = new HuaChenQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
