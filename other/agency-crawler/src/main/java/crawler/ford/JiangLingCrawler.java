package crawler.ford;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionPost;
import util.MybatisTool;
import util.TianYanCha;

public class JiangLingCrawler {
	public List<AgencyEntity> getAgency() {
		
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		
		Map<String, String> proMap = new HashMap<String, String>();
		//发送请求,获得所有省名和编号
		String proUrl = "http://suv.jmc.com.cn/index.php/welcome/get_city";
		try {
			Document proDoc = Jsoup.connect(proUrl).execute().parse();
			//Elements proSelect= doc.getElementsByAttributeValue("name", "province_id");
			Elements proOptions = proDoc.getElementsByTag("option");
			for(int i=1;i<proOptions.size();i++) {
				String proNum = proOptions.get(i).attr("value");
				String proName = proOptions.get(i).text();
				//System.out.println(proNum+":"+proName);
				proMap.put(proNum, proName);
				
				
				//发送请求,获取每个省名下属的市名
				Document cityDoc = Jsoup.connect(proUrl)
									.data("type", "2")
									.data("flag", "1")
									.data("main_id", proNum)
									.post();
				Elements cityOptions = cityDoc.getElementsByTag("option");
				for(int j=1;j<cityOptions.size();j++) {
					String cityName = cityOptions.get(j).text();
					//System.out.println(cityName);
					
					String path = "http://suv.jmc.com.cn/index.php/welcome/get_dealer";
					String param = "brand_dealer=all&brand=all&type=li&city="+cityName;
					String dealerJson = HttpConnectionPost.getJson(path, param);
					//System.out.println(dealerJson);
					JSONObject json = JSON.parseObject(dealerJson);
					JSONArray dealerArray = json.getJSONArray("data");
					for(int k=0;k<dealerArray.size();k++) {
						JSONObject dealer = dealerArray.getJSONObject(k);
						String name = dealer.getString("dealer_name");
						String sellTell = dealer.getString("dealer_fwtel");
						String address = dealer.getString("dealer_address");
						String lng = dealer.getString("dealer_lon");
						String lat = dealer.getString("dealer_lat");
						List<String> shareholder = TianYanCha.getShareholder(name);
						String sControllingShareholder = shareholder.get(0);
						String sOtherShareholders = shareholder.get(1);
						
						AgencyEntity agency = new AgencyEntity();
						agency.setdCloseDate(null);
						agency.setdOpeningDate(null);
						agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
						agency.setnBrandID(-1);
						agency.setsBrand("福特");

						agency.setnDealerIDWeb(-1);
						agency.setnManufacturerID(-1);
						agency.setsManufacturer("江铃汽车");

						agency.setnState(1);
						agency.setsAddress(address);
						agency.setsCity(cityName);
						agency.setsCustomerServiceCall(null);
						agency.setsDealerName(name);
						agency.setsDealerType(null);
						agency.setsProvince(proName);
						agency.setsSaleCall(sellTell);
						agency.setsLongitude(lng);
						agency.setsLatitude(lat);
						agency.setsControllingShareholder(sControllingShareholder);
						agency.setsOtherShareholders(sOtherShareholders);
						list.add(agency);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	
	public static void main(String[] args) {
		JiangLingCrawler crawler = new JiangLingCrawler();
		System.out.println("爬虫开始...");
		AgencyDao dao = new AgencyDao();
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}
}
