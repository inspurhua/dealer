package crawler.leikesasi;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class FengTianQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.lexus.com.cn/json/dealerdata";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		JSONArray dealerArray = JSON.parseArray(dealerText);
		for(int i=0;i<dealerArray.size();i++) {
			JSONObject objdect = dealerArray.getJSONObject(i);
			String name = objdect.getString("Name");
			String cityName = objdect.getString("City");
			String proName = objdect.getString("Province");
			String address = objdect.getString("Address");
			String sellTell = objdect.getString("Tel");
			String[] lngAndLat = objdect.getString("MapCode").split(",");
			
			String lng = lngAndLat[0];
			String lat = lngAndLat[1];
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("雷克萨斯");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("丰田汽车");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			
			list.add(agency);
			
			
		}
		
		
		return list;
	}
	public static void main(String[] args) {
		FengTianQiCheCrawler crawler = new FengTianQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
		
	}

}
