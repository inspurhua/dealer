package crawler.rongwei;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;

public class ShangHaiQiCheCrawler {
	public List<AgencyEntity> getAgency() {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getPro = "http://openapi.roewe.com.cn/ndms-base-server/getArea/2/1000000000/1/1";
		String proText = HttpConnectionGet.getJson(getPro).trim();
		int startPro = proText.indexOf("[");
		int endPro = proText.length()-1;
		JSONArray proArray = JSON.parseArray(proText.substring(startPro, endPro));
		for(int i=0;i<proArray.size();i++) {
			String proNum = proArray.getJSONObject(i).getString("id");
			String proName = proArray.getJSONObject(i).getString("name");
			
			String getCiyt = "http://openapi.roewe.com.cn/ndms-base-server/getArea/3/"+proNum+"/1/1";
			String cityText = HttpConnectionGet.getJson(getCiyt).trim();
			if(cityText.isEmpty()) {
				continue;
			}
			int startCity = cityText.indexOf("[");
			int endCity = cityText.length()-1;
			JSONArray cityArray = JSON.parseArray(cityText.substring(startCity, endCity));
			for(int j=0;j<cityArray.size();j++) {
				String cityNum = cityArray.getJSONObject(j).getString("id");
				String cityName = cityArray.getJSONObject(j).getString("name");
				
				String getDealer = "http://openapi.roewe.com.cn/ndms-base-server/getSalesInfo/1/"+cityNum+"/3";
				String dealerText = HttpConnectionGet.getJson(getDealer).trim();
				if(dealerText.isEmpty()) {
					continue;
				}
				int startDealer = dealerText.indexOf("[");
				int endDealer = dealerText.length()-1;
				JSONArray dealerArray = JSON.parseArray(dealerText.substring(startDealer, endDealer));
				for(int k=0;k<dealerArray.size();k++) {
					JSONObject dealer = dealerArray.getJSONObject(k);
					String sellTell = dealer.getString("hotLine");
					if(sellTell == null || sellTell.isEmpty()) {
						continue;
					}
					String name = dealer.getString("name");
					String address = dealer.getString("salesAddress");
					String lat = dealer.getString("latitude");
					String lng = dealer.getString("longitude");
					List<String> shareholder = TianYanCha.getShareholder(name);
					String sControllingShareholder = shareholder.get(0);
					String sOtherShareholders = shareholder.get(1);
					
					AgencyEntity agency = new AgencyEntity();
					agency.setdCloseDate(null);
					agency.setdOpeningDate(null);
					agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
					agency.setnBrandID(-1);
					agency.setsBrand("荣威");

					agency.setnDealerIDWeb(-1);
					agency.setnManufacturerID(-1);
					agency.setsManufacturer("上海汽车");

					agency.setnState(1);
					agency.setsAddress(address);
					agency.setsCity(cityName);
					agency.setsCustomerServiceCall(null);
					agency.setsDealerName(name);
					agency.setsDealerType(null);
					agency.setsProvince(proName);
					agency.setsSaleCall(sellTell);
					agency.setsLongitude(lng);
					agency.setsLatitude(lat);
					agency.setsControllingShareholder(sControllingShareholder);
					agency.setsOtherShareholders(sOtherShareholders);
					list.add(agency);
				}
			}
		}
				
		return list;
	}
	public static void main(String[] args) {
		ShangHaiQiCheCrawler crawler = new ShangHaiQiCheCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
