package crawler.kaidilake;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.Location;
import util.MybatisTool;
import util.TianYanCha;

public class ShangHaiTongYong {
	public List<AgencyEntity> getAgency() throws ScriptException {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "http://www.cadillac.com.cn/api/mapdealer.ashx";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		System.out.println(dealerText+"===");
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		String dealersStr = (String) engine.eval(dealerText + "JSON.stringify(dealerinfo)");
		
		JSONArray dealerArray= JSON.parseArray(dealersStr);
		for(int i=0;i<dealerArray.size();i++) {
			JSONArray dealer = dealerArray.getJSONArray(i);
			String name = dealer.getString(1);
			String address = dealer.getString(2);
			String sellTell = dealer.getString(7);
			String proName = dealer.getString(5);
			String cityName = dealer.getString(6);
			String lng = dealer.getString(4);
			String lat = dealer.getString(3);
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("凯迪拉克");
			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("上海通用");
			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			
			list.add(agency);
			
		}
		
		
		
		
		return list;
		
	}
	public static void main(String[] args) throws ScriptException {
		ShangHaiTongYong crawler = new ShangHaiTongYong();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		/*System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");*/
	}

}
