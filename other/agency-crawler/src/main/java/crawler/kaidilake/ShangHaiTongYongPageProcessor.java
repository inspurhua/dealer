package crawler.kaidilake;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.jayway.jsonpath.JsonPath;
import com.zql.entity.AgencyDao;
import com.zql.entity.AgencyEntity2;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/** 
* @author 钟琴隆 E-mail: ai31354907@163.com
* @version 创建时间：2018年11月5日 上午10:10:40 
* 类说明 
*/
public class ShangHaiTongYongPageProcessor implements PageProcessor {

	@Override
	public void process(Page page) {
		try {
			//System.out.println();
			String dealerText =page.getJson().get();
			//System.out.println(dealerText+"===");
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			//System.out.println(dealerText);
			String dealersStr = (String) engine.eval(dealerText + "JSON.stringify(dealerinfo)");
			List<String> name = JsonPath.read(dealersStr, "$[*].[1]");
			List<String> address = JsonPath.read(dealersStr, "$[*].[2]");
			List<String> lat = JsonPath.read(dealersStr, "$[*].[3]");
			List<String> lng = JsonPath.read(dealersStr, "$[*].[4]");
			List<String> proName = JsonPath.read(dealersStr, "$[*].[5]");
			List<String> cityName = JsonPath.read(dealersStr, "$[*].[6]");
			List<String> sellTell = JsonPath.read(dealersStr, "$[*].[7]");
			List<String> dnull = new ArrayList<>();
			for(int i=0;i<name.size();i++) {
				dnull.add("");
			}
			
			AgencyEntity2 agency = new AgencyEntity2();
			//agency.setdCloseDate(null);
			//agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			//agency.setnBrandID(-1);
			agency.setsBrand("凯迪拉克");
			//agency.setnDealerIDWeb(-1);
			//agency.setnManufacturerID(-1);
			agency.setsManufacturer("上海通用");
			//agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(dnull);
			agency.setsDealerName(name);
			agency.setsDealerType(dnull);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			//agency.setsControllingShareholder(sControllingShareholder);
			//agency.setsOtherShareholders(sOtherShareholders);
			
			new AgencyDao().add(agency);
			
			//JSONArray dealerArray= JSON.parseArray(dealersStr);
			//System.out.println(dealersStr);
/*			for(int i=0;i<dealerArray.size();i++) {
				JSONArray dealer = dealerArray.getJSONArray(i);
				String name = dealer.getString(1);
				//System.out.println(name);
				}*/
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}
	private static Site site = Site.me()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0")
			.addHeader("Host", "www.cadillac.com.cn")
			.addHeader("Accept", "*/*")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2")
			.addHeader("Accept-Encoding", "gzip, deflate, br")
			.addHeader("Referer", "https://www.cadillac.com.cn/plus/delearsearch.html")
			.addHeader("Connection", "keep-alive")
								;
	@Override
	public Site getSite() {
		return site;
	}
public static void main(String[] args) {
	Spider.create(new ShangHaiTongYongPageProcessor())
	.addUrl("https://www.cadillac.com.cn/api/mapdealer.ashx")
	.run();
}
}
 