package crawler.jeep;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.HttpConnectionGet;
import util.MybatisTool;
import util.TianYanCha;
//WARNING: Hostname is not matched for cert.数据存了 证书不匹配，不影响抓取结果
public class KeLaiSiLeCrawler {
	public List<AgencyEntity> getAgency() throws ScriptException {
		List<AgencyEntity> list = new ArrayList<AgencyEntity>();
		String getDealer = "https://www.jeep.com.cn/financial/js/dealer_data.js";
		String dealerText = HttpConnectionGet.getJson(getDealer);
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		String dealersStr = (String) engine.eval(dealerText + "JSON.stringify(dealerData)");
		
		JSONArray dealerArray= JSON.parseArray(dealersStr);
		for(int i=0;i<dealerArray.size();i++) {
			JSONObject dealer = dealerArray.getJSONObject(i);
			String name = dealer.getString("name");
			String address = dealer.getString("address");
			String sellTell = dealer.getString("phone");
			String proName = dealer.getString("province");
			String cityName = dealer.getString("city");
			String[] lngAndLat = dealer.getString("map").split(",");
			String lng = lngAndLat[0];
			String lat = lngAndLat[1];
			List<String> shareholder = TianYanCha.getShareholder(name);
			String sControllingShareholder = shareholder.get(0);
			String sOtherShareholders = shareholder.get(1);
			
			AgencyEntity agency = new AgencyEntity();
			agency.setdCloseDate(null);
			agency.setdOpeningDate(null);
			agency.setdUpdateTime(new Timestamp(System.currentTimeMillis()));
			agency.setnBrandID(-1);
			agency.setsBrand("Jeep");

			agency.setnDealerIDWeb(-1);
			agency.setnManufacturerID(-1);
			agency.setsManufacturer("克莱斯勒");

			agency.setnState(1);
			agency.setsAddress(address);
			agency.setsCity(cityName);
			agency.setsCustomerServiceCall(null);
			agency.setsDealerName(name);
			agency.setsDealerType(null);
			agency.setsProvince(proName);
			agency.setsSaleCall(sellTell);
			agency.setsLongitude(lng);
			agency.setsLatitude(lat);
			agency.setsControllingShareholder(sControllingShareholder);
			agency.setsOtherShareholders(sOtherShareholders);
			list.add(agency);
		}
		
		
		return list;
	}
	public static void main(String[] args) throws ScriptException {
		KeLaiSiLeCrawler crawler = new KeLaiSiLeCrawler();
		System.out.println("爬虫开始...");
		List<AgencyEntity> agencys = crawler.getAgency();
		System.out.println("抓取完毕,正在存库");
		for(AgencyEntity agency : agencys) {
			MybatisTool.save(agency);
		}
		MybatisTool.close();
		System.out.println("请查看数据库");
	}

}
