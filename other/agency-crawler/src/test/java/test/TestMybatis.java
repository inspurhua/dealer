package test;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.zql.entity.AgencyEntity;

import dao.AgencyDaoInterface;

public class TestMybatis {
	private SqlSession session;
	@Before
	public void before() {
		SqlSessionFactoryBuilder ssfb = new SqlSessionFactoryBuilder();
		SqlSessionFactory ssf = ssfb.build(TestMybatis.class.getClassLoader().getResourceAsStream("SqlMapConfig.xml"));
		session = ssf.openSession();

	}
	@Test
	public void test1() {
		
		AgencyDaoInterface dao = session.getMapper(AgencyDaoInterface.class);
		AgencyEntity agency = new AgencyEntity();
		agency.setsAddress("山东省济南市高新区");
		agency.setsBrand("大众");
		dao.save(agency);
		session.commit();
		session.close();
		System.out.println("完成");
		
	}
}
