package test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.zql.entity.AgencyEntity;

import dao.AgencyDao;
import util.DBTool;

public class Duibi {

	public static List<String> test1() {
		Connection conn = null;
		List<String> list = new ArrayList<String>();
		try {
			conn = DBTool.getConnection();
			Statement s = conn.createStatement();
			String sql = "SELECT name from shang_hai_da_zhong ";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				String name = rs.getString("name");
				list.add(name);
				//System.out.println(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBTool.close(conn);
		}
		return list;
	}
	
	public static List<String> test2() {
		Connection conn = null;
		List<String> list = new ArrayList<String>();
		try {
			conn = DBTool.getConnection();
			Statement s = conn.createStatement();
			String sql = "SELECT name from shanghaiVW6 ";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				String name = rs.getString("name");
				list.add(name);
				//System.out.println(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBTool.close(conn);
		}
		return list;
	}
	

	public static AgencyEntity test3(String n) {
		Connection conn = null;
		AgencyEntity ae = null;
		try {
			conn = DBTool.getConnection();
			Statement s = conn.createStatement();
			String sql = "SELECT * from shanghaiVW6 "
					+ "where name="+ "'"+n+"'";
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				String name = rs.getString("name");
				System.out.println(name);
				//name = name.split("\\.")[1];
				String province = rs.getString("province");
				String city = rs.getString("city");
				String address = rs.getString("address");
				String sellTell = rs.getString("tell");
				
				ae = new AgencyEntity();
				
				System.out.println(ae);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBTool.close(conn);
		}
		return ae;
	}
	
	
	
	
	public static void main(String[] args) {
		List<String> xin = test1();
		System.out.println(xin.size());
		List<String> jiu = test2();
		System.out.println(jiu.size());
		for(String str1 : jiu) {
			boolean flag = xin.contains(str1);
			if(!flag) {
				System.out.println(str1);
				AgencyEntity agency = test3(str1);
				AgencyDao dao = new AgencyDao();
				dao.save(agency);
			}
		}
		System.out.println("完毕");
	}
	
}
