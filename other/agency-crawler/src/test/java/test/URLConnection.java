package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.junit.Test;
public class URLConnection {
	@Test
	public void test1() throws IOException {
		 String strURL = "http://www.changan-mazda.com.cn/dictionary/dealer/110000";  
		    URL url = new URL(strURL);  
		    HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();  
		    httpConn.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
		    httpConn.setRequestProperty("Accept-Encoding", "gzip,deflate");
		    httpConn.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.5");
		    httpConn.setRequestProperty("Connection", "keep-alive");
		    httpConn.setRequestProperty("Cookie", "_dc3c=1; Hm_lvt_8ac456d3f48e55a4f1db93ee8a3bdae3=1506778569,1506844041; Hm_lpvt_8ac456d3f48e55a4f1db93ee8a3bdae3=1506845962; dmt2=5%7C0%7C0%7Cwww.changan-mazda.com.cn%2Fdealer%7C; dmts2=1; dm2=2%7C1506845962%7C0%7C%7C%7C%7C%7C1506778570%7C1506778570%7C1506778570%7C1506844041%7Cfbeb819f5714ed3e903d11558da3619d%7C0%7C%7C; dcad2=; dc_search2=; CIGDCID=fbeb819f5714ed3e903d11558da3619d; _ga=GA1.3.549206199.1506778570; _gid=GA1.3.125866624.1506778570");
		    httpConn.setRequestProperty("Host", "www.changan-mazda.com.cn");
		    httpConn.setRequestProperty("Referer", "http://www.changan-mazda.com.cn/dealer");
		    httpConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36");
		    httpConn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
		    
		    httpConn.connect();
		    System.out.println(httpConn.getResponseCode());
		    GZIPInputStream gzipInputStream = new GZIPInputStream(httpConn.getInputStream());
		    BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInputStream));
		    String line = null;
		    while ((line = reader.readLine()) != null){
		    	//line = new String(line.getBytes(), "utf16");
		    	System.out.println(line);
		    }
	
		    
	}
}
