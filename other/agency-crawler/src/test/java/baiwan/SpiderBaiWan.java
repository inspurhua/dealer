package baiwan;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import util.HttpConnectionGet;

public class SpiderBaiWan {
	
	public void action() {
		String url = "https://www.wukong.com/wenda/wapshare/subject/tab/brow/?subject_id=6509045544260731143&offset=0&tab_id=1";
		String qJson = HttpConnectionGet.getJson(url);
		JSONArray items = JSON.parseObject(qJson).getJSONObject("data").getJSONArray("tab_item_list");
		for(int i=0;i<items.size();i++) {
			JSONObject question = items.getJSONObject(i).getJSONObject("question");
			String title = question.getString("title");
			String qid = question.getString("qid");
			String options = question.getJSONObject("content").getString("text");
			
			//根据qid,获取回答数据
			String aUrl = "https://www.wukong.com/question/"+qid+"/";
			String answerList = answer(aUrl);
			
		}
	}
	
	public String answer(String aUrl) {
		Map<String, String> header = new HashMap<String, String>();
		header.put("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
		header.put("upgrade-insecure-requests", "1");
		header.put("cache-control", "max-age=0");
		header.put("accept-language", "zh-CN,zh;q=0.9");
		header.put("accept-encoding", "gzip, deflate, br");
		header.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
//		header.put(":authority", "www.wukong.com");
//		header.put(":method", "GET");
//		header.put(":path:", "/question/6508630697689743624/");
//		header.put(":scheme", "https");
		StringBuffer strBuffer = new StringBuffer();
		try {
			Document doc = Jsoup.connect(aUrl).headers(header).get();
			Elements items = doc.getElementsByClass("answer-items").get(0).getElementsByClass("answer-text-full");
			if(items.isEmpty()) {
				System.out.println("空");
			}
			for(Element item : items) {
				String ansText = item.text();
				strBuffer.append(ansText+"|");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strBuffer.toString();
		

	}
	public static void main(String[] args) {
		SpiderBaiWan spider = new SpiderBaiWan();
		//spider.action();
		//spider.answer("https://www.wukong.com/question/6508630697689743624/");
		spider.answer("https://www.wukong.com/question/6509706042375405831/");
	}
}
