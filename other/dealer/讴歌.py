import requests
import json
from datetime import date
from dbutil.pgsql import PgsqlPipeline

headers = {
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36"
}

def get_provinces():
	url = "http://www.acura.com.cn/api/sitecore/Common/GetProvinceFilterByDealer"
	r = requests.get(url, headers=headers)
	provinces = json.loads(r.text)['Data']
	return provinces

def get_cities(province_id):
	url = "https://www.acura.com.cn/api/sitecore/Common/GetCityFilterByDealer"
	data={
		"provinceId":province_id
	}
	r = requests.post(url, headers=headers,data=data)

	cities = json.loads(r.text)['Data']

	return cities
#
def get_dealers(city_id):
	url = 'https://www.acura.com.cn/api/sitecore/Map/GetDealerList'
	data ={
		'cityId':city_id
	}
	r = requests.get(url, headers=headers,data=data)

	dealers = json.loads(r.text)['Data']
	return dealers

def get_item(dealer, province_name, city_name):
	item = {
		"dealer_name" : dealer["DEALER_NAME"],
		"brand_id" : None,
		"address" : dealer["ADDRESS"],
		"brand" : "讴歌",
		"province" : province_name,
		"city" : city_name,
		"sale_call" : dealer["SALES_PHONE"],
		"customer_service_call" : None,
		"update_time" : date.today(),
		"longitude" : dealer['Lng'],
		"latitude" : dealer['Lat'],
		"dealer_type" : None,
		"manufacturer_id" : None,
		"manufacturer" : "讴歌",
		"state" : None,
		"opening_date" : None,
		"close_date" : None,
		"dealer_id_web" : None,
		"controlling_shareholder" : None,
		"other_shareholders" : None,
		"status" : None,
		"remarks" : None,
	}
	return item

def get_items():
	items = []
	provinces = get_provinces()
	for province in provinces:

		province_id = province["Id"]
		province_name = province["Name"]
		print(province_id,province_name)
		cities = get_cities(province_id)
		for city in cities:
			city_id = city["Id"]
			city_name = city["Name"]
			print(city_id,city_name)
			dealers = get_dealers(city_id)
			for dealer in dealers:
				item = get_item(dealer, province_name, city_name)
				items.append(item)
	return items

def main():
	print("爬虫开始...")
	items = get_items()
	print("数据存库")
	pg = PgsqlPipeline()
	for item in items:
		pg.process_item(item)
	pg.close()
	print("完成")

if __name__ == '__main__':
	main()


