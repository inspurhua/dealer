import requests
import json
from datetime import datetime
from dbutil.pgsql import PgsqlPipeline

headers = { 'Host': 'cn-digital2-app.bmw.com.cn',
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',
			'Accept': '*/*',
			'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
			'Accept-Encoding': 'gzip, deflate, br',
			'Referer': 'https://www.minichina.com.cn/zh_CN/home/dlo.html',
			'Connection': 'keep-alive',
			'Pragma': 'no-cache',
			'Cache-Control': 'no-cache'}

provinces = []
cities = []
dealers = []

def get_start_end_position(res_str):
	"""获得截取json字符串的起始位置"""
	try:
		start = res_str.index('(')
		end = res_str.rindex(')')
	except Exception as e:
		print('没有找到"("')
	else:
		return (start+1, end)

def get_province_city():
	province_city_url = "https://cn-digital2-app.bmw.com.cn/dlo/init?"
	res = requests.get(province_city_url, headers=headers)
	res_str = res.text
	position = get_start_end_position(res_str)
	json_str = res_str[position[0]:position[1]]
	total = json.loads(json_str)
	global provinces, cities
	provinces = total["responseBody"]["provinces"]
	cities = total["responseBody"]["cities"]

def get_dealer():
	dealer_url = f"https://cn-digital2-app.bmw.com.cn/dlo/outlet?brands=2"
	r = requests.get(dealer_url, headers=headers)
	res_str = r.text
	position = get_start_end_position(res_str)
	json_str = res_str[position[0]:position[1]]
	total = json.loads(json_str)
	global dealers
	dealers = total["responseBody"]["outlets"]

def get_province_by_id(pid):
	"""根据province_id获取省名"""
	for pro in provinces:
		if pro["id"] == pid:
			return pro["nz"]

def get_city_by_id(cid):
	"""根据city_id获取市名"""
	for city in cities:
		if city["id"] == cid:
			return city["nz"]


def get_items():
	get_province_city()
	get_dealer()
	items = []
	for dealer in dealers:
		display = dealer["rtlFmt"]["id"]
		if display == "16":
			continue
		
		item = {
		"dealer_name" : dealer["nz"],
		"brand_id" : None,
		"address" : dealer["az"],
		"brand" : "宝马Mini",
		"province" : get_province_by_id(dealer["pv"]),
		"city" : get_city_by_id(dealer["ct"]),
		"sale_call" : dealer["tel"],
		"customer_service_call" : None,
		"update_time" : datetime.now(),
		"longitude" : dealer["lnb"],
		"latitude" : dealer["ltb"],
		"dealer_type" : None,
		"manufacturer_id" : None,
		"manufacturer" : "宝马汽车",
		"state" : None,
		"opening_date" : None,
		"close_date" : None,
		"dealer_id_web" : None,
		"controlling_shareholder" : None,
		"other_shareholders" : None,
		"status" : None,
		"remarks" : None,
		}
		items.append(item)
	return items

def main():
	print("爬虫开始....")
	items = get_items()
	print("开始存库...")
	pgsql = PgsqlPipeline()
	for item in items:
		pgsql.process_item(item)
		pass
	pgsql.close
	print("完成")
		


if __name__ == '__main__':
	main()