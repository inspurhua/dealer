import psycopg2

class PgsqlPipeline(object):
    def __init__(self):
        self.conn = psycopg2.connect(database="pa", user="postgres", password="postgres", host="127.0.0.1", port="15432")
        self.cursor = self.conn.cursor()
    def process_item(self, item):
        insert_sql = f"""INSERT INTO "tDealer" 
		("sDealerName","nBrandID","sBrand","sProvince","sCity","sAddress","sSaleCall","sCustomerServiceCall","sDealerType","nManufacturerID","sManufacturer","nState","dOpeningDate","dCloseDate","dUpdateTime","nDealerIDWeb","sLongitude","sLatitude","sControllingShareholder","sOtherShareholders","sStatus","sRemarks")
		VALUES 
		
       	(%(dealer_name)s, %(brand_id)s, %(brand)s, %(province)s, %(city)s, %(address)s, %(sale_call)s, %(customer_service_call)s, %(dealer_type)s, %(manufacturer_id)s,%(manufacturer)s, %(state)s, %(opening_date)s, %(close_date)s, %(update_time)s, %(dealer_id_web)s, %(longitude)s, %(latitude)s, %(controlling_shareholder)s, %(other_shareholders)s, %(status)s, %(remarks)s)"""

        self.cursor.execute(insert_sql, item)
        self.conn.commit()
    def close(self):
        self.cursor.close()
        self.conn.close()
if __name__ == '__main__':
	pgsql = PgsqlPipeline()
	print(pgsql)
