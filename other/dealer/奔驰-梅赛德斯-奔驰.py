import requests
import json
from datetime import datetime
import sys
from dbutil.pgsql import PgsqlPipeline

def get_provinces():
	url = "https://api.oneweb.mercedes-benz.com.cn/ow-dealers-location/provinces/query?needFilterByDealer=true&needFilterByModel=false&modelName=null";
	res = requests.get(url, verify=False)
	provinces = json.loads(res.text)["result"]
	return provinces

def get_cities_by_province_id(province_id):
	url = f"https://api.oneweb.mercedes-benz.com.cn/ow-dealers-location/cities/query?needFilterByDealer=true&provinceId={province_id}&needFilterByModel=false&modelName="
	res = requests.get(url, verify=False)
	cities = json.loads(res.text)["result"]
	return cities

def get_dealer_by_city_name(city_name):
	url = f"https://api.oneweb.mercedes-benz.com.cn/ow-dealers-location/dealers/query?sort=alphabetical&city={city_name}&longitude=&latitude=&keywords=&dealerId=&needFilterByModel=false&modelName="
	res = requests.get(url, verify=False)
	dealers = json.loads(res.text)["result"]
	return dealers

def get_items():
	items = []
	provinces = get_provinces()
	for province in provinces:
		province_id = province["id"]
		province_name = province["name"]
		cities = get_cities_by_province_id(province_id)
		for city in cities:
			city_name = city["name"]
			dealers = get_dealer_by_city_name(city_name)
			for dealer in dealers:
				remarks = "/"
				services = dealer["service_scope"]
				if services != None and len(services) != 0:
					for service in services:
						if service["categoryCode"] == "sales":
							types = service["types"]
							remarks = remarks.join([type["name"] for type in types] )
				
				item = {
					"dealer_name" : dealer["displayName"],
					"brand_id" : None,
					"address" : dealer["address"],
					"brand" : "奔驰",
					"province" : dealer["province"],
					"city" : dealer["city"],
					"sale_call" : dealer["phoneNumber"],
					"customer_service_call" : dealer["afterSaleServicePhoneNumber"],
					"update_time" : datetime.now(),
					"longitude" : dealer["longitude"],
					"latitude" : dealer["latitude"],
					"dealer_type" : None,
					"manufacturer_id" : None,
					"manufacturer" : "梅赛德斯-奔驰",
					"state" : None,
					"opening_date" : None,
					"close_date" : None,
					"dealer_id_web" : None,
					"controlling_shareholder" : None,
					"other_shareholders" : None,
					"status" : None,
					"remarks" : remarks,
				}
				items.append(item)
	return items			

def main():
	print("爬虫开始...")
	items = get_items()
	print("数据存库")
	pg = PgsqlPipeline()
	for item in items:
		pg.process_item(item)
	pg.close()
	print("完成")

if __name__ == '__main__':
	main()