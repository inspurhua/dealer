import requests
import json
from datetime import date
from dbutil.pgsql import PgsqlPipeline

def get_provinces():
	url = "http://mmc.prod.bjev.com.cn/cmsapi//configlocation/getAllProvinces?timestap=1551684643066"
	r = requests.get(url)
	provinces = json.loads(r.text)["result"]
	return provinces

def get_province_id(province_id):
	url = f"http://mmc.prod.bjev.com.cn/cmsapi//configlocation/getCitiesByProvinceId?timestap=1551688602416&provinceId={province_id}"
	res = requests.get(url)
	cities = json.loads(res.text)["result"]
	return cities

def get_dealers(city_id):
	url = f"http://mmc.prod.bjev.com.cn/cmsapi//cmsdealerinfo2c/getDealerInfoList?timestap=1551687958127&locationId={city_id}&serialId=&pageSize=10000"
	r = requests.get(url)
	dealers = json.loads(r.text)["result"]["list"]
	return dealers

def get_items():
	items = []
	nn = get_provinces()
	for nn2 in nn:
		pid = nn2["id"]
		cities = get_province_id(pid)
		for cit in cities:
			cid = cit["id"]
			prname = cit["provinceName"]
			ciname = cit["name"]
			locationId = get_dealers(cid)
			for dealer in locationId:
					item = {
				"dealer_name" : dealer["dealerFullName"],
				"brand_id" : None,
				"address" : dealer["contactAddress"],
				"brand" : "北汽新能源",
				"province" : prname,
				"city" : ciname,
				"sale_call" : dealer["salesPhones"],
				"customer_service_call" : None,
				"update_time" : date.today(),
				"longitude" : dealer["longitude"],
				"latitude" : dealer["latitude"],
				"dealer_type" : None,
				"manufacturer_id" : None,
				"manufacturer" : "北汽新能源",
				"state" : None,
				"opening_date" : None,
				"close_date" : None,
				"dealer_id_web" : None,
				"controlling_shareholder" : None,
				"other_shareholders" : None,
				"status" : None,
				"remarks" : None,
				}
					#print(item)
			items.append(item)
	return items

def main():
	print("爬虫开始...")
	items = get_items()
	print("数据存库")
	pg = PgsqlPipeline()
	for item in items:
		pg.process_item(item)
	pg.close()
	print("完成")

if __name__ == '__main__':
	main()