import requests
import json
from datetime import date
from dbutil.pgsql import PgsqlPipeline

headers = {
	"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36"
}

def get_provinces()->list:
	url = "http://www.geely.com/api/geely/official/get/getprovincelist"
	r = requests.get(url, headers=headers)
	provinces = json.loads(r.text)
	return provinces

def get_cities(province_id)->list:
	url = f"http://www.geely.com/api/geely/official/get/getcitylist?provinceid={province_id}"
	r = requests.get(url, headers=headers)
	cities = json.loads(r.text)
	return cities

def get_dealers(province_id, city_id)->list:
	url = f"http://www.geely.com/api/geely/official/get/GetDealer?seriesCode=&province={province_id}&city={city_id}&keyword="
	r = requests.get(url, headers=headers)
	dealers = json.loads(r.text)
	return dealers

def get_item(dealer, province_name, city_name):
	longitude = ""
	latitude = ""

	if dealer["coordinates"] != None and "," in dealer["coordinates"]:
		try:
			longitude = dealer["coordinates"].split(",")[0]
			latitude = dealer["coordinates"].split(",")[1]
		except Exception as e:
			longitude = dealer["coordinates"].split("，")[0]
			latitude = dealer["coordinates"].split("，")[1]
	print(longitude, latitude)

	item = {
		"dealer_name" : dealer["dealerName"],
		"brand_id" : None,
		"address" : dealer["address"],
		"brand" : "吉利",
		"province" : province_name,
		"city" : city_name,
		"sale_call" : dealer["bizPhone"],
		"customer_service_call" : None,
		"update_time" : date.today(),
		"longitude" : longitude,
		"latitude" : latitude,
		"dealer_type" : None,
		"manufacturer_id" : None,
		"manufacturer" : "吉利汽车",
		"state" : None,
		"opening_date" : None,
		"close_date" : None,
		"dealer_id_web" : None,
		"controlling_shareholder" : None,
		"other_shareholders" : None,
		"status" : None,
		"remarks" : None,
	}
	return item

def get_items():
	items = []
	provinces = get_provinces();
	for province in provinces:
		province_id = province["regionId"]
		province_name = province["regionName"]
		cities = get_cities(province_id)
		for city in cities:
			city_id = city["regionId"]
			city_name = city["regionName"]
			dealers = get_dealers(province_id, city_id)
			for dealer in dealers:
				item = get_item(dealer, province_name, city_name)
				items.append(item)
	return items

def main():
	print("爬虫开始...")
	items = get_items()
	print("数据存库")
	pg = PgsqlPipeline()
	for item in items:
		pg.process_item(item)
	pg.close()
	print("完成")

if __name__ == '__main__':
	main()