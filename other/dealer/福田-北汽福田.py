import requests
import json
from datetime import date
from dbutil.pgsql import PgsqlPipeline

provinces = []
cities = []

def get_pro_city()->dict:
	url = "http://www.foton.com.cn/webback/dmp/getProvinceCity"
	r = requests.get(url)
	data = json.loads(r.text)
	return data

def parse_pro_city_data(data):
	global provinces
	global cities

	for key, value in data.items():
		province_name = key.split("=")[2]
		for city in value:
			city_id = city["id"]
			province_id = city["provinceId"]
			city_name = city["cityName"]
			cities.append({city_id:city_name})
			provinces.append({province_id:province_name})

def get_dealers(province_id)->dict:
	url = f"http://www.foton.com.cn/webback/dmp/getDealer?type=1&brandId=-1&provinceId={province_id}&cityId=-1&areaId=-1"
	r = requests.get(url)
	dealers = json.loads(r.text)
	return dealers

def get_province_name(province_id)->str:
	for key in provinces:
		if key == province_id:
			return provinces[key]

def get_city_name(city_id)->str:
	for key in cities:
		if key == city_id:
			return cities[key]

def get_dealer_point(longitudeLatitude)->list:
	return longitudeLatitude.split("_")
def get_item(dealer):
	item = {
		"dealer_name" : dealer["dealerName"],
		"brand_id" : None,
		"address" : dealer["dealerAddress"],
		"brand" : "福田",
		"province" : get_province_name(dealer["provinceId"]),
		"city" : get_city_name(dealer["cityId"]),
		"sale_call" : dealer["dealerTel"],
		"customer_service_call" : None,
		"update_time" : date.today(),
		"longitude" : get_dealer_point(dealer["longitudeLatitude"])[0],
		"latitude" : get_dealer_point(dealer["longitudeLatitude"])[1],
		"dealer_type" : None,
		"manufacturer_id" : None,
		"manufacturer" : "北汽福田",
		"state" : None,
		"opening_date" : None,
		"close_date" : None,
		"dealer_id_web" : None,
		"controlling_shareholder" : None,
		"other_shareholders" : None,
		"status" : None,
		"remarks" : None,
	}
	return item

def get_items():
	data = get_pro_city()
	parse_pro_city_data(data)

	items = []
	for province in provinces:
		province_id = list(province.keys())[0]
		dealers = get_dealers(province_id)
		for dealer in dealers:
			item = get_item(dealer)
			items.append(item)
	return items

def main():
	print("爬虫开始...")
	items = get_items()
	print("数据存库")
	pg = PgsqlPipeline()
	for item in items:
		pg.process_item(item)
	pg.close()
	print("完成")

if __name__ == '__main__':
	main()