/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 100011
 Source Host           : localhost:5432
 Source Catalog        : dealer
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100011
 File Encoding         : 65001

 Date: 02/12/2019 14:10:56
*/


-- ----------------------------
-- Table structure for tDealer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tDealer";
CREATE TABLE "public"."tDealer" (
  "sDealerName" varchar(255) COLLATE "pg_catalog"."default",
  "nBrandID" varchar(255) COLLATE "pg_catalog"."default",
  "sBrand" varchar(255) COLLATE "pg_catalog"."default",
  "sProvince" varchar(255) COLLATE "pg_catalog"."default",
  "sCity" varchar(255) COLLATE "pg_catalog"."default",
  "sAddress" varchar(255) COLLATE "pg_catalog"."default",
  "sSaleCall" varchar(255) COLLATE "pg_catalog"."default",
  "sCustomerServiceCall" varchar(255) COLLATE "pg_catalog"."default",
  "sDealerType" varchar(255) COLLATE "pg_catalog"."default",
  "nManufacturerID" varchar(255) COLLATE "pg_catalog"."default",
  "sManufacturer" varchar(255) COLLATE "pg_catalog"."default",
  "nState" varchar(255) COLLATE "pg_catalog"."default",
  "dOpeningDate" varchar(255) COLLATE "pg_catalog"."default",
  "dCloseDate" varchar(255) COLLATE "pg_catalog"."default",
  "dUpdateTime" varchar(255) COLLATE "pg_catalog"."default",
  "nDealerIDWeb" varchar(255) COLLATE "pg_catalog"."default",
  "sLongitude" varchar(255) COLLATE "pg_catalog"."default",
  "sLatitude" varchar(255) COLLATE "pg_catalog"."default",
  "sControllingShareholder" varchar(255) COLLATE "pg_catalog"."default",
  "sOtherShareholders" varchar(255) COLLATE "pg_catalog"."default",
  "sStatus" varchar(255) COLLATE "pg_catalog"."default",
  "sRemarks" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."tDealer" OWNER TO "postgres";

-- ----------------------------
-- Records of tDealer
-- ----------------------------
